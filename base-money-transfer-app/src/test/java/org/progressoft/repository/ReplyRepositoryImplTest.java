package org.progressoft.repository;

import org.junit.jupiter.api.Test;
import org.progressoft.contract.ReplyRepository;
import org.progressoft.entity.Reply;
import org.progressoft.enums.Reason;
import org.progressoft.enums.Status;
import org.progressoft.exception.*;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class ReplyRepositoryImplTest {

    private final ReplyRepository replyRepository;
    private final Connection connection;

    private final PreparedStatement preparedStatement;

    public ReplyRepositoryImplTest() throws SQLException {
        DataSource dataSource = mock(DataSource.class);
        connection = mock(Connection.class);
        preparedStatement = mock(PreparedStatement.class);
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeUpdate()).thenReturn(1);
        when(preparedStatement.execute()).thenReturn(false);
        this.replyRepository = new ReplyRepositoryImpl(dataSource);
    }

    @Test
    void givenNullReply_whenWhenSave_thenFail() {
        NullReplyException nullReplyException = assertThrows(NullReplyException.class,
                () -> replyRepository.saveReply(null));
        assertEquals("reply is null", nullReplyException.getMessage());
    }


    @Test
    void givenNullMessageId_whenUpdateReplyStatusAndReasonById_thenFail() {
        NullMessageIdException nullMessageIdException = assertThrows(NullMessageIdException.class,
                () -> replyRepository.updateReplyStatusAndReasonById(null,
                        Status.PROCESSED, Reason.ACSP));
        assertEquals("messageId is null", nullMessageIdException.getMessage());
    }

    @Test
    void givenNullStatus_whenUpdateReplyStatusAndReasonById_thenFail() {
        NullStatusException nullStatusException = assertThrows(NullStatusException.class,
                () -> replyRepository.updateReplyStatusAndReasonById("message123", null, Reason.RJCT));
        assertEquals("status is null", nullStatusException.getMessage());
    }

    @Test
    void givenNullReason_whenUpdateReplyStatusAndReasonById_thenFail() {
        NullReasonException nullReasonException = assertThrows(NullReasonException.class,
                () -> replyRepository.updateReplyStatusAndReasonById("message123", Status.PROCESSED, null));
        assertEquals("reason is null", nullReasonException.getMessage());
    }

    @Test
    void givenValidReply_whenSaveReply_thenSuccess() throws SQLException {
        Reply reply = getReply();
        replyRepository.saveReply(reply);
        verify(connection).prepareStatement(anyString());
        verify(preparedStatement, times(4)).setString(anyInt(), anyString());
        verify(preparedStatement).setTimestamp(anyInt(), any(Timestamp.class));
    }

    @Test
    void givenValidInput_whenUpdateReplyStatusAndReasonById_thenUpdateReply() throws SQLException {
        Reply reply = getReply();
        replyRepository.updateReplyStatusAndReasonById(reply.getMessageId(), Status.PROCESSED, Reason.ACSP);
        verify(preparedStatement, times(3)).setString(anyInt(), anyString());
        verify(preparedStatement).executeUpdate();
    }


    @Test
    void givenInvalidReply_whenSave_thenFail() throws SQLException {
        Reply reply = getReply();
        reply.setMessageId(null);
        reply.setStatus(null);
        when(preparedStatement.execute()).thenThrow(new ReplyNotSavedException("Could not save reply"));
        ReplyNotSavedException replyNotSavedException = assertThrows(ReplyNotSavedException.class,
                () -> replyRepository.saveReply(reply));
        assertEquals("Could not save reply", replyNotSavedException.getMessage());
    }

    @Test
    void givenTwoValidRepliesWithSameId_whenSaveReply_thenFail() throws SQLException {
        Reply reply1 = getReply();
        Reply reply2 = getReply();
        reply2.setMessageId(reply1.getMessageId());
        replyRepository.saveReply(reply1);
        verify(connection).prepareStatement(anyString());
        verify(preparedStatement, times(4)).setString(anyInt(), anyString());
        verify(preparedStatement).setTimestamp(anyInt(), any(Timestamp.class));
        when(preparedStatement.execute()).thenThrow(new ReplyNotSavedException("Could not save reply"));
        ReplyNotSavedException replyNotSavedException = assertThrows(ReplyNotSavedException.class,
                () -> replyRepository.saveReply(reply2));
        assertEquals("Could not save reply", replyNotSavedException.getMessage());
    }

    private Reply getReply() {
        Reply reply = new Reply();
        reply.setMessageId(UUID.randomUUID().toString());
        reply.setPaymentMessageId(UUID.randomUUID().toString());
        reply.setCreationDateTime(LocalDateTime.now());
        reply.setStatus(Status.PENDING_RESPONSE);
        reply.setReason(Reason.AUTH);
        return reply;
    }
}