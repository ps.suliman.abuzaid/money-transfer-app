package org.progressoft.repository;

import org.junit.jupiter.api.Test;
import org.progressoft.contract.ParticipantRepository;
import org.progressoft.entity.Participant;
import org.progressoft.exception.NullDataSourceException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


class ParticipantRepositoryImplTest {

    private final ParticipantRepository participantRepository;

    public ParticipantRepositoryImplTest() throws SQLException {
        DataSource dataSource = mock(DataSource.class);
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);

        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(true, true, false);
        when(resultSet.getString("name")).thenReturn("Arab Banking Corporation (Jordan)", "Jordan Islamic Bank");
        when(resultSet.getString("bic")).thenReturn("ABCJJOA0", "JIBAJOA0");
        this.participantRepository = new ParticipantRepositoryImpl(dataSource);
    }

    @Test
    void givenNullConnectionFactory_whenConstructing_thenFail() {
        NullDataSourceException nullDataSourceException = assertThrows(NullDataSourceException.class, () -> new ParticipantRepositoryImpl(null));
        assertEquals("dataSource must not be null", nullDataSourceException.getMessage());
    }

    @Test
    void givenValidConnectionFactory_whenGetConnection_thenReturnResult() {
        List<Participant> participants = participantRepository.getParticipants();
        assertNotNull(participants);
        assertFalse(participants.isEmpty());
        Participant arabBankCorp = getParticipantByBic(participants, "ABCJJOA0");
        Participant jib = getParticipantByBic(participants, "JIBAJOA0");
        assertNotNull(arabBankCorp, "Expected participant not returned");
        assertEquals("Arab Banking Corporation (Jordan)", arabBankCorp.getName());
        assertEquals("ABCJJOA0", arabBankCorp.getBic());
        assertNotNull(jib, "Expected participant not returned");
        assertEquals("Jordan Islamic Bank", jib.getName());
        assertEquals("JIBAJOA0", jib.getBic());
    }

    private Participant getParticipantByBic(List<Participant> participants, String bic) {
        Predicate<Participant> participantPredicate = participant -> participant.getBic().equals(bic);
        Optional<Participant> optionalParticipant = participants.stream().filter(participantPredicate).findFirst();
        return optionalParticipant.orElse(null);
    }

}