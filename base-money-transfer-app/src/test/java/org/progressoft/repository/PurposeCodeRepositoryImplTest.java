package org.progressoft.repository;

import org.junit.jupiter.api.Test;
import org.progressoft.contract.PurposeCodeRepository;
import org.progressoft.entity.PurposeCode;
import org.progressoft.exception.NullDataSourceException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


class PurposeCodeRepositoryImplTest {

    private final PurposeCodeRepository purposeCodeRepository;

    PurposeCodeRepositoryImplTest() throws SQLException {
        DataSource dataSource = mock(DataSource.class);
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);

        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(true, true, false);
        when(resultSet.getString("code")).thenReturn("42010", "41010");
        when(resultSet.getString("usage")).thenReturn("Legal Return a Payment", "Individual Return a Payment");
        purposeCodeRepository = new PurposeCodeRepositoryImpl(dataSource);
    }

    @Test
    void givenNullConnectionFactory_whenConstructing_thenFail() {
        NullDataSourceException nullDataSourceException = assertThrows(NullDataSourceException.class, () -> new PurposeCodeRepositoryImpl(null));
        assertEquals("dataSource must not be null", nullDataSourceException.getMessage());
    }

    @Test
    void givenValidConnectionFactory_whenGetConnection_thenReturnResult() {
        List<PurposeCode> purposeCodes = purposeCodeRepository.getPurposeCodes();
        assertNotNull(purposeCodes);
        assertFalse(purposeCodes.isEmpty());
        PurposeCode firstExpectedPurposeCode = getPurposeCodeByCode(purposeCodes, "42010");
        assertNotNull(firstExpectedPurposeCode);
        assertEquals("42010", firstExpectedPurposeCode.getCode());
        assertEquals("Legal Return a Payment", firstExpectedPurposeCode.getUsage());
        PurposeCode secondExpectedPurposeCode = getPurposeCodeByCode(purposeCodes, "41010");
        assertNotNull(secondExpectedPurposeCode);
        assertEquals("41010", secondExpectedPurposeCode.getCode());
        assertEquals("Individual Return a Payment", secondExpectedPurposeCode.getUsage());

    }

    private PurposeCode getPurposeCodeByCode(List<PurposeCode> purposeCodes, String code) {
        Optional<PurposeCode> optionalPurposeCode = purposeCodes.stream().filter(purposeCode -> purposeCode.getCode().equals(code)).findFirst();
        return optionalPurposeCode.orElse(null);
    }
}