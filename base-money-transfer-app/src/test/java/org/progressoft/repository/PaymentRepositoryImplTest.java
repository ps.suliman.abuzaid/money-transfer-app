package org.progressoft.repository;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.progressoft.contract.PaymentRepository;
import org.progressoft.entity.Payment;
import org.progressoft.entity.PaymentCompositeKey;
import org.progressoft.enums.Status;
import org.progressoft.exception.*;
import org.progressoft.factory.PaymentFactory;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.*;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class PaymentRepositoryImplTest {

    private final PaymentRepository paymentRepository;
    private final Connection connection;

    private final PreparedStatement preparedStatement;

    public PaymentRepositoryImplTest() throws SQLException {
        DataSource dataSource = mock(DataSource.class);
        connection = mock(Connection.class);
        preparedStatement = mock(PreparedStatement.class);
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.execute()).thenReturn(false);
        when(preparedStatement.executeUpdate()).thenReturn(1);
        this.paymentRepository = mock(PaymentRepositoryImpl.class,
                withSettings().useConstructor(dataSource));
        doCallRealMethod().when(paymentRepository).savePayment(any(Payment.class));
        doCallRealMethod().when(paymentRepository).updatePaymentStatusByCompositeKey(any(Status.class), any(PaymentCompositeKey.class));
    }

    @Test
    void givenNullPayment_whenSavePayment_thenFail() {
        doThrow(new NullPaymentException("Can not save null payment"))
                .when(paymentRepository).savePayment(null);
        NullPaymentException nullPaymentException = assertThrows(NullPaymentException.class,
                () -> paymentRepository.savePayment(null));
        assertEquals("Can not save null payment", nullPaymentException.getMessage());
    }

    @Test
    void givenPayment_whenSavePayment_thenSuccess() throws SQLException {
        Payment payment = PaymentFactory.getValidPayment();
        ArgumentCaptor<Payment> paymentArgumentCaptor = ArgumentCaptor.forClass(Payment.class);
        paymentRepository.savePayment(payment);
        verify(connection, times(1)).prepareStatement(anyString());
        verify(preparedStatement, times(1)).setInt(anyInt(), anyInt());
        verify(preparedStatement, times(1)).setTimestamp(anyInt(), any(Timestamp.class));
        verify(preparedStatement, times(1)).setBigDecimal(anyInt(), any(BigDecimal.class));
        verify(preparedStatement, times(1)).setDate(anyInt(), any(Date.class));
        verify(preparedStatement, times(24)).setString(anyInt(), anyString());
        verify(paymentRepository).savePayment(paymentArgumentCaptor.capture());
        Payment savedPayment = paymentArgumentCaptor.getValue();
        assertEquals(payment, savedPayment);
    }

    @Test
    void givenTwoPaymentsWithSameUniqueValues_whenSavePayment_thenFail() throws SQLException {
        Payment payment1 = PaymentFactory.getValidPayment();
        Payment payment2 = PaymentFactory.getValidPayment();
        paymentRepository.savePayment(payment1);
        verify(connection, times(1)).prepareStatement(anyString());
        verify(preparedStatement, times(1)).setInt(anyInt(), anyInt());
        verify(preparedStatement, times(1)).setTimestamp(anyInt(), any(Timestamp.class));
        verify(preparedStatement, times(1)).setBigDecimal(anyInt(), any(BigDecimal.class));
        verify(preparedStatement, times(1)).setDate(anyInt(), any(Date.class));
        verify(preparedStatement, times(24)).setString(anyInt(), anyString());
        doThrow(new PaymentNotSavedException("Could not save payment")).when(paymentRepository).savePayment(payment2);
        PaymentNotSavedException paymentNotSavedException = assertThrows(PaymentNotSavedException.class,
                () -> paymentRepository.savePayment(payment2));
        assertEquals("Could not save payment", paymentNotSavedException.getMessage());
    }

    @Test
    void givenNullStatus_whenUpdatePaymentStatusByCompositeId_thenFail() {
        PaymentCompositeKey key = new PaymentCompositeKey();
        doThrow(new NullStatusException("status is null")).when(paymentRepository)
                .updatePaymentStatusByCompositeKey(null, key);
        NullStatusException nullStatusException = assertThrows(NullStatusException.class,
                () -> paymentRepository.updatePaymentStatusByCompositeKey(null, key));
        assertEquals("status is null", nullStatusException.getMessage());
    }

    @Test
    void givenNullPaymentCompositeKey_whenUpdatePaymentStatusByCompositeId_theFail() {
        doThrow(new NullPaymentCompositeKeyException("paymentCompositeKey is null"))
                .when(paymentRepository).updatePaymentStatusByCompositeKey(Status.PROCESSED, null);
        NullPaymentCompositeKeyException exception = assertThrows(NullPaymentCompositeKeyException.class,
                () -> paymentRepository.updatePaymentStatusByCompositeKey(Status.PROCESSED, null));
        assertEquals("paymentCompositeKey is null", exception.getMessage());
    }

    @Test
    void givenPaymentCompositeKeyWithNullMessageId_whenUpdatePaymentStatusByCompositeId_theFail() {
        PaymentCompositeKey compositeKey = getPaymentCompositeKey();
        compositeKey.setMessageId(null);
        doThrow(new InvalidPaymentCompositeKeyException("messageId is null"))
                .when(paymentRepository).updatePaymentStatusByCompositeKey(Status.PROCESSED, compositeKey);
        InvalidPaymentCompositeKeyException exception = assertThrows(InvalidPaymentCompositeKeyException.class,
                () -> paymentRepository.updatePaymentStatusByCompositeKey(Status.PROCESSED, compositeKey));
        assertEquals("messageId is null", exception.getMessage());
    }

    @Test
    void givenPaymentCompositeKeyWithNullInstructionId_whenUpdatePaymentStatusByCompositeId_theFail() {
        PaymentCompositeKey compositeKey = getPaymentCompositeKey();
        compositeKey.setInstructionId(null);
        doThrow(new InvalidPaymentCompositeKeyException("instructionId is null"))
                .when(paymentRepository).updatePaymentStatusByCompositeKey(Status.PROCESSED, compositeKey);
        InvalidPaymentCompositeKeyException exception = assertThrows(InvalidPaymentCompositeKeyException.class,
                () -> paymentRepository.updatePaymentStatusByCompositeKey(Status.PROCESSED, compositeKey));
        assertEquals("instructionId is null", exception.getMessage());
    }

    @Test
    void givenPaymentCompositeKeyWithNullEndToEndId_whenUpdatePaymentStatusByCompositeId_theFail() {
        PaymentCompositeKey compositeKey = getPaymentCompositeKey();
        compositeKey.setEndToEndId(null);
        doThrow(new InvalidPaymentCompositeKeyException("endToEndId is null"))
                .when(paymentRepository).updatePaymentStatusByCompositeKey(Status.PROCESSED, compositeKey);
        InvalidPaymentCompositeKeyException exception = assertThrows(InvalidPaymentCompositeKeyException.class,
                () -> paymentRepository.updatePaymentStatusByCompositeKey(Status.PROCESSED, compositeKey));
        assertEquals("endToEndId is null", exception.getMessage());
    }

    @Test
    void givenPaymentCompositeKeyWithNullTransactionId_whenUpdatePaymentStatusByCompositeId_theFail() {
        PaymentCompositeKey compositeKey = getPaymentCompositeKey();
        compositeKey.setTransactionId(null);
        doThrow(new InvalidPaymentCompositeKeyException("transactionId is null"))
                .when(paymentRepository).updatePaymentStatusByCompositeKey(Status.PROCESSED, compositeKey);
        InvalidPaymentCompositeKeyException exception = assertThrows(InvalidPaymentCompositeKeyException.class,
                () -> paymentRepository.updatePaymentStatusByCompositeKey(Status.PROCESSED, compositeKey));
        assertEquals("transactionId is null", exception.getMessage());
    }

    @Test
    void givenPaymentCompositeKeyWithNullSettlementDate_whenUpdatePaymentStatusByCompositeId_theFail() {
        PaymentCompositeKey compositeKey = getPaymentCompositeKey();
        compositeKey.setInterBankSettlementDate(null);
        doThrow(new InvalidPaymentCompositeKeyException("settlementDate is null"))
                .when(paymentRepository).updatePaymentStatusByCompositeKey(Status.PROCESSED, compositeKey);
        InvalidPaymentCompositeKeyException exception = assertThrows(InvalidPaymentCompositeKeyException.class,
                () -> paymentRepository.updatePaymentStatusByCompositeKey(Status.PROCESSED, compositeKey));
        assertEquals("settlementDate is null", exception.getMessage());
    }

    @Test
    void givenValidInput_whenUpdatePaymentStatusByCompositeId_theSuccess() throws SQLException {
        PaymentCompositeKey compositeKey = getPaymentCompositeKey();
        paymentRepository.updatePaymentStatusByCompositeKey(Status.PROCESSED, compositeKey);
        verify(connection, times(1)).prepareStatement(anyString());
        verify(preparedStatement, times(1)).setDate(anyInt(), any(Date.class));
        verify(preparedStatement, times(5)).setString(anyInt(), anyString());
        verify(preparedStatement, times(1)).executeUpdate();
    }

    private PaymentCompositeKey getPaymentCompositeKey() {
        PaymentCompositeKey compositeKey = new PaymentCompositeKey();
        compositeKey.setMessageId("message123");
        compositeKey.setInstructionId("instruction123");
        compositeKey.setEndToEndId("endToEnd123");
        compositeKey.setTransactionId("transaction123");
        compositeKey.setInterBankSettlementDate(LocalDate.now());
        return compositeKey;
    }

}