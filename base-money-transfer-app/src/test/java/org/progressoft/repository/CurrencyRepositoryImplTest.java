package org.progressoft.repository;

import org.junit.jupiter.api.Test;
import org.progressoft.contract.CurrencyRepository;
import org.progressoft.exception.NullDataSourceException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


class CurrencyRepositoryImplTest {
    private final CurrencyRepository currencyRepository;

    public CurrencyRepositoryImplTest() throws SQLException {
        DataSource dataSource = mock(DataSource.class);
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);

        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(true, true, true, false);
        when(resultSet.getString("currency")).thenReturn("JOD", "EUR", "USD");
        this.currencyRepository = new CurrencyRepositoryImpl(dataSource);
    }

    @Test
    void givenNullConnectionFactory_whenConstructing_thenFail() {
        NullDataSourceException nullDataSourceException = assertThrows(NullDataSourceException.class, () -> new CurrencyRepositoryImpl(null));
        assertEquals("dataSource must not be null", nullDataSourceException.getMessage());
    }

    @Test
    void givenValidConnectionFactory_whenGetConnection_thenReturnResult() {
        List<String> currencies = currencyRepository.getSupportedIsoCurrencies();
        assertNotNull(currencies);
        assertFalse(currencies.isEmpty());
        String firstExpectedCurrency = getCurrency(currencies, "JOD");
        assertNotNull(firstExpectedCurrency);
        assertEquals("JOD", firstExpectedCurrency);
        String secondExpectedCurrency = getCurrency(currencies, "EUR");
        assertNotNull(secondExpectedCurrency);
        assertEquals("EUR", secondExpectedCurrency);
        String unSupportedCurrency = getCurrency(currencies, "YEN");
        assertNull(unSupportedCurrency);
    }


    private String getCurrency(List<String> currencies, String supportedCurrency) {
        Predicate<String> currenciesPredicate = currency -> currency.equals(supportedCurrency);
        Optional<String> currencyOptional = currencies.stream().filter(currenciesPredicate).findFirst();
        return currencyOptional.orElse(null);
    }
}