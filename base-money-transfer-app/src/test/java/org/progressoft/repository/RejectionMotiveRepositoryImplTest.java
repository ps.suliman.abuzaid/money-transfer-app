package org.progressoft.repository;

import org.junit.jupiter.api.Test;
import org.progressoft.contract.RejectionMotiveRepository;
import org.progressoft.entity.RejectionMotive;
import org.progressoft.exception.NullDataSourceException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


class RejectionMotiveRepositoryImplTest {

    private final RejectionMotiveRepository rejectionMotiveRepository;

    public RejectionMotiveRepositoryImplTest() throws SQLException {
        DataSource dataSource = mock(DataSource.class);
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);

        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        when(resultSet.next()).thenReturn(true, true, false);
        when(resultSet.getString("motive_code")).thenReturn("ACDE", "ACLS");
        when(resultSet.getString("motive_description")).thenReturn("Account does not exist",
                "Account is closed");
        this.rejectionMotiveRepository = new RejectionMotiveRepositoryImpl(dataSource);
    }

    @Test
    void givenNullConnectionFactory_whenConstructing_thenFail() {
        NullDataSourceException nullDataSourceException = assertThrows(NullDataSourceException.class, () -> new RejectionMotiveRepositoryImpl(null));
        assertEquals("dataSource must not be null", nullDataSourceException.getMessage());
    }

    @Test
    void givenValidConnectionFactory_whenGetConnection_thenReturnResult() {
        List<RejectionMotive> rejectionMotives = rejectionMotiveRepository.getRejectionMotives();
        assertNotNull(rejectionMotives);
        assertFalse(rejectionMotives.isEmpty());
        RejectionMotive firstExpectedRejectionMotive = getRejectionMotiveByMotiveCode(rejectionMotives, "ACDE");
        assertNotNull(firstExpectedRejectionMotive);
        assertEquals("ACDE", firstExpectedRejectionMotive.getMotiveCode());
        assertEquals("Account does not exist", firstExpectedRejectionMotive.getMotiveDescription());
        RejectionMotive secondExpectedRejectionMotive = getRejectionMotiveByMotiveCode(rejectionMotives, "ACLS");
        assertNotNull(secondExpectedRejectionMotive);
        assertEquals("ACLS", secondExpectedRejectionMotive.getMotiveCode());
        assertEquals("Account is closed", secondExpectedRejectionMotive.getMotiveDescription());

    }

    private RejectionMotive getRejectionMotiveByMotiveCode(List<RejectionMotive> rejectionMotives, String motiveCode) {
        Predicate<RejectionMotive> rejectionMotivePredicate = rejectionMotive -> rejectionMotive.getMotiveCode().equals(motiveCode);
        Optional<RejectionMotive> optionalPurposeCode = rejectionMotives.stream().filter(rejectionMotivePredicate).findFirst();
        return optionalPurposeCode.orElse(null);
    }
}