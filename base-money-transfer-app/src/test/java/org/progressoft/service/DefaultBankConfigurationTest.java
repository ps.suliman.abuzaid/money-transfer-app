package org.progressoft.service;

import org.junit.jupiter.api.Test;
import org.progressoft.contract.BankConfiguration;
import org.progressoft.entity.Participant;
import org.progressoft.entity.PurposeCode;
import org.progressoft.entity.RejectionMotive;

import java.util.Arrays;
import java.util.Currency;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DefaultBankConfigurationTest {

    private final BankConfiguration bankConfiguration;

    public DefaultBankConfigurationTest() {
        bankConfiguration = mock(DefaultBankConfiguration.class);
        setupMockData();
    }

    private void setupMockData() {
        when(bankConfiguration.getParticipants())
                .thenReturn(List.of(new Participant("JIBAJOA0", "Jordan Islamic Bank")));
        when(bankConfiguration.getPurposeCodes()).thenReturn(
                List.of(new PurposeCode("42010", "Legal Return a Payment")));
        when(bankConfiguration.getRejectionMotives())
                .thenReturn(List.of(new RejectionMotive("ACDE", "Account does not exist")));
        when(bankConfiguration.getSupportedCurrencies()).thenReturn(Arrays.asList("JOD", "USD", "EUR"));
        when(bankConfiguration.getPurposeCodeByCode("42010"))
                .thenReturn(new PurposeCode("41010", "Individual Return a Payment"));
        when(bankConfiguration.getParticipantByBic("JDIBJOA0"))
                .thenReturn(new Participant("SAFWA ISLAMIC BANK", "JDIBJOA0"));
        when(bankConfiguration.getRejectionMotiveByCode("ACLS"))
                .thenReturn(new RejectionMotive("ACLS", "Account is closed"));
        when(bankConfiguration.getCurrencyByIsoCurrency("JOD")).thenReturn(Currency.getInstance("JOD"));
        when(bankConfiguration.getCurrencyByIsoCurrency("YEN")).thenReturn(null);
        when(bankConfiguration.getCurrentParticipantBicFi()).thenReturn("HBHOJOA0");
        when(bankConfiguration.getDefaultRejectionCode()).thenReturn("AC02");
    }

    @Test
    void whenGetParticipants_thenReturnList() {
        List<Participant> participants = bankConfiguration.getParticipants();
        assertNotNull(participants, "Must not be null");
        assertFalse(participants.isEmpty());
        for (Participant participant : participants) {
            assertNotNull(participant, "Participant must not be null");
        }
    }

    @Test
    void givenBic_whenGetParticipant_thenReturnExpectedParticipant() {
        Participant jib = bankConfiguration.getParticipantByBic("JDIBJOA0");
        assertNotNull(jib, "Expected participant not returned");
        assertEquals("SAFWA ISLAMIC BANK", jib.getName());
        assertEquals("JDIBJOA0", jib.getBic());
    }

    @Test
    void whenGetPurposeCodes_thenReturnList() {
        List<PurposeCode> purposeCodes = bankConfiguration.getPurposeCodes();
        assertNotNull(purposeCodes, "Must not be null");
        assertFalse(purposeCodes.isEmpty());
        for (PurposeCode purposeCode : purposeCodes) {
            assertNotNull(purposeCode, "PurposeCode must not be null");
        }
    }

    @Test
    void givenCode_whenGetPurposeCode_thenReturnExpectedPurposeCode() {
        PurposeCode expectedPurposeCode = bankConfiguration.getPurposeCodeByCode("42010");
        assertNotNull(expectedPurposeCode);
        assertEquals("41010", expectedPurposeCode.getCode());
        assertEquals("Individual Return a Payment", expectedPurposeCode.getUsage());
    }

    @Test
    void whenGetRejectionMotives_thenReturnList() {
        List<RejectionMotive> rejectionMotives = bankConfiguration.getRejectionMotives();
        assertNotNull(rejectionMotives, "Must not be null");
        assertFalse(rejectionMotives.isEmpty());
        for (RejectionMotive rejectionMotive : rejectionMotives) {
            assertNotNull(rejectionMotive, "RejectionMotive must not be null");
        }
    }

    @Test
    void givenCode_whenGetRejectionMotiveByCode_thenReturnExpectedRejectionMotive() {
        RejectionMotive expectedRejectionMotive = bankConfiguration.getRejectionMotiveByCode("ACLS");
        assertNotNull(expectedRejectionMotive);
        assertEquals("ACLS", expectedRejectionMotive.getMotiveCode());
        assertEquals("Account is closed", expectedRejectionMotive.getMotiveDescription());
    }

    @Test
    void whenGetSupportedIsoCurrencies_thenReturnList() {
        List<String> supportedCurrencies = bankConfiguration.getSupportedCurrencies();
        assertNotNull(supportedCurrencies, "Must not be null");
        assertFalse(supportedCurrencies.isEmpty());
        for (String supportedCurrency : supportedCurrencies) {
            assertNotNull(supportedCurrency, "Supported Currency Code must not be null");
            try {
                Currency.getInstance(supportedCurrency);
            } catch (IllegalArgumentException e) {
                fail("currencyCode is not a supported ISO 4217 code");
            }
        }
    }

    @Test
    void givenUnsupportedCurrencyIsoCode_whenGetCurrency_thenFail() {
        Currency currency = bankConfiguration.getCurrencyByIsoCurrency("YEN");
        assertNull(currency, "Must be null");
    }

    @Test
    void givenSupportedCurrencyIsoCode_whenGetCurrency_thenSuccess() {
        Currency currency = bankConfiguration.getCurrencyByIsoCurrency("JOD");
        assertNotNull(currency, "Must not be null");
        assertEquals("JOD", currency.getCurrencyCode());
    }

    @Test
    void whenGetCurrentParticipantBicFi_thenReturnResult() {
        String currentBic = bankConfiguration.getCurrentParticipantBicFi();
        assertNotNull(currentBic);
        assertEquals("HBHOJOA0", currentBic);
    }

    @Test
    void whenGetDefaultRejectionCode_thenReturnResult() {
        String defaultRejectionCode = bankConfiguration.getDefaultRejectionCode();
        assertNotNull(defaultRejectionCode);
        assertEquals("AC02", defaultRejectionCode);
    }


}