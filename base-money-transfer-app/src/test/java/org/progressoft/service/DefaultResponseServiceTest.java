package org.progressoft.service;

import org.junit.jupiter.api.Test;
import org.mockito.InOrder;
import org.progressoft.contract.*;
import org.progressoft.entity.Payment;
import org.progressoft.entity.PaymentCompositeKey;
import org.progressoft.entity.ResponseData;
import org.progressoft.enums.Reason;
import org.progressoft.enums.Status;
import org.progressoft.exception.XmlBodyValidationException;

import java.time.LocalDate;

import static org.mockito.Mockito.*;

class DefaultResponseServiceTest {
    private final PaymentRepository paymentRepository = mock(PaymentRepository.class);
    private final ReplyRepository replyRepository = mock(ReplyRepository.class);
    private final AccountCreditorService accountCreditorService = mock(AccountCreditorService.class);
    private final NotificationService notificationService = mock(NotificationService.class);
    private final ResponseParser responseParser = mock(ResponseParser.class);
    private final ErrorHandler errorHandler = mock(ErrorHandler.class);
    private final ResponseService responseService = new DefaultResponseService(paymentRepository, replyRepository,
            accountCreditorService, notificationService,
            errorHandler, responseParser);

    @Test
    void givenInvalidXmlResponse_whenProcessResponse_thenFail() {
        String invalidXmlResponse = "<response></response>";
        String errorMessage = "Unknown element 'response'";
        when(responseParser.parseResponse(invalidXmlResponse))
                .thenThrow(new XmlBodyValidationException(new RuntimeException(invalidXmlResponse), errorMessage
                ));
        responseService.processResponse(invalidXmlResponse);
        InOrder inOrder = inOrder(responseParser, errorHandler);
        inOrder.verify(responseParser).parseResponse(invalidXmlResponse);
        inOrder.verify(errorHandler).dispatchError(invalidXmlResponse, errorMessage);
        verify(errorHandler).dispatchError(invalidXmlResponse, errorMessage);
    }

    @Test
    void givenXmlForNAutResponse_whenProcessResponse_thenSuccess() {
        String validXml = "valid naut xml";
        ResponseData responseData = getRejectedResponseData();
        Status paymentStatus = Status.REJECTED;
        PaymentCompositeKey key = getPaymentCompositeKey(responseData);
        prepareNAutMockScenario(validXml, responseData, paymentStatus, key);
        responseService.processResponse(validXml);
        verifyMocks(validXml, paymentStatus, key, responseData);

        verifyNAutResponseOrder(validXml, key, responseData);
    }

    @Test
    void givenXmlForAuthResponse_whenProcessResponse_thenSuccess() {
        String validXml = "valid auth xml";
        ResponseData responseData = getAcceptedResponseData();
        Status paymentStatus = Status.PROCESSED;
        Payment payment = new Payment();
        PaymentCompositeKey key = getPaymentCompositeKey(responseData);
        prepareAuthMockScenario(validXml, responseData, paymentStatus, key, payment);
        responseService.processResponse(validXml);
        verifyMocks(validXml, paymentStatus, key, responseData);
        verify(paymentRepository).getPaymentByCompositeId(key);
        verify(accountCreditorService).creditAccount(payment);
        verify(notificationService).notifyAccount(payment);

    }

    private void prepareAuthMockScenario(String validXml, ResponseData responseData, Status paymentStatus, PaymentCompositeKey key, Payment payment) {
        prepareNAutMockScenario(validXml, responseData, paymentStatus, key);
        when(paymentRepository.getPaymentByCompositeId(key)).thenReturn(payment);
        doNothing().when(accountCreditorService).creditAccount(payment);
        doNothing().when(notificationService).notifyAccount(payment);
    }

    private void prepareNAutMockScenario(String validXml, ResponseData responseData, Status paymentStatus, PaymentCompositeKey key) {
        when(responseParser.parseResponse(validXml)).thenReturn(responseData);
        doNothing().when(paymentRepository).updatePaymentStatusByCompositeKey(paymentStatus, key);
        doNothing().when(replyRepository).updateReplyStatusAndReasonById(responseData.getMessageId(),
                paymentStatus, Reason.ACSP);
    }

    private void verifyMocks(String validXml, Status paymentStatus, PaymentCompositeKey key, ResponseData responseData) {
        verify(responseParser).parseResponse(validXml);
        verify(paymentRepository).updatePaymentStatusByCompositeKey(eq(paymentStatus), eq(key));
        verify(replyRepository).updateReplyStatusAndReasonById(responseData.getMessageId(),
                Status.PROCESSED, responseData.getReason());
    }

    private void verifyNAutResponseOrder(String validXml, PaymentCompositeKey key, ResponseData responseData) {
        InOrder inOrder = inOrder(responseParser, paymentRepository, replyRepository);
        inOrder.verify(responseParser).parseResponse(validXml);
        inOrder.verify(paymentRepository).updatePaymentStatusByCompositeKey(Status.REJECTED,
                key);
        inOrder.verify(replyRepository).updateReplyStatusAndReasonById(responseData.getMessageId(),
                Status.PROCESSED, responseData.getReason());
    }

    private PaymentCompositeKey getPaymentCompositeKey(ResponseData responseData) {
        String messageId = responseData.getPaymentMessageId();
        String instructionId = responseData.getInstructionId();
        String endToEndId = responseData.getEndToEndId();
        String transactionId = responseData.getTransactionId();
        LocalDate settlementDate = responseData.getSettlementDate();
        return new PaymentCompositeKey(messageId, instructionId, endToEndId,
                transactionId, settlementDate);
    }

    private ResponseData getRejectedResponseData() {
        ResponseData data = new ResponseData();
        data.setMessageId("ResponseId123");
        data.setInstructionId("Instruction1234");
        data.setEndToEndId("EndToEnd123");
        data.setTransactionId("Transaction123");
        data.setPaymentMessageId("PaymentId123");
        data.setSettlementDate(LocalDate.now());
        data.setReason(Reason.RJCT);
        return data;
    }

    private ResponseData getAcceptedResponseData() {
        ResponseData data = getRejectedResponseData();
        data.setReason(Reason.ACSP);
        return data;
    }
}