package org.progressoft.service;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.progressoft.contract.*;
import org.progressoft.entity.Payment;
import org.progressoft.entity.RejectionMotive;
import org.progressoft.exception.XmlBodyValidationException;
import org.progressoft.factory.PaymentFactory;
import org.progressoft.mapper.JacksonPaymentParser;
import org.progressoft.repository.PaymentRepositoryImpl;
import org.progressoft.validator.DefaultPaymentValidator;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


public class DefaultPaymentRequestServiceTest {

    private final PaymentParser paymentParser;
    private final PaymentValidator paymentValidator;
    private final ReplyService replyService;
    private final PaymentRepository paymentRepository;
    private final PaymentRequestService paymentRequestService;

    public DefaultPaymentRequestServiceTest() {
        paymentParser = mock(JacksonPaymentParser.class);
        paymentValidator = mock(DefaultPaymentValidator.class);
        replyService = mock(DefaultReplyService.class);
        paymentRepository = mock(PaymentRepositoryImpl.class);
        paymentRequestService = new DefaultPaymentRequestService(paymentParser, paymentValidator,
                replyService, paymentRepository);
    }

    @Test
    void givenXmlStringForValidPayment_whenProcessRequest_thenSuccess() {
        Payment validPayment = PaymentFactory.getValidPayment();
        Set<RejectionMotive> rejectionMotives = new HashSet<>();
        testProcessRequest(validPayment, rejectionMotives);
    }

    @Test
    void givenXmlStringForInvalidPayment_whenProcessRequest_thenSuccess() {
        Payment invalidPayment = PaymentFactory.getInvalidPayment();
        Set<RejectionMotive> rejectionMotives = new HashSet<>();
        rejectionMotives.add(new RejectionMotive("AC02",
                "Missing or Invalid Debtor name"));
        rejectionMotives.add(new RejectionMotive("AC02", "Missing or Invalid Amount"));
        testProcessRequest(invalidPayment, rejectionMotives);
    }

    @Test
    void givenInvalidXml_whenProcessRequest_thenFail() {
        ArgumentCaptor<Payment> argumentCaptor = ArgumentCaptor.forClass(Payment.class);
        Set<RejectionMotive> rejectionMotives = new HashSet<>();
        String invalidXmlPayment = "<root></root>";
        String errorMessage = "Invalid element 'root'";
        when(paymentParser.parse(invalidXmlPayment)).thenThrow(new XmlBodyValidationException(new RuntimeException(errorMessage), errorMessage));
        XmlBodyValidationException exception = assertThrows(XmlBodyValidationException.class,
                () -> paymentRequestService.processRequest(invalidXmlPayment));
        assertEquals("Invalid element 'root'", exception.getMessage());
        verify(paymentParser).parse(invalidXmlPayment);
        verify(paymentValidator, times(0)).validatePayment(any(Payment.class));
        verify(paymentRepository, times(0)).savePayment(argumentCaptor.capture());
        verify(replyService, times(0)).sendReply(any(Payment.class), eq(rejectionMotives));
    }


    private void testProcessRequest(Payment payment, Set<RejectionMotive> rejectionMotives) {
        ArgumentCaptor<Payment> argumentCaptor = ArgumentCaptor.forClass(Payment.class);
        when(paymentParser.parse(anyString())).thenReturn(payment);
        when(paymentValidator.validatePayment(payment)).thenReturn(rejectionMotives);
        when(replyService.sendReply(payment, rejectionMotives)).thenReturn("Valid Reply Xml");
        String xmlReply = paymentRequestService.processRequest(anyString());
        assertNotNull(xmlReply);
        assertEquals("Valid Reply Xml", xmlReply);
        verifyExecutionOrder(payment, rejectionMotives);
        verify(paymentRepository, times(1)).savePayment(argumentCaptor.capture());
        verify(replyService, times(1)).sendReply(payment, rejectionMotives);
        assertEquals(payment, argumentCaptor.getValue());
    }

    private void verifyExecutionOrder(Payment payment, Set<RejectionMotive> rejectionMotives) {
        InOrder inOrder = inOrder(paymentParser, paymentValidator,
                paymentRepository, replyService);
        inOrder.verify(paymentParser).parse(anyString());
        inOrder.verify(paymentValidator).validatePayment(payment);
        inOrder.verify(paymentRepository).savePayment(payment);
        inOrder.verify(replyService).sendReply(payment, rejectionMotives);
    }

}
