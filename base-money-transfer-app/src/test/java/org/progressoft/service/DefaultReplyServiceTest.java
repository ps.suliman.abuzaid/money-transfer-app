package org.progressoft.service;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.progressoft.contract.*;
import org.progressoft.entity.Payment;
import org.progressoft.entity.Reply;
import org.progressoft.entity.ReplyBody;
import org.progressoft.enums.Reason;
import org.progressoft.enums.Status;
import org.progressoft.exception.NullReplyBodyException;
import org.progressoft.exception.NullReplyException;
import org.progressoft.exception.NullXmlReplyException;
import org.progressoft.factory.PaymentFactory;
import org.progressoft.mapper.DefaultPaymentToReplyBodyMapper;
import org.progressoft.repository.ReplyRepositoryImpl;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class DefaultReplyServiceTest {

    private final PaymentToReplyBodyMapper paymentToReplyBodyMapper;
    private final MessageDispatcher messageDispatcher;
    private final ReplyRepository replyRepository;
    private final ReplyService replyService;

    public DefaultReplyServiceTest() {
        MessageIdGenerator generator = () -> UUID.randomUUID().toString();
        paymentToReplyBodyMapper = mock(DefaultPaymentToReplyBodyMapper.class, withSettings().useConstructor(generator));
        messageDispatcher = mock(DefaultMessageDispatcher.class);
        replyRepository = mock(ReplyRepositoryImpl.class);
        replyService = new DefaultReplyService(paymentToReplyBodyMapper,
                messageDispatcher, replyRepository);
    }

    @Test
    void givenValidPayment_whenSendReply_thenSuccess() {
        Reply reply = getReply();
        Payment payment = PaymentFactory.getValidPayment();
        ReplyBody replyBody = new ReplyBody("<xml></xml>", reply);
        ArgumentCaptor<Reply> replyArgumentCaptor = ArgumentCaptor.forClass(Reply.class);
        ArgumentCaptor<String> xmlReplyCaptor = ArgumentCaptor.forClass(String.class);
        when(paymentToReplyBodyMapper.convertPaymentToReplyBody(any(Payment.class), anySet()))
                .thenReturn(replyBody);
        replyService.sendReply(payment, Set.of());
        verify(messageDispatcher).dispatchMessage(xmlReplyCaptor.capture());
        verify(replyRepository).saveReply(replyArgumentCaptor.capture());
        verifyExecutionOrder(payment, reply);
        Reply savedReply = replyArgumentCaptor.getValue();
        String dispatchedXmlReply = xmlReplyCaptor.getValue();
        assertEquals(reply, savedReply);
        assertEquals(replyBody.getXmlReply(), dispatchedXmlReply);
    }

    private void verifyExecutionOrder(Payment payment, Reply reply) {
        InOrder inOrder = inOrder(paymentToReplyBodyMapper, messageDispatcher, replyRepository);
        inOrder.verify(paymentToReplyBodyMapper).convertPaymentToReplyBody(payment, Set.of());
        inOrder.verify(messageDispatcher).dispatchMessage(anyString());
        inOrder.verify(replyRepository).saveReply(reply);
    }

    @Test
    void givenValidPayment_whenSendReply_thenThrowExceptionIfReplyBodyIsNull() {
        when(paymentToReplyBodyMapper.convertPaymentToReplyBody(any(Payment.class), anySet()))
                .thenReturn(null);
        NullReplyBodyException exception = assertThrows(NullReplyBodyException.class,
                () -> replyService.sendReply(PaymentFactory.getValidPayment(), Set.of()));
        assertEquals("replyBody is null", exception.getMessage());
        verify(messageDispatcher, times(0)).dispatchMessage(anyString());
        verify(replyRepository, times(0)).saveReply(any(Reply.class));
    }

    @Test
    void givenValidPayment_whenSendReply_thenThrowExceptionIfReplyBodyXmlReplyIsNull() {
        Reply reply = getReply();
        ReplyBody replyBody = new ReplyBody(null, reply);
        when(paymentToReplyBodyMapper.convertPaymentToReplyBody(any(Payment.class), anySet()))
                .thenReturn(replyBody);
        NullXmlReplyException exception = assertThrows(NullXmlReplyException.class,
                () -> replyService.sendReply(PaymentFactory.getValidPayment(), Set.of()));
        assertEquals("xmlReply is null", exception.getMessage());
        verify(messageDispatcher, times(0)).dispatchMessage(anyString());
        verify(replyRepository, times(0)).saveReply(null);
    }

    @Test
    void givenValidPayment_whenSendReply_thenThrowExceptionIfReplyInReplyBodyIsNull() {
        ReplyBody replyBody = new ReplyBody("<xml></xml>", null);
        when(paymentToReplyBodyMapper.convertPaymentToReplyBody(any(Payment.class), anySet()))
                .thenReturn(replyBody);
        NullReplyException exception = assertThrows(NullReplyException.class,
                () -> replyService.sendReply(PaymentFactory.getValidPayment(), Set.of()));
        assertEquals("reply is null", exception.getMessage());
        verify(messageDispatcher, times(0)).dispatchMessage(anyString());
        verify(replyRepository, times(0)).saveReply(null);
    }


    private Reply getReply() {
        Reply reply = new Reply();
        reply.setMessageId(UUID.randomUUID().toString());
        reply.setPaymentMessageId(UUID.randomUUID().toString());
        reply.setCreationDateTime(LocalDateTime.now());
        reply.setStatus(Status.PENDING_RESPONSE);
        reply.setReason(Reason.AUTH);
        return reply;
    }

}
