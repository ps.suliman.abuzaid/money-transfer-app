package org.progressoft.service;

import org.junit.jupiter.api.Test;
import org.progressoft.contract.EnvironmentPropertyService;
import org.progressoft.exception.NullPropertyKeyException;
import org.progressoft.factory.DefaultBeanFactory;

import static org.junit.jupiter.api.Assertions.*;

class DefaultEnvironmentPropertyServiceTest {

    private final EnvironmentPropertyService propertyService = new DefaultBeanFactory().getEnvironmentPropertyService();

    @Test
    void givenNullKey_whenGetProperty_thenFail() {
        NullPropertyKeyException propertyKeyException = assertThrows(NullPropertyKeyException.class,
                () -> propertyService.getProperty(null));
        assertEquals("property key is null", propertyKeyException.getMessage());
    }

    @Test
    void givenEmptyOrNonExistentKey_whenGetProperty_thenReturnNull() {
        String emptyKeyValue = propertyService.getProperty("");
        assertNull(emptyKeyValue);
        String nonExistentKeyValue = propertyService.getProperty("fake.property");
        assertNull(nonExistentKeyValue);
    }

    @Test
    void givenValidKey_whenGetProperty_thenReturnValue() {
        String value = propertyService.getProperty("bank.default.bic");
        assertNotNull(value);
        assertEquals("HBHOJOA0", value);
    }
}