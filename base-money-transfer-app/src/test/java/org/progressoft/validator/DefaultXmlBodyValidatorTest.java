package org.progressoft.validator;

import org.junit.jupiter.api.Test;
import org.progressoft.contract.XmlBodyValidator;
import org.progressoft.exception.InvalidXmlBodyException;
import org.progressoft.exception.NullXmlBodyException;
import org.progressoft.exception.XmlBodyValidationException;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class DefaultXmlBodyValidatorTest {

    private final XmlBodyValidator xmlBodyValidator;

    public DefaultXmlBodyValidatorTest() {
        Path path = Paths.get("src", "main", "resources", "schema", "payment_schema.xsd");
        xmlBodyValidator = new DefaultXmlBodyValidator(path);
    }

    @Test
    void givenNullXmlString_whenParse_thenFail() {
        NullXmlBodyException nullXmlBodyException = assertThrows(NullXmlBodyException.class,
                () -> xmlBodyValidator.validateXmlBody(null));
        assertEquals("Xml string is null", nullXmlBodyException.getMessage());
    }

    @Test
    void givenEmptyXmlString_whenParse_thenFail() {
        InvalidXmlBodyException xmlPaymentException = assertThrows(InvalidXmlBodyException.class,
                () -> xmlBodyValidator.validateXmlBody(""));
        assertEquals("Xml string is empty", xmlPaymentException.getMessage());
    }

    @Test
    void givenInvalidXmlString_whenParse_thenReturnFalse() {
        XmlBodyValidationException xmlBodyValidationException = assertThrows(XmlBodyValidationException.class,
                () -> xmlBodyValidator.validateXmlBody("<root>Hello</root>"));
        assertEquals("cvc-elt.1.a: Cannot find the declaration of element 'root'.",
                xmlBodyValidationException.getMessage());
    }

}