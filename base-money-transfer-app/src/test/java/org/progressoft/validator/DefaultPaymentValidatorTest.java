package org.progressoft.validator;

import org.junit.jupiter.api.Test;
import org.progressoft.contract.AccountValidator;
import org.progressoft.contract.BankConfiguration;
import org.progressoft.contract.PaymentValidator;
import org.progressoft.entity.Participant;
import org.progressoft.entity.Payment;
import org.progressoft.entity.PurposeCode;
import org.progressoft.entity.RejectionMotive;
import org.progressoft.factory.PaymentFactory;
import org.progressoft.service.DefaultBankConfiguration;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DefaultPaymentValidatorTest {

    private final PaymentValidator validPaymentValidator;
    private final PaymentValidator invalidPaymentValidator;

    DefaultPaymentValidatorTest() {
        BankConfiguration bankConfiguration = mock(DefaultBankConfiguration.class);
        when(bankConfiguration.getDefaultRejectionCode()).thenReturn("AC02");
        when(bankConfiguration.getCurrentParticipantBicFi()).thenReturn("HBHOJOA0");
        when(bankConfiguration.getCurrencyByIsoCurrency("JOD")).thenReturn(Currency.getInstance("JOD"));
        when(bankConfiguration.getParticipantByBic("HBHOJOA0")).thenReturn(new Participant("The Housing Bank for Trade and Finance", "HBHOJOA0"));
        when(bankConfiguration.getParticipantByBic("JGBAJOA0")).thenReturn(new Participant("Jordan Commercial Bank", "JGBAJOA0"));
        when(bankConfiguration.getPurposeCodeByCode("11110")).thenReturn(new PurposeCode("11110", "individual Transfer through Mobile Banking " +
                "to Alias/IBAN to Individual (P2P)"));
        when(bankConfiguration.getRejectionMotiveByCode("AC02")).thenReturn(new RejectionMotive("AC02", "Debtor account number invalid or missing"));
        when(bankConfiguration.getRejectionMotiveByCode("AC03")).thenReturn(new RejectionMotive("AC03", "Creditor account number invalid or missing"));
        when(bankConfiguration.getRejectionMotiveByCode("FF03")).thenReturn(new RejectionMotive("AC03", "Payment Type Information is missing or invalid"));
        when(bankConfiguration.getRejectionMotiveByCode("ACDM")).thenReturn(new RejectionMotive("ACDM", "Account owner details inadequate or missing"));
        this.validPaymentValidator = new DefaultPaymentValidator(bankConfiguration, accountRequest -> new HashSet<>());
        this.invalidPaymentValidator = new DefaultPaymentValidator(bankConfiguration, getInvalidMockAccountValidator());
    }

    @Test
    void givenInvalidPaymentWithNullFields_whenValidatePayment_thenFail() {
        Payment payment = new Payment();
        Set<RejectionMotive> rejectionMotives = validPaymentValidator.validatePayment(payment);
        List<String> expectedCodes = Arrays.asList("AC02", "AC03", "BE20", "FF03", "ACDM");
        List<String> expectedDescriptions = getAllExpectedRejectionDescriptions();
        assertNotNull(rejectionMotives, "Must not be null");
        assertFalse(rejectionMotives.isEmpty());
        for (RejectionMotive motive : rejectionMotives) {
            assertMotive(motive);
            if (!expectedCodes.contains(motive.getMotiveCode())) {
                fail("Unexpected Motive Code: " + motive.getMotiveCode());
            }
            if (!expectedDescriptions.contains(motive.getMotiveDescription())) {
                fail("Unexpected Motive Description: " + motive.getMotiveDescription());
            }
        }
    }

    private void assertMotive(RejectionMotive motive) {
        assertNotNull(motive, "Must not be null");
        assertNotNull(motive.getMotiveCode());
        assertNotNull(motive.getMotiveDescription());
        assertFalse(motive.getMotiveCode().isEmpty());
        assertFalse(motive.getMotiveDescription().isEmpty());
    }

    private List<String> getAllExpectedRejectionDescriptions() {
        return Arrays.asList("Missing Message Id", "Missing Creation Date And Time",
                "Missing or Invalid Number of Transactions", "Missing or Invalid Settlement Method", "Missing Instructing Id",
                "Missing EndToEnd Id", "Missing Transaction Id", "Missing Or Invalid Debtor Name",
                "Instructed Participant is Missing or not the same as Current Participant",
                "Creditor Agent is missing or not the same as Instructed Agent",
                "Debtor Agent is missing or not the same as Instructing Agent",
                "Missing Or Invalid Agent", "Missing or Invalid Settlement Amount",
                "Missing Or Invalid Currency", "Missing or Invalid Settlement Date",
                "Account owner details inadequate or missing", "Invalid or Missing Debtor AddressLines",
                "Creditor account number invalid or missing", "Payment Type Information is missing or invalid",
                "Debtor account number invalid or missing", "Missing or Invalid Charge Bearer");
    }

    @Test
    void givenInvalidPaymentWithInvalidAccountValidator_whenValidatePayment_thenFail() {
        Payment payment = new Payment();
        Set<RejectionMotive> rejectionMotives = invalidPaymentValidator.validatePayment(payment);
        List<String> expectedCodes = Arrays.asList("AC02", "AC03", "FF03", "ACDM", "ACLS", "CUDC");
        List<String> expectedDescriptions = Arrays.asList("Account is closed", "Customer reported deceased");
        assertNotNull(rejectionMotives, "Must not be null");
        assertFalse(rejectionMotives.isEmpty());
        for (RejectionMotive motive : rejectionMotives) {
            assertMotive(motive);
            if (!expectedCodes.contains(motive.getMotiveCode())) {
                fail("Unexpected Motive Code: " + motive.getMotiveCode());
            }
            if ((motive.getMotiveCode().equals("ACLS") || motive.getMotiveCode().equals("CUBC")) &&
                    !expectedDescriptions.contains(motive.getMotiveDescription())) {
                fail("Unexpected Motive Description: " + motive.getMotiveDescription());
            }
        }
    }

    @Test
    void givenValidPaymentWithEmptyFields_whenValidatePayment_thenFail() {
        Payment payment = PaymentFactory.getValidPayment();
        payment.setInstructionId("");
        payment.setDebtorAccountIban("");
        payment.setClearingChannel("");
        List<String> expectedCodes = Arrays.asList("AC02", "AC03", "FF03");
        List<String> expectedDescriptions = getAllExpectedRejectionDescriptions();
        Set<RejectionMotive> rejectionMotives = validPaymentValidator.validatePayment(payment);
        assertNotNull(rejectionMotives, "Must not be null");
        assertFalse(rejectionMotives.isEmpty());
        for (RejectionMotive motive : rejectionMotives) {
            assertMotive(motive);
            if (!expectedCodes.contains(motive.getMotiveCode())) {
                fail("Unexpected Motive Code: " + motive.getMotiveCode());
            }
            if (!expectedDescriptions.contains(motive.getMotiveDescription())) {
                fail("Unexpected Motive Description: " + motive.getMotiveDescription());
            }
        }
    }


    @Test
    void givenValidPayment_whenValidatePayment_thenReturnEmptyList() {
        Payment payment = PaymentFactory.getValidPayment();
        Set<RejectionMotive> rejectionMotives = validPaymentValidator.validatePayment(payment);
        assertNotNull(rejectionMotives, "Must not be null");
        assertTrue(rejectionMotives.isEmpty());
    }

    @Test
    void givenPaymentWithInvalidNameAndAddressLines_whenValidatePayment_thenFail() {
        Payment payment = PaymentFactory.getValidPayment();
        payment.setDebtorName("Ahmed Khaled");
        payment.setCreditorAddressLines(List.of("Amman"));
        Set<RejectionMotive> rejectionMotives = validPaymentValidator.validatePayment(payment);
        assertNotNull(rejectionMotives, "Must not be null");
        assertFalse(rejectionMotives.isEmpty());
        assertEquals(2, rejectionMotives.size());
    }

    private AccountValidator getInvalidMockAccountValidator() {
        return accountRequest -> new HashSet<>(Arrays.asList(
                new RejectionMotive("ACLS", "Account is closed"),
                new RejectionMotive("CUDC", "Customer reported deceased")
        ));
    }

}