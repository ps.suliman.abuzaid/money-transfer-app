package org.progressoft.mapper;

import org.junit.jupiter.api.Test;
import org.progressoft.entity.jackson.Proprietary;
import org.progressoft.entity.jackson.payment.*;
import org.progressoft.exception.InvalidSchemaException;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class XmlPaymentConverterTest {
    private final XmlToJacksonPaymentMapper xmlToJacksonPaymentMapper = new XmlToJacksonPaymentMapper();

    @Test
    void givenInvalidXmlString_whenConvertXmlToObject_thenFail() {
        InvalidSchemaException invalidSchemaException = assertThrows(InvalidSchemaException.class,
                () -> xmlToJacksonPaymentMapper.convertXmlToObject("<root>Hello</root>"));
        assertEquals("Xml string schema does not match expected schema", invalidSchemaException.getMessage());
    }

    @Test
    void givenXmlStringWithMissingInstructingAgent_whenConvertXmlToObject_thenReturnObjectWithNullInstructingAgent() throws IOException {
        Path pathToXmlFile = Paths.get("src", "test", "resources", "pacs.008-test-incomplete.xml");
        String xmlString = Files.readString(pathToXmlFile);
        JacksonXmlPayment jacksonXmlPayment = xmlToJacksonPaymentMapper.convertXmlToObject(xmlString);
        assertNotNull(jacksonXmlPayment, "Must not be null");
        assertNull(jacksonXmlPayment.getTransferTransactionInfo().getInstructingAgent());
    }

    @Test
    void givenValidXmlString_whenConvertXmlToObject_thenReturnResult() throws IOException {
        Path pathToXmlFile = Paths.get("src", "test", "resources", "pacs.008-test.xml");
        String xmlString = Files.readString(pathToXmlFile);
        JacksonXmlPayment jacksonXmlPayment = xmlToJacksonPaymentMapper.convertXmlToObject(xmlString);
        assertNotNull(jacksonXmlPayment, "Must not be null");
        assertGroupHeader(jacksonXmlPayment);
        assertCreditTransferTransactionInfo(jacksonXmlPayment);
    }

    private void assertCreditTransferTransactionInfo(JacksonXmlPayment jacksonXmlPayment) {
        CreditTransferTransactionInfo transactionInfo = jacksonXmlPayment.getTransferTransactionInfo();
        assertNotNull(transactionInfo);
        assertPaymentId(transactionInfo);
        assertPaymentTypeInfo(transactionInfo);
        assertSettlementAmount(transactionInfo);
        assertEquals(LocalDate.of(2023, 9, 26), transactionInfo.getInterBankSettlementDate());
        assertEquals("SLEV", transactionInfo.getChargeBearer());
        assertAgent(transactionInfo.getInstructingAgent(), "JGBAJOA0");
        assertAgent(transactionInfo.getInstructedAgent(), "HBHOJOA0");
        assertAgent(transactionInfo.getDebtorAgent(), "JGBAJOA0");
        assertAgent(transactionInfo.getCreditorAgent(), "HBHOJOA0");
        assertClient(transactionInfo.getDebtor(), "NISREEN MOHAMMAD YOUSEF", List.of("new zarqa"));
        assertClient(transactionInfo.getCreditor(), "MAEN SAMI HATTAR SALEM", List.of("Amman Jordan SLT JO SLT"));
        assertAccount(transactionInfo.getDebtorAccount(), "JO93JGBA6010000290450010010000");
        assertAccount(transactionInfo.getCreditorAccount(), "JO83HBHO0320000033330600101001");
    }

    private void assertAccount(Account account, String expectedIban) {
        assertNotNull(account);
        Account.AccountId id = account.getId();
        assertNotNull(id);
        assertEquals(expectedIban, id.getIban());
    }

    private void assertClient(Client client, String expectedName, List<String> expectedAddressLines) {
        assertNotNull(client);
        assertEquals(expectedName, client.getName());
        Client.PostalAddress address = client.getPostalAddress();
        assertNotNull(address);
        assertEquals(expectedAddressLines, address.getAddressLines());
    }

    private void assertAgent(Agent agent, String bicFi) {
        assertNotNull(agent);
        Agent.FinancialInstitutionId institutionId = agent.getInstitutionId();
        assertNotNull(institutionId);
        assertEquals(bicFi, institutionId.getBicFi());
    }

    private void assertSettlementAmount(CreditTransferTransactionInfo transactionInfo) {
        InterBankSettlementAmount settlementAmount = transactionInfo.getSettlementAmount();
        assertNotNull(settlementAmount);
        assertEquals("JOD", settlementAmount.getCurrency());
        assertEquals(new BigDecimal("23.000"), settlementAmount.getValue());
    }

    private void assertPaymentTypeInfo(CreditTransferTransactionInfo transactionInfo) {
        PaymentTypeInfo typeInfo = transactionInfo.getPaymentTypeInfo();
        assertNotNull(typeInfo);
        Proprietary serviceLevel = typeInfo.getServiceLevel();
        Proprietary localInstrument = typeInfo.getLocalInstrument();
        Proprietary categoryPurpose = typeInfo.getCategoryPurpose();
        assertNotNull(serviceLevel);
        assertNotNull(localInstrument);
        assertNotNull(categoryPurpose);
        assertEquals("RTNS", typeInfo.getClearingChannel());
        assertEquals("0100", serviceLevel.getProprietary());
        assertEquals("CSDC", localInstrument.getProprietary());
        assertEquals("11110", categoryPurpose.getProprietary());
    }

    private void assertPaymentId(CreditTransferTransactionInfo transactionInfo) {
        PaymentId paymentId = transactionInfo.getPaymentId();
        assertEquals("JGBA2812094750927334298", paymentId.getInstructionId());
        assertEquals("NOTPROVIDED", paymentId.getEndToEndId());
        assertEquals("JGBA2812094750927334297", paymentId.getTransactionId());
    }

    private void assertGroupHeader(JacksonXmlPayment jacksonXmlPayment) {
        GroupHeader groupHeader = jacksonXmlPayment.getGroupHeader();
        assertNotNull(groupHeader);
        assertEquals("JGBA2812094750927334298", groupHeader.getMessageId());
        assertEquals(LocalDateTime.of(2020, 12, 28, 7, 47, 50),
                groupHeader.getCreationDateTime());
        assertEquals(1, groupHeader.getNumberOfTransactions());
        assertEquals("CLRG", groupHeader.getSettlementInfo().getSettlementMethodCode());
    }
}
