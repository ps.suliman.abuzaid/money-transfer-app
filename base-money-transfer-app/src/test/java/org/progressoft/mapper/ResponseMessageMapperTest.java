package org.progressoft.mapper;

import org.junit.jupiter.api.Test;
import org.progressoft.entity.jackson.Proprietary;
import org.progressoft.entity.jackson.message.*;
import org.progressoft.entity.jackson.payment.Agent;
import org.progressoft.entity.jackson.payment.InterBankSettlementAmount;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class ResponseMessageMapperTest {

    private final ResponseMessageMapper responseMessageMapper = new ResponseMessageMapper();

    @Test
    void givenInvalidXmlResponseWithMissingTags_whenParseXmlToMessage_thenReturnSemiEmptyResponseMessage() throws IOException {
        Path path = Paths.get(
                Objects.requireNonNull(this.getClass().getResource("/pacs.002.response-invalid.xml")).getPath());
        String xmlResponse = Files.readString(path);
        ResponseMessage responseMessage = responseMessageMapper.parseXmlToMessage(xmlResponse);
        assertNotNull(responseMessage);
        assertResponseGroupHeader(responseMessage);
        assertNull(responseMessage.getGroupInfoAndStatus());
        assertNull(responseMessage.getTransactionInfoAndStatus());
    }

    @Test
    void givenValidXmlResponse_whenParseXmlToMessage_thenReturnResponseMessage() throws IOException {
        Path path = Paths.get(
                Objects.requireNonNull(this.getClass().getResource("/pacs.002.response.xml")).getPath());
        String xmlResponse = Files.readString(path);
        ResponseMessage responseMessage = responseMessageMapper.parseXmlToMessage(xmlResponse);
        assertNotNull(responseMessage);
        assertResponseGroupHeader(responseMessage);
        assertOriginalGroupInfoAndStatus(responseMessage);
        assertTransactionInfoAndStatus(responseMessage);
    }

    private void assertTransactionInfoAndStatus(ResponseMessage responseMessage) {
        TransactionInfoAndStatus infoAndStatus = responseMessage.getTransactionInfoAndStatus();
        assertNotNull(infoAndStatus);
        assertEquals("JGBA2812094750927334298", infoAndStatus.getOriginalInstructionId());
        assertEquals("NOTPROVIDED", infoAndStatus.getOriginalEndToEndId());
        assertEquals("JGBA2812094750927334297", infoAndStatus.getOriginalTransactionId());
        assertAgent(infoAndStatus.getInstructingAgent(), "JGBAJOA0");
        assertAgent(infoAndStatus.getInstructedAgent(), "HBHOJOA0");
        assertOriginalTransactionRef(infoAndStatus);
    }

    private void assertOriginalTransactionRef(TransactionInfoAndStatus infoAndStatus) {
        OriginalTransactionRef transactionRef = infoAndStatus.getOriginalTransactionRef();
        assertNotNull(transactionRef);
        InterBankSettlementAmount settlementAmount = transactionRef.getSettlementAmount();
        assertNotNull(settlementAmount);
        assertEquals("JOD", settlementAmount.getCurrency());
        assertEquals(new BigDecimal("21.234"), settlementAmount.getValue());
        assertEquals(LocalDate.of(2023, 9, 26), transactionRef.getInterBankSettlementDate());
    }

    private void assertOriginalGroupInfoAndStatus(ResponseMessage responseMessage) {
        OriginalGroupInfoAndStatus groupInfoAndStatus = responseMessage.getGroupInfoAndStatus();
        assertNotNull(groupInfoAndStatus);
        assertEquals("MessageId20", groupInfoAndStatus.getOriginalMessageId());
        assertEquals("pacs.008.001.08", groupInfoAndStatus.getOriginalMessageNumberId());
        StatusReasonInfo reasonInfo = groupInfoAndStatus.getStatusReasonInfo();
        assertNotNull(reasonInfo);
        Proprietary reason = reasonInfo.getReason();
        assertNotNull(reason);
        assertEquals("ACSP", reason.getProprietary());
    }

    private void assertResponseGroupHeader(ResponseMessage responseMessage) {
        ResponseGroupHeader groupHeader = responseMessage.getResponseGroupHeader();
        assertNotNull(groupHeader);
        assertEquals("892b962b-c420-4cc1-ac21-094c701437f1", groupHeader.getMessageId());
        assertCentralAgent(groupHeader);
        assertAgent(groupHeader.getInstructedAgent(), "HBHOJOA0");
        assertEquals(LocalDateTime.of(2023, 9, 26,
                12, 19, 31, 623577000), groupHeader.getCreationDateTime());
    }

    private void assertCentralAgent(ResponseGroupHeader groupHeader) {
        CentralAgent centralAgent = groupHeader.getCentralAgent();
        assertNotNull(centralAgent);
        CentralFinancialInstitutionId institutionId = centralAgent.getFinancialInstitutionId();
        assertNotNull(institutionId);
        CentralFinancialInstitutionId.ClearingSystemMemberId id = institutionId.getSystemMemberId();
        assertNotNull(id);
        assertEquals("ZYAAJOA0AIPS", id.getMemberId());
    }

    private void assertAgent(Agent agent, String bic) {
        assertNotNull(agent);
        Agent.FinancialInstitutionId agentInstitutionId = agent.getInstitutionId();
        assertNotNull(agentInstitutionId);
        assertEquals(bic, agentInstitutionId.getBicFi());
    }

}