package org.progressoft.mapper;

import org.junit.jupiter.api.Test;
import org.progressoft.contract.PaymentParser;
import org.progressoft.contract.XmlBodyValidator;
import org.progressoft.entity.Payment;
import org.progressoft.exception.InvalidXmlBodyException;
import org.progressoft.exception.NullXmlBodyException;
import org.progressoft.exception.XmlBodyValidationException;
import org.progressoft.validator.DefaultXmlBodyValidator;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

public class JacksonPaymentParserTest {

    private final PaymentParser paymentParser;

    public JacksonPaymentParserTest() {
        XmlBodyValidator xmlBodyValidator = mock(DefaultXmlBodyValidator.class);
        doThrow(new NullXmlBodyException("Xml string is null"))
                .when(xmlBodyValidator).validateXmlBody(null);
        doThrow(new InvalidXmlBodyException("Xml string is empty"))
                .when(xmlBodyValidator).validateXmlBody("");
        doThrow(new XmlBodyValidationException(new RuntimeException(), "Unknown element 'root'"))
                .when(xmlBodyValidator).validateXmlBody("<root></root>");
        this.paymentParser = new JacksonPaymentParser(xmlBodyValidator);
    }

    @Test
    void givenNullXml_whenParse_thenFail() {
        NullXmlBodyException xmlPaymentException = assertThrows(NullXmlBodyException.class,
                () -> paymentParser.parse(null));
        assertEquals("Xml string is null", xmlPaymentException.getMessage());
    }

    @Test
    void givenEmptyXml_whenParse_thenFail() {
        InvalidXmlBodyException invalidXmlBodyException = assertThrows(InvalidXmlBodyException.class,
                () -> paymentParser.parse(""));
        assertEquals("Xml string is empty", invalidXmlBodyException.getMessage());
    }

    @Test
    void givenInvalidXml_whenParse_thenFail() {
        XmlBodyValidationException xmlBodyValidationException = assertThrows(XmlBodyValidationException.class,
                () -> paymentParser.parse("<root></root>"));
        assertEquals("Unknown element 'root'", xmlBodyValidationException.getMessage());
    }

    @Test
    void givenValidXmlString_whenParse_thenReturnPayment() throws IOException {
        Path path = Paths.get(Objects.requireNonNull(this.getClass().getResource("/pacs.008-test.xml")).getPath());
        String xmlString = Files.readString(path);
        Payment payment = paymentParser.parse(xmlString);
        assertNotNull(payment, "Must not be null");
        assertEquals("JGBA2812094750927334298", payment.getMessageId());
        assertEquals(LocalDateTime.of(2020, 12, 28, 7, 47, 50),
                payment.getCreationDateTime());
        assertEquals(1, payment.getNumberOfTransactions());
        assertEquals("CLRG", payment.getSettlementMethod());
        assertEquals("JGBA2812094750927334298", payment.getInstructionId());
        assertEquals("NOTPROVIDED", payment.getEndToEndId());
        assertEquals("JGBA2812094750927334297", payment.getTransactionId());
        assertEquals("RTNS", payment.getClearingChannel());
        assertEquals("0100", payment.getServiceLevel());
        assertEquals("CSDC", payment.getLocalInstrument());
        assertEquals("11110", payment.getCategoryPurpose());
        assertEquals("JOD", payment.getInterBankSettlementCurrency());
        assertEquals(new BigDecimal("23.000"), payment.getInterBankSettlementAmount());
        assertEquals(LocalDate.of(2023, 9, 26), payment.getInterBankSettlementDate());
        assertEquals("SLEV", payment.getChargeBearer());
        assertEquals("JGBAJOA0", payment.getInstructingAgentBicFi());
        assertEquals("HBHOJOA0", payment.getInstructedAgentBicFi());
        assertEquals("NISREEN MOHAMMAD YOUSEF", payment.getDebtorName());
        assertEquals(List.of("new zarqa"), payment.getDebtorAddressLines());
        assertEquals("JO93JGBA6010000290450010010000", payment.getDebtorAccountIban());
        assertEquals("JGBAJOA0", payment.getDebtorAgentBicFi());
        assertEquals("MAEN SAMI HATTAR SALEM", payment.getCreditorName());
        assertEquals(List.of("Amman Jordan SLT JO SLT"), payment.getCreditorAddressLines());
        assertEquals("JO83HBHO0320000033330600101001", payment.getCreditorAccountIban());
        assertEquals("HBHOJOA0", payment.getCreditorAgentBicFi());
    }
}
