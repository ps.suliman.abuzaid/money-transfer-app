package org.progressoft.mapper;

import org.junit.jupiter.api.Test;
import org.progressoft.entity.Payment;
import org.progressoft.entity.jackson.Proprietary;
import org.progressoft.entity.jackson.payment.*;
import org.progressoft.exception.NullJacksonPaymentException;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class JacksonPaymentMapperTest {

    private final JacksonPaymentMapper jacksonPaymentMapper = new JacksonPaymentMapper();

    @Test
    void givenNullObject_whenConvert_thenFail() {
        NullJacksonPaymentException nullJacksonPaymentException = assertThrows(NullJacksonPaymentException.class,
                () -> jacksonPaymentMapper.convert(null));
        assertEquals("paymentObject is null", nullJacksonPaymentException.getMessage());
    }

    @Test
    void givenPaymentObjectWithNullRootFields_whenConvert_thenReturnPaymentWithNullFields() {
        JacksonXmlPayment jacksonPayment = new JacksonXmlPayment();
        Payment payment = jacksonPaymentMapper.convert(jacksonPayment);
        assertNotNull(payment, "Must not be null");
        assertNull(payment.getCreditorName());
        assertNull(payment.getMessageId());
        assertNull(payment.getClearingChannel());
        assertNull(payment.getServiceLevel());
        assertNull(payment.getCategoryPurpose());
        assertNull(payment.getCreditorAddressLines());
        assertNull(payment.getInstructionId());
    }

    @Test
    void givenValidJacksonPaymentObject_whenConvert_thenReturnPayment() {
        Payment payment = jacksonPaymentMapper.convert(getJacksonXmlPayment());
        assertNotNull(payment, "Must not be null");
        assertFilledGroupHeaderData(payment);
        assertPaymentIdData(payment);
        assertPaymentTypeInfoData(payment);
        assertInterBankSettlementAmountData(payment);
        assertEquals(LocalDate.of(2023, 9, 20), payment.getInterBankSettlementDate());
        assertEquals("SLEV", payment.getChargeBearer());
        assetDebtorData(payment);
        assertCreditorData(payment);
        assertAgents(payment);
        assertEquals("JO93JGBA6010000290450010010000", payment.getDebtorAccountIban());
        assertEquals("JO83HBHO0320000033330600101001", payment.getCreditorAccountIban());
    }

    private static void assertAgents(Payment payment) {
        assertEquals("JGBAJOA0", payment.getInstructingAgentBicFi());
        assertEquals("HBHOJOA0", payment.getInstructedAgentBicFi());
        assertEquals("JGBAJOA0", payment.getDebtorAgentBicFi());
        assertEquals("HBHOJOA0", payment.getCreditorAgentBicFi());
    }

    private void assertCreditorData(Payment payment) {
        assertEquals("MAEN SAMI HATTAR SALEM", payment.getCreditorName());
        assertArrayEquals(new String[]{"Amman Jordan SLT JO SLT"}, payment.getCreditorAddressLines().toArray());
    }

    private void assetDebtorData(Payment payment) {
        assertEquals("NISREEN MOHAMMAD YOUSEF", payment.getDebtorName());
        assertArrayEquals(new String[]{"new zarqa"}, payment.getDebtorAddressLines().toArray());
    }

    private void assertInterBankSettlementAmountData(Payment payment) {
        assertEquals("JOD", payment.getInterBankSettlementCurrency());
        assertEquals(new BigDecimal("45.000"), payment.getInterBankSettlementAmount());
    }

    private void assertPaymentTypeInfoData(Payment payment) {
        assertEquals("RTNS", payment.getClearingChannel());
        assertEquals("0100", payment.getServiceLevel());
        assertEquals("CSDC", payment.getLocalInstrument());
        assertEquals("11110", payment.getCategoryPurpose());
    }

    private void assertPaymentIdData(Payment payment) {
        assertEquals("JGBA2812094750927334298", payment.getInstructionId());
        assertEquals("NOTPROVIDED", payment.getEndToEndId());
        assertEquals("JGBA2812094750927334297", payment.getTransactionId());
    }

    private void assertFilledGroupHeaderData(Payment payment) {
        assertEquals("JGBA2812094750927334298", payment.getMessageId());
        assertEquals(LocalDateTime.of(2020, 12, 28, 7, 47, 50)
                , payment.getCreationDateTime());
        assertEquals(1, payment.getNumberOfTransactions());
        assertEquals("CLRG", payment.getSettlementMethod());
    }

    private JacksonXmlPayment getJacksonXmlPayment() {
        JacksonXmlPayment xmlPayment = new JacksonXmlPayment();
        xmlPayment.setGroupHeader(getGroupHeader());
        xmlPayment.setTransferTransactionInfo(getCreditTransferTransactionInfo());
        return xmlPayment;
    }

    private static CreditTransferTransactionInfo getCreditTransferTransactionInfo() {
        CreditTransferTransactionInfo transactionInfo = new CreditTransferTransactionInfo();
        transactionInfo.setPaymentId(getPaymentId());
        transactionInfo.setPaymentTypeInfo(getPaymentTypeInfo());
        transactionInfo.setSettlementAmount(getInterBankSettlementAmount());
        transactionInfo.setInterBankSettlementDate(LocalDate.of(2023, 9, 20));
        transactionInfo.setChargeBearer("SLEV");
        setClients(transactionInfo);
        setAccounts(transactionInfo);
        setAgents(transactionInfo);
        return transactionInfo;
    }

    private static void setClients(CreditTransferTransactionInfo transactionInfo) {
        transactionInfo.setDebtor(new Client("NISREEN MOHAMMAD YOUSEF", List.of("new zarqa")));
        transactionInfo.setCreditor(new Client("MAEN SAMI HATTAR SALEM",
                List.of("Amman Jordan SLT JO SLT")));
    }

    private static void setAccounts(CreditTransferTransactionInfo transactionInfo) {
        transactionInfo.setDebtorAccount(new Account("JO93JGBA6010000290450010010000"));
        transactionInfo.setCreditorAccount(new Account("JO83HBHO0320000033330600101001"));
    }

    private static void setAgents(CreditTransferTransactionInfo transactionInfo) {
        transactionInfo.setInstructingAgent(new Agent("JGBAJOA0"));
        transactionInfo.setInstructedAgent(new Agent("HBHOJOA0"));
        transactionInfo.setDebtorAgent(new Agent("JGBAJOA0"));
        transactionInfo.setCreditorAgent(new Agent("HBHOJOA0"));
    }

    private static InterBankSettlementAmount getInterBankSettlementAmount() {
        return new InterBankSettlementAmount("JOD", new BigDecimal("45.000"));
    }

    private static PaymentTypeInfo getPaymentTypeInfo() {
        PaymentTypeInfo typeInfo = new PaymentTypeInfo();
        typeInfo.setClearingChannel("RTNS");
        typeInfo.setServiceLevel(new Proprietary("0100"));
        typeInfo.setLocalInstrument(new Proprietary("CSDC"));
        typeInfo.setCategoryPurpose(new Proprietary("11110"));
        return typeInfo;
    }

    private static PaymentId getPaymentId() {
        PaymentId id = new PaymentId();
        id.setInstructionId("JGBA2812094750927334298");
        id.setEndToEndId("NOTPROVIDED");
        id.setTransactionId("JGBA2812094750927334297");
        return id;
    }

    private static GroupHeader getGroupHeader() {
        GroupHeader groupHeader = new GroupHeader();
        groupHeader.setMessageId("JGBA2812094750927334298");
        groupHeader.setCreationDateTime(LocalDateTime.of(2020, 12, 28, 7, 47, 50));
        groupHeader.setNumberOfTransactions(1);
        GroupHeader.SettlementInfo info = new GroupHeader.SettlementInfo();
        info.setSettlementMethodCode("CLRG");
        groupHeader.setSettlementInfo(info);
        return groupHeader;
    }
}
