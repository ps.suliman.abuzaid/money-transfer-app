package org.progressoft.mapper;

import org.junit.jupiter.api.Test;
import org.progressoft.contract.PaymentToReplyBodyMapper;
import org.progressoft.entity.Payment;
import org.progressoft.entity.RejectionMotive;
import org.progressoft.entity.Reply;
import org.progressoft.entity.ReplyBody;
import org.progressoft.enums.Reason;
import org.progressoft.enums.Status;
import org.progressoft.exception.NullPaymentException;
import org.progressoft.exception.NullRejectionMotiveSetException;
import org.progressoft.factory.DefaultBeanFactory;
import org.progressoft.factory.PaymentFactory;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class DefaultPaymentToReplyBodyMapperTest {

    private final PaymentToReplyBodyMapper replyBodyMapper = new DefaultBeanFactory().getPaymentToReplyBodyMapper();

    @Test
    void givenNullPayment_whenConvertPaymentToReplyBody_thenFail() {
        NullPaymentException nullPaymentException = assertThrows(NullPaymentException.class,
                () -> replyBodyMapper.convertPaymentToReplyBody(null, new HashSet<>()));
        assertEquals("payment is null", nullPaymentException.getMessage());
    }

    @Test
    void givenNullRejectionMotiveSet_whenConvertPaymentToReplyBody_thenFail() {
        NullRejectionMotiveSetException motiveSetException = assertThrows(NullRejectionMotiveSetException.class,
                () -> replyBodyMapper.convertPaymentToReplyBody(new Payment(), null));
        assertEquals("rejectionMotives set is null", motiveSetException.getMessage());
    }

    @Test
    void givenValidInput_whenConvertPaymentToReplyBody_thenFail() {
        Payment payment = PaymentFactory.getValidPayment();
        ReplyBody replyBody = replyBodyMapper.convertPaymentToReplyBody(payment, new HashSet<>());
        assertNotNull(replyBody);
        assertNotNull(replyBody.getXmlReply());
        Reply reply = replyBody.getReply();
        assertReplyNotNull(reply);
        assertEquals(payment.getMessageId(), reply.getPaymentMessageId());
        assertEquals(Reason.AUTH, reply.getReason());
        assertEquals(Status.PENDING_RESPONSE, reply.getStatus());
    }

    @Test
    void givenInvalidPayment_whenConvertPaymentToReplyBody_thenFail() {
        Payment payment = PaymentFactory.getInvalidPayment();
        ReplyBody replyBody = replyBodyMapper.convertPaymentToReplyBody(payment,
                Set.of(new RejectionMotive("AC02", "Invalid Settlement amount"),
                        new RejectionMotive("AC03", "Invalid Debtor name")));
        assertNotNull(replyBody);
        assertNotNull(replyBody.getXmlReply());
        Reply reply = replyBody.getReply();
        assertReplyNotNull(reply);
        assertEquals(payment.getMessageId(), reply.getPaymentMessageId());
        assertEquals(Reason.NAUT, reply.getReason());
        assertEquals(Status.PENDING_RESPONSE, reply.getStatus());
    }

    private void assertReplyNotNull(Reply reply) {
        assertNotNull(reply);
        assertNotNull(reply.getPaymentMessageId());
        assertNotNull(reply.getStatus());
        assertNotNull(reply.getCreationDateTime());
        assertNotNull(reply.getMessageId());
        assertNotNull(reply.getReason());
    }
}