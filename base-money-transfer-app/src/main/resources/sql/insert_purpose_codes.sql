INSERT INTO public.purpose_code
(id, code, usage)
VALUES(1, '11110', 'individual Transfer through Mobile Banking to Alias/IBAN to Individual (P2P)');
INSERT INTO public.purpose_code
(id, code, usage)
VALUES(2, '11120', 'individual Transfer through Mobile Banking to Alias/IBAN to Legal (P2B)');
INSERT INTO public.purpose_code
(id, code, usage)
VALUES(3, '11130', 'individual Transfer through Mobile Banking to Government (P2G)');
INSERT INTO public.purpose_code
(id, code, usage)
VALUES(4, '11210', 'individual Transfer through E-Banking to Alias/IBAN to Individual (P2P)');
INSERT INTO public.purpose_code
(id, code, usage)
VALUES(5, '11220', 'individual Transfer through E-Banking to Alias/IBAN to Legal (P2B)');
INSERT INTO public.purpose_code
(id, code, usage)
VALUES(6, '11230', 'individual Transfer through E-Banking to Government (P2G)');
INSERT INTO public.purpose_code
(id, code, usage)
VALUES(7, '12110', 'Legal Transfer through Mobile Banking to Alias/IBAN to Individual (B2P)');
INSERT INTO public.purpose_code
(id, code, usage)
VALUES(8, '12120', 'Legal Transfer through Mobile Banking to Alias/IBAN to Legal (B2B)');
INSERT INTO public.purpose_code
(id, code, usage)
VALUES(9, '12130', 'Legal Transfer through Mobile Banking to Government (B2G)');
INSERT INTO public.purpose_code
(id, code, usage)
VALUES(10, '12210', 'Legal Transfer through E-Banking to Alias/IBAN to Individual (B2P)');
INSERT INTO public.purpose_code
(id, code, usage)
VALUES(11, '12220', 'Legal Transfer through E-Banking to Alias/IBAN to Legal (B2B)');
INSERT INTO public.purpose_code
(id, code, usage)
VALUES(12, '12230', 'Legal Transfer through E-Banking to Government (B2G)');
INSERT INTO public.purpose_code
(id, code, usage)
VALUES(13, '21110', 'Purchases using Mobile Banking through entering Alias/IBAN for individuals');
INSERT INTO public.purpose_code
(id, code, usage)
VALUES(14, '21120', 'Purchases using Mobile Banking through QR for individuals');
INSERT INTO public.purpose_code
(id, code, usage)
VALUES(15, '21130', 'Purchases using E-Banking through entering Alias/IBAN for individuals');
INSERT INTO public.purpose_code
(id, code, usage)
VALUES(16, '22110', 'Purchases using Mobile Banking through entering Alias/IBAN for Legal');
INSERT INTO public.purpose_code
(id, code, usage)
VALUES(17, '22120', 'Purchases using Mobile Banking through QR for Legal');
INSERT INTO public.purpose_code
(id, code, usage)
VALUES(18, '22210', 'Purchases using E-Banking through entering Alias/IBAN for Legal');
INSERT INTO public.purpose_code
(id, code, usage)
VALUES(19, '41010', 'Individual Return a Payment');
INSERT INTO public.purpose_code
(id, code, usage)
VALUES(20, '42010', 'Legal Return a Payment');