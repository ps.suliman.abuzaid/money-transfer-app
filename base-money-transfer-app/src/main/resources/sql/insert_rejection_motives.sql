INSERT INTO public.rejection_motive
(id, motive_code, motive_description)
VALUES(1, 'AC02', 'Debtor account number is invalid or missing');
INSERT INTO public.rejection_motive
(id, motive_code, motive_description)
VALUES(2, 'AC03', 'Creditor account number invalid or missing');
INSERT INTO public.rejection_motive
(id, motive_code, motive_description)
VALUES(3, 'BE01', 'Transaction type not supported or not authorized on this account');
INSERT INTO public.rejection_motive
(id, motive_code, motive_description)
VALUES(4, 'BE15', 'Identification of end customer is not consistent with associated account number');
INSERT INTO public.rejection_motive
(id, motive_code, motive_description)
VALUES(5, 'BE20', 'Customer identification code missing or invalid.');
INSERT INTO public.rejection_motive
(id, motive_code, motive_description)
VALUES(6, 'FF03', 'Payment Type Information is missing or invalid.');
INSERT INTO public.rejection_motive
(id, motive_code, motive_description)
VALUES(7, 'ACDE', 'Account does not exist');
INSERT INTO public.rejection_motive
(id, motive_code, motive_description)
VALUES(8, 'ACDM', 'Account owner details inadequate or missing');
INSERT INTO public.rejection_motive
(id, motive_code, motive_description)
VALUES(9, 'ACLD', 'Account is locked/frozen, prohibiting posting of transactions');
INSERT INTO public.rejection_motive
(id, motive_code, motive_description)
VALUES(10, 'ACLS', 'Account is closed');
INSERT INTO public.rejection_motive
(id, motive_code, motive_description)
VALUES(11, 'ADRM', 'Account is dormant');
INSERT INTO public.rejection_motive
(id, motive_code, motive_description)
VALUES(12, 'AMEX', 'Transaction amount exceeds maximum allowed for this account');
INSERT INTO public.rejection_motive
(id, motive_code, motive_description)
VALUES(13, 'CUDC', 'Customer reported deceased');
INSERT INTO public.rejection_motive
(id, motive_code, motive_description)
VALUES(14, 'QRER', 'QR is invalid or couldn''t be parsed');