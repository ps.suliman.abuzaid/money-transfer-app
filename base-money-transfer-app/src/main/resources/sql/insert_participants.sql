INSERT INTO public.participant
(id, name, bic)
VALUES(1, 'Arab Banking Corporation (Jordan)', 'ABCJJOA0');
INSERT INTO public.participant
(id, name, bic)
VALUES(2, 'Arab Jordan Investment Bank', 'AJIBJOA0');
INSERT INTO public.participant
(id, name, bic)
VALUES(3, 'Arab Bank Ltd', 'ARABJOA0');
INSERT INTO public.participant
(id, name, bic)
VALUES(4, 'Egyptian Arab Land Bank', 'ARLBJOA0');
INSERT INTO public.participant
(id, name, bic)
VALUES(5, 'Bank of Jordan', 'BJORJOA0');
INSERT INTO public.participant
(id, name, bic)
VALUES(6, 'BLOM BANK', 'BLOMJOA0');
INSERT INTO public.participant
(id, name, bic)
VALUES(7, 'CAAB BANK', 'CAABJOA0');
INSERT INTO public.participant
(id, name, bic)
VALUES(8, 'CITIBANK', 'CITIJOA0');
INSERT INTO public.participant
(id, name, bic)
VALUES(9, 'Capital Bank of Jordan', 'EFBKJOA0');
INSERT INTO public.participant
(id, name, bic)
VALUES(10, 'The Housing Bank for Trade and Finance', 'HBHOJOA0');
INSERT INTO public.participant
(id, name, bic)
VALUES(11, 'Islamic Intl Arab Bank', 'IIBAJOA0');
INSERT INTO public.participant
(id, name, bic)
VALUES(12, 'SAFWA ISLAMIC BANK', 'JDIBJOA0');
INSERT INTO public.participant
(id, name, bic)
VALUES(13, 'Jordan Commercial Bank', 'JGBAJOA0');
INSERT INTO public.participant
(id, name, bic)
VALUES(14, 'Jordan Islamic Bank', 'JIBAJOA0');
INSERT INTO public.participant
(id, name, bic)
VALUES(15, 'Jordan Investment and Finance Bank', 'JIFBJOA0');
INSERT INTO public.participant
(id, name, bic)
VALUES(16, 'Jordan Kuwait Bank', 'JKBAJOA0');
INSERT INTO public.participant
(id, name, bic)
VALUES(17, 'Jordan Ahli Bank', 'JONBJOA0');
INSERT INTO public.participant
(id, name, bic)
VALUES(18, 'National Bank of Kuwait - Jordan', 'NBOKJOA0');
INSERT INTO public.participant
(id, name, bic)
VALUES(19, 'AL RAJHI BANK', 'RJHIJOA0');
INSERT INTO public.participant
(id, name, bic)
VALUES(20, 'Standard Chartered Bank', 'SCBLJOA0');
INSERT INTO public.participant
(id, name, bic)
VALUES(21, 'Societe Generale de Banque-Jordanie', 'SGMEJOA0');
INSERT INTO public.participant
(id, name, bic)
VALUES(22, 'Union Bank for Savings and Investment', 'UBSIJOA0');