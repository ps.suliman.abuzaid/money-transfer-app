package org.progressoft;

import org.progressoft.contract.*;
import org.progressoft.exception.XmlBodyValidationException;
import org.progressoft.factory.DefaultBeanFactory;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class MoneyTransferApp {

    private final static BeanFactory beanFactory = new DefaultBeanFactory();
    private final static ErrorHandler errorHandler = new DefaultBeanFactory().getPaymentErrorDispatcher();

    public static void main(String[] args) throws IOException {
        EnvironmentPropertyService propertyService = beanFactory.getEnvironmentPropertyService();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to Participant System ...");
        System.out.println("If you want to receive inward messages, please hit [Any-key] to continue or [X] for exit ...");
        String control = scanner.next();
        while (!control.equalsIgnoreCase("x")) {
            processRequestsAndSendReplies(propertyService);
            processResponsesAndChangeStatus(propertyService);
            System.out.println("If you want to receive inward messages, please hit [Any-key] to continue or [X] for exit ...");
            control = scanner.next();
        }
        scanner.close();
    }

    private static void processResponsesAndChangeStatus(EnvironmentPropertyService propertyService) throws IOException {
        ResponseService responseService = beanFactory.getResponseService();
        Path inPacs002Path = Paths.get(propertyService.getProperty("in.pacs.002.dir"));
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(inPacs002Path, "*.xml")) {
            for (Path path : directoryStream) {
                String xmlResponse = Files.readString(path);
                responseService.processResponse(xmlResponse);
                Files.delete(path);
            }
        }
    }

    private static void processRequestsAndSendReplies(EnvironmentPropertyService propertyService) throws IOException {
        Path paymentsPath = Paths.get(propertyService.getProperty("in.pacs.008.dir"));
        PaymentRequestService paymentRequestService = beanFactory.getPaymentRequestService();
        try (DirectoryStream<Path> paymentDirectoryStream = Files.newDirectoryStream(paymentsPath, "*.xml")) {
            String xmlPayment;
            for (Path path : paymentDirectoryStream) {
                xmlPayment = Files.readString(path);
                try {
                    paymentRequestService.processRequest(xmlPayment);
                } catch (XmlBodyValidationException e) {
                    e.printStackTrace();
                    System.out.println("Invalid pacs.008 file, check error_pacs.008 directory for more details");
                    errorHandler.dispatchError(xmlPayment, e.getMessage());
                }
                Files.delete(path);
            }
        }
    }
}
