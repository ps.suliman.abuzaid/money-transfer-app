package org.progressoft.exception;

public class ResponseNotValidException extends RuntimeException {
    public ResponseNotValidException(String message) {
        super(message);
    }
}
