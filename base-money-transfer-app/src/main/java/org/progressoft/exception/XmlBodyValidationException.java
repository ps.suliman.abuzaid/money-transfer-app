package org.progressoft.exception;

public class XmlBodyValidationException extends RuntimeException {

    public XmlBodyValidationException(Exception e, String errorMessage) {
        super(errorMessage, e);
    }
}
