package org.progressoft.exception;

public class ReplyNotUpdatedException extends RuntimeException {
    public ReplyNotUpdatedException(String message, Throwable cause) {
        super(message, cause);
    }
}
