package org.progressoft.exception;

public class NullReplyBodyException extends RuntimeException {
    public NullReplyBodyException(String message) {
        super(message);
    }
}
