package org.progressoft.exception;

public class NullRejectionMotiveSetException extends RuntimeException {
    public NullRejectionMotiveSetException(String message) {
        super(message);
    }
}
