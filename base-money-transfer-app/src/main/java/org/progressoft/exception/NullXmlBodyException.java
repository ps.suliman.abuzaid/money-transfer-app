package org.progressoft.exception;

public class NullXmlBodyException extends RuntimeException {
    public NullXmlBodyException(String message) {
        super(message);
    }
}
