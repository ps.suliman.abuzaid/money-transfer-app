package org.progressoft.exception;

public class PaymentNotSavedException extends RuntimeException {

    public PaymentNotSavedException(String message) {
        super(message);
    }

    public PaymentNotSavedException(String message, Throwable cause) {
        super(message, cause);
    }
}
