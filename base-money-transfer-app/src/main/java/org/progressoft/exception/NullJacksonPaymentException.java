package org.progressoft.exception;

public class NullJacksonPaymentException extends RuntimeException {
    public NullJacksonPaymentException(String message) {
        super(message);
    }
}
