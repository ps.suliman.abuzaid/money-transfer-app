package org.progressoft.exception;

public class NullStatusException extends RuntimeException {
    public NullStatusException(String message) {
        super(message);
    }
}
