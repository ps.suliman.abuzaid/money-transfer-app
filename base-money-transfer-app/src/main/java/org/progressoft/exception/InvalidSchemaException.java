package org.progressoft.exception;

public class InvalidSchemaException extends RuntimeException {
    public InvalidSchemaException(String message, Throwable cause) {
        super(message, cause);
    }
}
