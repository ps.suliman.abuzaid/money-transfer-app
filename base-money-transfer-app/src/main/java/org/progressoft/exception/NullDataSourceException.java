package org.progressoft.exception;

public class NullDataSourceException extends RuntimeException {

    public NullDataSourceException(String message) {
        super(message);
    }
}
