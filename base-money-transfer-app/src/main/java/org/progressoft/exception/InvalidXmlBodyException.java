package org.progressoft.exception;

public class InvalidXmlBodyException extends RuntimeException {
    public InvalidXmlBodyException(String message) {
        super(message);
    }
}
