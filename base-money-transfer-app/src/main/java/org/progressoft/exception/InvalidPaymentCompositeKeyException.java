package org.progressoft.exception;

public class InvalidPaymentCompositeKeyException extends RuntimeException {
    public InvalidPaymentCompositeKeyException(String message) {
        super(message);
    }
}
