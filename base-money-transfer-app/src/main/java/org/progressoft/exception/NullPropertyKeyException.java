package org.progressoft.exception;

public class NullPropertyKeyException extends RuntimeException {

    public NullPropertyKeyException(String message) {
        super(message);
    }
}
