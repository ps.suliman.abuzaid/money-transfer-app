package org.progressoft.exception;

public class NullReasonException extends RuntimeException {
    public NullReasonException(String message) {
        super(message);
    }
}
