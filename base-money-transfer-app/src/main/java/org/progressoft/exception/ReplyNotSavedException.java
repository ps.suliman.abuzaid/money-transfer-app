package org.progressoft.exception;

public class ReplyNotSavedException extends RuntimeException {
    public ReplyNotSavedException(String message) {
        super(message);
    }

    public ReplyNotSavedException(String message, Throwable cause) {
        super(message, cause);
    }
}
