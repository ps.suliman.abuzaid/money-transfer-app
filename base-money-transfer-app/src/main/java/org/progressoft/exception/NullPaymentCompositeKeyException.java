package org.progressoft.exception;

public class NullPaymentCompositeKeyException extends RuntimeException {

    public NullPaymentCompositeKeyException(String message) {
        super(message);
    }
}
