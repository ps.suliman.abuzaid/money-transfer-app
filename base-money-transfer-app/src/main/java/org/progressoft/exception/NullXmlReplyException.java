package org.progressoft.exception;

public class NullXmlReplyException extends RuntimeException {
    public NullXmlReplyException(String message) {
        super(message);
    }
}
