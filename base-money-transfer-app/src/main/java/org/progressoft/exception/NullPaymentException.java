package org.progressoft.exception;

public class NullPaymentException extends RuntimeException {
    public NullPaymentException(String message) {
        super(message);
    }
}
