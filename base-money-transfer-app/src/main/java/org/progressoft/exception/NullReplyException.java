package org.progressoft.exception;

public class NullReplyException extends RuntimeException {
    public NullReplyException(String message) {
        super(message);
    }
}
