package org.progressoft.exception;

public class NullMessageIdException extends RuntimeException {
    public NullMessageIdException(String message) {
        super(message);
    }
}
