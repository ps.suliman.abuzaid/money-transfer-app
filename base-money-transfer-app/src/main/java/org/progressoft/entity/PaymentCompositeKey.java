package org.progressoft.entity;

import java.time.LocalDate;
import java.util.Objects;

public class PaymentCompositeKey {
    private String messageId;
    private String instructionId;
    private String endToEndId;
    private String transactionId;
    private LocalDate interBankSettlementDate;

    public PaymentCompositeKey() {
    }

    public PaymentCompositeKey(String messageId, String instructionId, String endToEndId,
                               String transactionId, LocalDate interBankSettlementDate) {
        this.messageId = messageId;
        this.instructionId = instructionId;
        this.endToEndId = endToEndId;
        this.transactionId = transactionId;
        this.interBankSettlementDate = interBankSettlementDate;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getInstructionId() {
        return instructionId;
    }

    public void setInstructionId(String instructionId) {
        this.instructionId = instructionId;
    }

    public String getEndToEndId() {
        return endToEndId;
    }

    public void setEndToEndId(String endToEndId) {
        this.endToEndId = endToEndId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public LocalDate getInterBankSettlementDate() {
        return interBankSettlementDate;
    }

    public void setInterBankSettlementDate(LocalDate interBankSettlementDate) {
        this.interBankSettlementDate = interBankSettlementDate;
    }

    @Override
    public String toString() {
        return "PaymentCompositeId{" +
                "messageId='" + messageId + '\'' +
                ", instructionId='" + instructionId + '\'' +
                ", endToEndId='" + endToEndId + '\'' +
                ", transactionId='" + transactionId + '\'' +
                ", interBankSettlementDate=" + interBankSettlementDate +
                '}';
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        PaymentCompositeKey key = (PaymentCompositeKey) object;

        if (!Objects.equals(messageId, key.messageId)) return false;
        if (!Objects.equals(instructionId, key.instructionId)) return false;
        if (!Objects.equals(endToEndId, key.endToEndId)) return false;
        if (!Objects.equals(transactionId, key.transactionId)) return false;
        return Objects.equals(interBankSettlementDate, key.interBankSettlementDate);
    }

    @Override
    public int hashCode() {
        int result = messageId != null ? messageId.hashCode() : 0;
        result = 31 * result + (instructionId != null ? instructionId.hashCode() : 0);
        result = 31 * result + (endToEndId != null ? endToEndId.hashCode() : 0);
        result = 31 * result + (transactionId != null ? transactionId.hashCode() : 0);
        result = 31 * result + (interBankSettlementDate != null ? interBankSettlementDate.hashCode() : 0);
        return result;
    }
}
