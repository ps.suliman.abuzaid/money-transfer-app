package org.progressoft.entity;

public class RejectionMotive {
    private final String motiveCode;
    private final String motiveDescription;

    public RejectionMotive(String motiveCode, String motiveDescription) {
        this.motiveCode = motiveCode;
        this.motiveDescription = motiveDescription;
    }

    public String getMotiveCode() {
        return motiveCode;
    }

    public String getMotiveDescription() {
        return motiveDescription;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        RejectionMotive that = (RejectionMotive) object;

        if (!motiveCode.equals(that.motiveCode)) return false;
        return motiveDescription.equals(that.motiveDescription);
    }

    @Override
    public int hashCode() {
        int result = motiveCode.hashCode();
        result = 31 * result + motiveDescription.hashCode();
        return result;
    }
}
