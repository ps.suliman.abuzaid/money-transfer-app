package org.progressoft.entity;

import java.math.BigDecimal;
import java.util.List;

public class AccountRequest {
    private String name;
    private String Iban;
    private List<String> addressLines;
    private BigDecimal settlementAmount;
    private String settlementIsoCurrency;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIban() {
        return Iban;
    }

    public void setIban(String iban) {
        Iban = iban;
    }

    public List<String> getAddressLines() {
        return addressLines;
    }

    public void setAddressLines(List<String> addressLines) {
        this.addressLines = addressLines;
    }

    public BigDecimal getSettlementAmount() {
        return settlementAmount;
    }

    public void setSettlementAmount(BigDecimal settlementAmount) {
        this.settlementAmount = settlementAmount;
    }

    public String getSettlementIsoCurrency() {
        return settlementIsoCurrency;
    }

    public void setSettlementIsoCurrency(String settlementIsoCurrency) {
        this.settlementIsoCurrency = settlementIsoCurrency;
    }
}
