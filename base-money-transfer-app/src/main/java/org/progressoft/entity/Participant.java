package org.progressoft.entity;

public class Participant {
    private String name;
    private String bic;

    public Participant(String name, String bic) {
        this.name = name;
        this.bic = bic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        Participant that = (Participant) object;

        if (!name.equals(that.name)) return false;
        return bic.equals(that.bic);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + bic.hashCode();
        return result;
    }
}
