package org.progressoft.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

public class PaymentFilterRequest {
    private String messageId;
    private String currency;
    private BigDecimal settlementAmountFrom;
    private BigDecimal settlementAmountTo;
    private LocalDate settlementDateFrom;
    private LocalDate settlementDateTo;
    private String debtorAccountIban;
    private String debtorAgentBicFi;
    private String creditorAccountIban;
    private String authStatus;
    private int pageCount;
    private int pageNumber;
    private int totalPages;
    private int startPage;
    private int endPage;
    private int totalItemsNumber;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getSettlementAmountFrom() {
        return settlementAmountFrom;
    }

    public void setSettlementAmountFrom(BigDecimal settlementAmountFrom) {
        this.settlementAmountFrom = settlementAmountFrom;
    }

    public BigDecimal getSettlementAmountTo() {
        return settlementAmountTo;
    }

    public void setSettlementAmountTo(BigDecimal settlementAmountTo) {
        this.settlementAmountTo = settlementAmountTo;
    }

    public LocalDate getSettlementDateFrom() {
        return settlementDateFrom;
    }

    public void setSettlementDateFrom(LocalDate settlementDateFrom) {
        this.settlementDateFrom = settlementDateFrom;
    }

    public LocalDate getSettlementDateTo() {
        return settlementDateTo;
    }

    public void setSettlementDateTo(LocalDate settlementDateTo) {
        this.settlementDateTo = settlementDateTo;
    }

    public String getDebtorAccountIban() {
        return debtorAccountIban;
    }

    public void setDebtorAccountIban(String debtorAccountIban) {
        this.debtorAccountIban = debtorAccountIban;
    }

    public String getDebtorAgentBicFi() {
        return debtorAgentBicFi;
    }

    public void setDebtorAgentBicFi(String debtorAgentBicFi) {
        this.debtorAgentBicFi = debtorAgentBicFi;
    }

    public String getCreditorAccountIban() {
        return creditorAccountIban;
    }

    public void setCreditorAccountIban(String creditorAccountIban) {
        this.creditorAccountIban = creditorAccountIban;
    }

    public String getAuthStatus() {
        return authStatus;
    }

    public void setAuthStatus(String authStatus) {
        this.authStatus = authStatus;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getStartPage() {
        return startPage;
    }

    public void setStartPage(int startPage) {
        this.startPage = startPage;
    }

    public int getEndPage() {
        return endPage;
    }

    public void setEndPage(int endPage) {
        this.endPage = endPage;
    }

    public int getTotalItems() {
        return totalItemsNumber;
    }

    public void setTotalItems(int totalItems) {
        this.totalItemsNumber = totalItems;
    }
}
