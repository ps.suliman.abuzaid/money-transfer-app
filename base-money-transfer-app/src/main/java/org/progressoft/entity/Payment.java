package org.progressoft.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class Payment {
    private String messageId;
    private LocalDateTime creationDateTime;
    private int numberOfTransactions;
    private String settlementMethod;
    private String instructionId;
    private String endToEndId;
    private String transactionId;
    private String clearingChannel;
    private String serviceLevel;
    private String localInstrument;
    private String categoryPurpose;
    private String interBankSettlementCurrency;
    private BigDecimal interBankSettlementAmount;
    private LocalDate interBankSettlementDate;
    private String chargeBearer;
    private String instructingAgentBicFi;
    private String instructedAgentBicFi;
    private String debtorName;
    private List<String> debtorAddressLines;
    private String debtorAccountIban;
    private String debtorAgentBicFi;
    private String creditorName;
    private List<String> creditorAddressLines;
    private String creditorAccountIban;
    private String creditorAgentBicFi;
    private String status;
    private String authStatus;
    private List<String> rejectionCodes;
    private String xmlPayment;
    private String xmlReply;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public LocalDateTime getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(LocalDateTime creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    public int getNumberOfTransactions() {
        return numberOfTransactions;
    }

    public void setNumberOfTransactions(int numberOfTransactions) {
        this.numberOfTransactions = numberOfTransactions;
    }

    public String getSettlementMethod() {
        return settlementMethod;
    }

    public void setSettlementMethod(String settlementMethod) {
        this.settlementMethod = settlementMethod;
    }

    public String getInstructionId() {
        return instructionId;
    }

    public void setInstructionId(String instructionId) {
        this.instructionId = instructionId;
    }

    public String getEndToEndId() {
        return endToEndId;
    }

    public void setEndToEndId(String endToEndId) {
        this.endToEndId = endToEndId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getClearingChannel() {
        return clearingChannel;
    }

    public void setClearingChannel(String clearingChannel) {
        this.clearingChannel = clearingChannel;
    }

    public String getServiceLevel() {
        return serviceLevel;
    }

    public void setServiceLevel(String serviceLevel) {
        this.serviceLevel = serviceLevel;
    }

    public String getLocalInstrument() {
        return localInstrument;
    }

    public void setLocalInstrument(String localInstrument) {
        this.localInstrument = localInstrument;
    }

    public String getCategoryPurpose() {
        return categoryPurpose;
    }

    public void setCategoryPurpose(String categoryPurpose) {
        this.categoryPurpose = categoryPurpose;
    }

    public String getInterBankSettlementCurrency() {
        return interBankSettlementCurrency;
    }

    public void setInterBankSettlementCurrency(String interBankSettlementCurrency) {
        this.interBankSettlementCurrency = interBankSettlementCurrency;
    }

    public BigDecimal getInterBankSettlementAmount() {
        return interBankSettlementAmount;
    }

    public void setInterBankSettlementAmount(BigDecimal interBankSettlementAmount) {
        this.interBankSettlementAmount = interBankSettlementAmount;
    }

    public LocalDate getInterBankSettlementDate() {
        return interBankSettlementDate;
    }

    public void setInterBankSettlementDate(LocalDate interBankSettlementDate) {
        this.interBankSettlementDate = interBankSettlementDate;
    }

    public String getChargeBearer() {
        return chargeBearer;
    }

    public void setChargeBearer(String chargeBearer) {
        this.chargeBearer = chargeBearer;
    }

    public String getInstructingAgentBicFi() {
        return instructingAgentBicFi;
    }

    public void setInstructingAgentBicFi(String instructingAgentBicFi) {
        this.instructingAgentBicFi = instructingAgentBicFi;
    }

    public String getInstructedAgentBicFi() {
        return instructedAgentBicFi;
    }

    public void setInstructedAgentBicFi(String instructedAgentBicFi) {
        this.instructedAgentBicFi = instructedAgentBicFi;
    }

    public String getDebtorName() {
        return debtorName;
    }

    public void setDebtorName(String debtorName) {
        this.debtorName = debtorName;
    }

    public List<String> getDebtorAddressLines() {
        return debtorAddressLines;
    }

    public void setDebtorAddressLines(List<String> debtorAddressLines) {
        this.debtorAddressLines = debtorAddressLines;
    }

    public String getDebtorAccountIban() {
        return debtorAccountIban;
    }

    public void setDebtorAccountIban(String debtorAccountIban) {
        this.debtorAccountIban = debtorAccountIban;
    }

    public String getDebtorAgentBicFi() {
        return debtorAgentBicFi;
    }

    public void setDebtorAgentBicFi(String debtorAgentBicFi) {
        this.debtorAgentBicFi = debtorAgentBicFi;
    }

    public String getCreditorName() {
        return creditorName;
    }

    public void setCreditorName(String creditorName) {
        this.creditorName = creditorName;
    }

    public List<String> getCreditorAddressLines() {
        return creditorAddressLines;
    }

    public void setCreditorAddressLines(List<String> creditorAddressLines) {
        this.creditorAddressLines = creditorAddressLines;
    }

    public String getCreditorAccountIban() {
        return creditorAccountIban;
    }

    public void setCreditorAccountIban(String creditorAccountIban) {
        this.creditorAccountIban = creditorAccountIban;
    }

    public String getCreditorAgentBicFi() {
        return creditorAgentBicFi;
    }

    public void setCreditorAgentBicFi(String creditorAgentBicFi) {
        this.creditorAgentBicFi = creditorAgentBicFi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAuthStatus() {
        return authStatus;
    }

    public void setAuthStatus(String authStatus) {
        this.authStatus = authStatus;
    }

    public List<String> getRejectionCodes() {
        return rejectionCodes;
    }

    public void setRejectionCodes(List<String> rejectionCodes) {
        this.rejectionCodes = rejectionCodes;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        Payment payment = (Payment) object;

        if (numberOfTransactions != payment.numberOfTransactions) return false;
        if (!Objects.equals(messageId, payment.messageId)) return false;
        if (!Objects.equals(creationDateTime, payment.creationDateTime))
            return false;
        if (!Objects.equals(settlementMethod, payment.settlementMethod))
            return false;
        if (!Objects.equals(instructionId, payment.instructionId))
            return false;
        if (!Objects.equals(endToEndId, payment.endToEndId)) return false;
        if (!Objects.equals(transactionId, payment.transactionId))
            return false;
        if (!Objects.equals(clearingChannel, payment.clearingChannel))
            return false;
        if (!Objects.equals(serviceLevel, payment.serviceLevel))
            return false;
        if (!Objects.equals(localInstrument, payment.localInstrument))
            return false;
        if (!Objects.equals(categoryPurpose, payment.categoryPurpose))
            return false;
        if (!Objects.equals(interBankSettlementCurrency, payment.interBankSettlementCurrency))
            return false;
        if (!Objects.equals(interBankSettlementAmount, payment.interBankSettlementAmount))
            return false;
        if (!Objects.equals(interBankSettlementDate, payment.interBankSettlementDate))
            return false;
        if (!Objects.equals(chargeBearer, payment.chargeBearer))
            return false;
        if (!Objects.equals(instructingAgentBicFi, payment.instructingAgentBicFi))
            return false;
        if (!Objects.equals(instructedAgentBicFi, payment.instructedAgentBicFi))
            return false;
        if (!Objects.equals(debtorName, payment.debtorName)) return false;
        if (!Objects.equals(debtorAddressLines, payment.debtorAddressLines))
            return false;
        if (!Objects.equals(debtorAccountIban, payment.debtorAccountIban))
            return false;
        if (!Objects.equals(debtorAgentBicFi, payment.debtorAgentBicFi))
            return false;
        if (!Objects.equals(creditorName, payment.creditorName))
            return false;
        if (!Objects.equals(creditorAddressLines, payment.creditorAddressLines))
            return false;
        if (!Objects.equals(creditorAccountIban, payment.creditorAccountIban))
            return false;
        if (!Objects.equals(creditorAgentBicFi, payment.creditorAgentBicFi))
            return false;
        if (!Objects.equals(status, payment.status)) return false;
        if (!Objects.equals(authStatus, payment.authStatus)) return false;
        return Objects.equals(rejectionCodes, payment.rejectionCodes);
    }

    @Override
    public int hashCode() {
        int result = messageId != null ? messageId.hashCode() : 0;
        result = 31 * result + (creationDateTime != null ? creationDateTime.hashCode() : 0);
        result = 31 * result + numberOfTransactions;
        result = 31 * result + (settlementMethod != null ? settlementMethod.hashCode() : 0);
        result = 31 * result + (instructionId != null ? instructionId.hashCode() : 0);
        result = 31 * result + (endToEndId != null ? endToEndId.hashCode() : 0);
        result = 31 * result + (transactionId != null ? transactionId.hashCode() : 0);
        result = 31 * result + (clearingChannel != null ? clearingChannel.hashCode() : 0);
        result = 31 * result + (serviceLevel != null ? serviceLevel.hashCode() : 0);
        result = 31 * result + (localInstrument != null ? localInstrument.hashCode() : 0);
        result = 31 * result + (categoryPurpose != null ? categoryPurpose.hashCode() : 0);
        result = 31 * result + (interBankSettlementCurrency != null ? interBankSettlementCurrency.hashCode() : 0);
        result = 31 * result + (interBankSettlementAmount != null ? interBankSettlementAmount.hashCode() : 0);
        result = 31 * result + (interBankSettlementDate != null ? interBankSettlementDate.hashCode() : 0);
        result = 31 * result + (chargeBearer != null ? chargeBearer.hashCode() : 0);
        result = 31 * result + (instructingAgentBicFi != null ? instructingAgentBicFi.hashCode() : 0);
        result = 31 * result + (instructedAgentBicFi != null ? instructedAgentBicFi.hashCode() : 0);
        result = 31 * result + (debtorName != null ? debtorName.hashCode() : 0);
        result = 31 * result + (debtorAddressLines != null ? debtorAddressLines.hashCode() : 0);
        result = 31 * result + (debtorAccountIban != null ? debtorAccountIban.hashCode() : 0);
        result = 31 * result + (debtorAgentBicFi != null ? debtorAgentBicFi.hashCode() : 0);
        result = 31 * result + (creditorName != null ? creditorName.hashCode() : 0);
        result = 31 * result + (creditorAddressLines != null ? creditorAddressLines.hashCode() : 0);
        result = 31 * result + (creditorAccountIban != null ? creditorAccountIban.hashCode() : 0);
        result = 31 * result + (creditorAgentBicFi != null ? creditorAgentBicFi.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (authStatus != null ? authStatus.hashCode() : 0);
        result = 31 * result + (rejectionCodes != null ? rejectionCodes.hashCode() : 0);
        return result;
    }

    public String getXmlPayment() {
        return xmlPayment;
    }

    public void setXmlPayment(String xmlPayment) {
        this.xmlPayment = xmlPayment;
    }

    public String getXmlReply() {
        return xmlReply;
    }

    public void setXmlReply(String xmlReply) {
        this.xmlReply = xmlReply;
    }
}
