package org.progressoft.entity;

import org.progressoft.enums.Reason;
import org.progressoft.enums.Status;

import java.time.LocalDateTime;

public class Reply {
    private String messageId;
    private Reason reason;
    private Status status;
    private String paymentMessageId;
    private LocalDateTime creationDateTime;

    private Reason responseReason;

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setPaymentMessageId(String paymentMessageId) {
        this.paymentMessageId = paymentMessageId;
    }

    public String getPaymentMessageId() {
        return paymentMessageId;
    }

    public void setCreationDateTime(LocalDateTime creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    public LocalDateTime getCreationDateTime() {
        return creationDateTime;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }

    public void setReason(Reason reason) {
        this.reason = reason;
    }

    public Reason getReason() {
        return reason;
    }

    public Reason getResponseReason() {
        return responseReason;
    }

    public void setResponseReason(Reason responseReason) {
        this.responseReason = responseReason;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        Reply reply = (Reply) object;

        if (!messageId.equals(reply.messageId)) return false;
        if (reason != reply.reason) return false;
        if (status != reply.status) return false;
        if (!paymentMessageId.equals(reply.paymentMessageId)) return false;
        if (!creationDateTime.equals(reply.creationDateTime)) return false;
        return responseReason == reply.responseReason;
    }

    @Override
    public int hashCode() {
        int result = messageId.hashCode();
        result = 31 * result + reason.hashCode();
        result = 31 * result + status.hashCode();
        result = 31 * result + paymentMessageId.hashCode();
        result = 31 * result + creationDateTime.hashCode();
        return result;
    }
}
