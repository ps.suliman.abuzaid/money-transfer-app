package org.progressoft.entity;

public class PurposeCode {
    private String code;
    private String usage;

    public PurposeCode(String code, String usage) {
        this.code = code;
        this.usage = usage;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }
}
