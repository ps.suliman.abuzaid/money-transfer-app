package org.progressoft.entity;

import org.progressoft.enums.Reason;

import java.time.LocalDate;

public class ResponseData {
    private String messageId;
    private String paymentMessageId;
    private String instructionId;
    private String endToEndId;
    private String transactionId;
    private LocalDate settlementDate;
    private Reason reason;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getPaymentMessageId() {
        return paymentMessageId;
    }

    public void setPaymentMessageId(String paymentMessageId) {
        this.paymentMessageId = paymentMessageId;
    }

    public String getInstructionId() {
        return instructionId;
    }

    public void setInstructionId(String instructionId) {
        this.instructionId = instructionId;
    }

    public String getEndToEndId() {
        return endToEndId;
    }

    public void setEndToEndId(String endToEndId) {
        this.endToEndId = endToEndId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public LocalDate getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(LocalDate settlementDate) {
        this.settlementDate = settlementDate;
    }

    public Reason getReason() {
        return reason;
    }

    public void setReason(Reason reason) {
        this.reason = reason;
    }
}
