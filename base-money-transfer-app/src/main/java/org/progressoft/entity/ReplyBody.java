package org.progressoft.entity;

public class ReplyBody {
    private final String xmlReply;
    private final Reply reply;

    public ReplyBody(String xmlReply, Reply reply) {
        this.xmlReply = xmlReply;
        this.reply = reply;
    }

    public String getXmlReply() {
        return xmlReply;
    }

    public Reply getReply() {
        return reply;
    }
}
