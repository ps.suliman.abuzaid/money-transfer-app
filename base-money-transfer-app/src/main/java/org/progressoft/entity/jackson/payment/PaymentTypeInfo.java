package org.progressoft.entity.jackson.payment;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.progressoft.entity.jackson.Proprietary;

public class PaymentTypeInfo {

    @JacksonXmlProperty(localName = "ClrChanl")
    private String clearingChannel;

    @JacksonXmlProperty(localName = "SvcLvl")
    private Proprietary serviceLevel;

    @JacksonXmlProperty(localName = "LclInstrm")
    private Proprietary localInstrument;

    @JacksonXmlProperty(localName = "CtgyPurp")
    private Proprietary categoryPurpose;

    public String getClearingChannel() {
        return clearingChannel;
    }

    public void setClearingChannel(String clearingChannel) {
        this.clearingChannel = clearingChannel;
    }

    public Proprietary getServiceLevel() {
        return serviceLevel;
    }

    public void setServiceLevel(Proprietary serviceLevel) {
        this.serviceLevel = serviceLevel;
    }

    public Proprietary getLocalInstrument() {
        return localInstrument;
    }

    public void setLocalInstrument(Proprietary localInstrument) {
        this.localInstrument = localInstrument;
    }

    public Proprietary getCategoryPurpose() {
        return categoryPurpose;
    }

    public void setCategoryPurpose(Proprietary categoryPurpose) {
        this.categoryPurpose = categoryPurpose;
    }
}
