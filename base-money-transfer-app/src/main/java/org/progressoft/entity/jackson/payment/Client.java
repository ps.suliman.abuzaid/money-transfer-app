package org.progressoft.entity.jackson.payment;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

public class Client {

    @JacksonXmlProperty(localName = "Nm")
    private String name;

    @JacksonXmlProperty(localName = "PstlAdr")
    private PostalAddress postalAddress;

    public Client() {
    }

    public Client(String name, List<String> addressLines) {
        this.name = name;
        this.postalAddress = new PostalAddress();
        this.postalAddress.addressLines = addressLines;
    }

    public static class PostalAddress {

        @JacksonXmlElementWrapper(useWrapping = false)
        @JacksonXmlProperty(localName = "AdrLine")
        private List<String> addressLines;

        public List<String> getAddressLines() {
            return addressLines;
        }

        public void setAddressLines(List<String> addressLines) {
            this.addressLines = addressLines;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PostalAddress getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(PostalAddress postalAddress) {
        this.postalAddress = postalAddress;
    }
}
