package org.progressoft.entity.jackson.payment;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Agent {
    @JacksonXmlProperty(localName = "FinInstnId")
    private FinancialInstitutionId institutionId;

    public Agent() {
    }

    public Agent(String bicFi) {
        this.institutionId = new FinancialInstitutionId();
        this.institutionId.bicFi = bicFi;
    }

    public static class FinancialInstitutionId {
        @JacksonXmlProperty(localName = "BICFI")
        private String bicFi;

        public String getBicFi() {
            return bicFi;
        }

        public void setBicFi(String bicFi) {
            this.bicFi = bicFi;
        }
    }

    public FinancialInstitutionId getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(FinancialInstitutionId institutionId) {
        this.institutionId = institutionId;
    }
}
