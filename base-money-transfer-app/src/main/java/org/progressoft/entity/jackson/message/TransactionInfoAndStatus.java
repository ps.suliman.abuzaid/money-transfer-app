package org.progressoft.entity.jackson.message;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.progressoft.entity.jackson.payment.Agent;

import java.util.Set;

public class TransactionInfoAndStatus {
    @JacksonXmlProperty(localName = "OrgnlInstrId")
    private String originalInstructionId;

    @JacksonXmlProperty(localName = "OrgnlEndToEndId")
    private String originalEndToEndId;

    @JacksonXmlProperty(localName = "OrgnlTxId")
    private String originalTransactionId;

    @JacksonXmlProperty(localName = "InstgAgt")
    private Agent instructingAgent;

    @JacksonXmlProperty(localName = "InstdAgt")
    private Agent instructedAgent;

    @JacksonXmlProperty(localName = "OrgnlTxRef")
    private OriginalTransactionRef originalTransactionRef;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "StsRsnInf")
    private Set<StatusReasonInfo> statusReasons;

    public void setOriginalInstructionId(String originalInstructionId) {
        this.originalInstructionId = originalInstructionId;
    }

    public String getOriginalEndToEndId() {
        return originalEndToEndId;
    }

    public void setOriginalEndToEndId(String originalEndToEndId) {
        this.originalEndToEndId = originalEndToEndId;
    }

    public String getOriginalTransactionId() {
        return originalTransactionId;
    }

    public void setOriginalTransactionId(String originalTransactionId) {
        this.originalTransactionId = originalTransactionId;
    }

    public Agent getInstructingAgent() {
        return instructingAgent;
    }

    public void setInstructingAgent(Agent instructingAgent) {
        this.instructingAgent = instructingAgent;
    }

    public Agent getInstructedAgent() {
        return instructedAgent;
    }

    public void setInstructedAgent(Agent instructedAgent) {
        this.instructedAgent = instructedAgent;
    }

    public OriginalTransactionRef getOriginalTransactionRef() {
        return originalTransactionRef;
    }

    public void setOriginalTransactionRef(OriginalTransactionRef originalTransactionRef) {
        this.originalTransactionRef = originalTransactionRef;
    }

    public Set<StatusReasonInfo> getStatusReasons() {
        return statusReasons;
    }

    public String getOriginalInstructionId() {
        return originalInstructionId;
    }

    public void setStatusReasons(Set<StatusReasonInfo> statusReasons) {
        this.statusReasons = statusReasons;
    }
}
