package org.progressoft.entity.jackson.message;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.progressoft.entity.jackson.Proprietary;

public class StatusReasonInfo {

    @JacksonXmlProperty(localName = "Rsn")
    private Proprietary reason;

    @JacksonXmlProperty(localName = "AddtlInf")
    private String additionalInfo;

    public StatusReasonInfo() {
    }

    public StatusReasonInfo(Proprietary reason) {
        this.reason = reason;
    }

    public StatusReasonInfo(Proprietary reason, String additionalInfo) {
        this.reason = reason;
        this.additionalInfo = additionalInfo;
    }

    public Proprietary getReason() {
        return reason;
    }

    public void setReason(Proprietary reason) {
        this.reason = reason;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }
}
