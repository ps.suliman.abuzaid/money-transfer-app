package org.progressoft.entity.jackson;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Proprietary {

    @JacksonXmlProperty(localName = "Prtry")
    private String proprietary;

    public Proprietary() {
    }

    public Proprietary(String proprietary) {
        this.proprietary = proprietary;
    }

    public String getProprietary() {
        return proprietary;
    }

    public Proprietary setProprietary(String proprietary) {
        this.proprietary = proprietary;
        return this;
    }
}
