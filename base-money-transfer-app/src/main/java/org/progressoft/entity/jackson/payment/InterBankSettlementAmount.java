package org.progressoft.entity.jackson.payment;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;

import java.math.BigDecimal;

public class InterBankSettlementAmount {

    @JacksonXmlProperty(isAttribute = true, localName = "Ccy")
    private String currency;

    @JacksonXmlText
    private BigDecimal value;

    public InterBankSettlementAmount() {
    }

    public InterBankSettlementAmount(String currency, BigDecimal value) {
        this.currency = currency;
        this.value = value;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
