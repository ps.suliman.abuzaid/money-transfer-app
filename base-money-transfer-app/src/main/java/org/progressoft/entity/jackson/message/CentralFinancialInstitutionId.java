package org.progressoft.entity.jackson.message;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.progressoft.contract.EnvironmentPropertyService;
import org.progressoft.service.DefaultEnvironmentPropertyService;

public class CentralFinancialInstitutionId {
    @JacksonXmlProperty(localName = "ClrSysMmbId")
    private ClearingSystemMemberId systemMemberId;

    public CentralFinancialInstitutionId() {
        EnvironmentPropertyService environmentPropertyService;
        environmentPropertyService = new DefaultEnvironmentPropertyService();
        this.systemMemberId = new ClearingSystemMemberId(environmentPropertyService.getProperty("bank.central.bic"));
    }

    public ClearingSystemMemberId getSystemMemberId() {
        return systemMemberId;
    }

    public void setSystemMemberId(ClearingSystemMemberId systemMemberId) {
        this.systemMemberId = systemMemberId;
    }

    public static class ClearingSystemMemberId {
        @JacksonXmlProperty(localName = "MmbId")
        private String memberId;

        public ClearingSystemMemberId() {
        }

        public ClearingSystemMemberId(String memberId) {
            this.memberId = memberId;
        }

        public String getMemberId() {
            return memberId;
        }

        public void setMemberId(String memberId) {
            this.memberId = memberId;
        }
    }

}
