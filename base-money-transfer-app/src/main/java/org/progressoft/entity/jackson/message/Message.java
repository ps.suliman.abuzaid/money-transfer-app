package org.progressoft.entity.jackson.message;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "FIToFIPmtStsRpt")
public class Message {

    @JacksonXmlProperty(localName = "GrpHdr")
    private MessageGroupHeader messageGroupHeader;

    @JacksonXmlProperty(localName = "OrgnlGrpInfAndSts")
    private OriginalGroupInfoAndStatus groupInfoAndStatus;

    @JacksonXmlProperty(localName = "TxInfAndSts")
    private TransactionInfoAndStatus transactionInfoAndStatus;

    public MessageGroupHeader getMessageGroupHeader() {
        return messageGroupHeader;
    }

    public void setMessageGroupHeader(MessageGroupHeader messageGroupHeader) {
        this.messageGroupHeader = messageGroupHeader;
    }

    public OriginalGroupInfoAndStatus getGroupInfoAndStatus() {
        return groupInfoAndStatus;
    }

    public void setGroupInfoAndStatus(OriginalGroupInfoAndStatus groupInfoAndStatus) {
        this.groupInfoAndStatus = groupInfoAndStatus;
    }

    public TransactionInfoAndStatus getTransactionInfoAndStatus() {
        return transactionInfoAndStatus;
    }

    public void setTransactionInfoAndStatus(TransactionInfoAndStatus transactionInfoAndStatus) {
        this.transactionInfoAndStatus = transactionInfoAndStatus;
    }
}
