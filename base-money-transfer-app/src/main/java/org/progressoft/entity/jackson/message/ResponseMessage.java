package org.progressoft.entity.jackson.message;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "FIToFIPmtStsRpt")
public class ResponseMessage {
    @JacksonXmlProperty(localName = "GrpHdr")
    private ResponseGroupHeader responseGroupHeader;

    @JacksonXmlProperty(localName = "OrgnlGrpInfAndSts")
    private OriginalGroupInfoAndStatus groupInfoAndStatus;

    @JacksonXmlProperty(localName = "TxInfAndSts")
    private TransactionInfoAndStatus transactionInfoAndStatus;

    public ResponseGroupHeader getResponseGroupHeader() {
        return responseGroupHeader;
    }

    public void setResponseGroupHeader(ResponseGroupHeader responseGroupHeader) {
        this.responseGroupHeader = responseGroupHeader;
    }

    public OriginalGroupInfoAndStatus getGroupInfoAndStatus() {
        return groupInfoAndStatus;
    }

    public void setGroupInfoAndStatus(OriginalGroupInfoAndStatus groupInfoAndStatus) {
        this.groupInfoAndStatus = groupInfoAndStatus;
    }

    public TransactionInfoAndStatus getTransactionInfoAndStatus() {
        return transactionInfoAndStatus;
    }

    public void setTransactionInfoAndStatus(TransactionInfoAndStatus transactionInfoAndStatus) {
        this.transactionInfoAndStatus = transactionInfoAndStatus;
    }
}
