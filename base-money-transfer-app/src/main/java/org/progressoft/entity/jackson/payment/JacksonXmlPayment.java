package org.progressoft.entity.jackson.payment;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "FIToFICstmrCdtTrf")
public class JacksonXmlPayment {

    @JacksonXmlProperty(localName = "GrpHdr")
    private GroupHeader groupHeader;

    @JacksonXmlProperty(localName = "CdtTrfTxInf")
    private CreditTransferTransactionInfo transferTransactionInfo;

    public GroupHeader getGroupHeader() {
        return groupHeader;
    }

    public void setGroupHeader(GroupHeader groupHeader) {
        this.groupHeader = groupHeader;
    }

    public CreditTransferTransactionInfo getTransferTransactionInfo() {
        return transferTransactionInfo;
    }

    public void setTransferTransactionInfo(CreditTransferTransactionInfo transferTransactionInfo) {
        this.transferTransactionInfo = transferTransactionInfo;
    }
}
