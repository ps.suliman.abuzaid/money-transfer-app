package org.progressoft.entity.jackson.message;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class OriginalGroupInfoAndStatus {

    @JacksonXmlProperty(localName = "OrgnlMsgId")
    private String originalMessageId;

    @JacksonXmlProperty(localName = "OrgnlMsgNmId")
    private String originalMessageNumberId;

    @JacksonXmlProperty(localName = "StsRsnInf")
    private StatusReasonInfo statusReasonInfo;

    public String getOriginalMessageId() {
        return originalMessageId;
    }

    public void setOriginalMessageId(String originalMessageId) {
        this.originalMessageId = originalMessageId;
    }

    public String getOriginalMessageNumberId() {
        return originalMessageNumberId;
    }

    public void setOriginalMessageNumberId(String originalMessageNumberId) {
        this.originalMessageNumberId = originalMessageNumberId;
    }

    public StatusReasonInfo getStatusReasonInfo() {
        return statusReasonInfo;
    }

    public void setStatusReasonInfo(StatusReasonInfo statusReasonInfo) {
        this.statusReasonInfo = statusReasonInfo;
    }
}
