package org.progressoft.entity.jackson.message;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "Document")
public class MessageDocumentWrapper {

    @JacksonXmlProperty(localName = "FIToFIPmtStsRpt")
    private Message message;

    @JacksonXmlProperty(isAttribute = true)
    private final String xmlns;

    public MessageDocumentWrapper(Message message) {
        this.message = message;
        this.xmlns = "urn:iso:std:iso:20022:tech:xsd:pacs.002.001.10";
    }


    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public String getXmlns() {
        return xmlns;
    }
}