package org.progressoft.entity.jackson.message;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.progressoft.entity.jackson.payment.Agent;

import java.time.LocalDateTime;

public class MessageGroupHeader {
    @JacksonXmlProperty(localName = "MsgId")
    private String messageId;

    @JacksonXmlProperty(localName = "CreDtTm")
    @JacksonXmlElementWrapper(useWrapping = false)
    private LocalDateTime creationDateTime;

    @JacksonXmlProperty(localName = "InstgAgt")
    private Agent instructingAgent;

    @JacksonXmlProperty(localName = "InstdAgt")
    private CentralAgent centralAgent;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public LocalDateTime getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(LocalDateTime creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    public Agent getInstructingAgent() {
        return instructingAgent;
    }

    public void setInstructingAgent(Agent instructingAgent) {
        this.instructingAgent = instructingAgent;
    }

    public CentralAgent getCentralAgent() {
        return centralAgent;
    }

    public void setCentralAgent(CentralAgent centralAgent) {
        this.centralAgent = centralAgent;
    }
}
