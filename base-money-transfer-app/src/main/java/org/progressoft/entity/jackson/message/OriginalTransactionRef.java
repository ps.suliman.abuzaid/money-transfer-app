package org.progressoft.entity.jackson.message;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import org.progressoft.entity.jackson.payment.InterBankSettlementAmount;

import java.time.LocalDate;

public class OriginalTransactionRef {
    @JacksonXmlProperty(localName = "IntrBkSttlmAmt")
    private InterBankSettlementAmount settlementAmount;

    @JacksonXmlProperty(localName = "IntrBkSttlmDt")
    private LocalDate interBankSettlementDate;

    public InterBankSettlementAmount getSettlementAmount() {
        return settlementAmount;
    }

    public void setSettlementAmount(InterBankSettlementAmount settlementAmount) {
        this.settlementAmount = settlementAmount;
    }

    public LocalDate getInterBankSettlementDate() {
        return interBankSettlementDate;
    }

    public void setInterBankSettlementDate(LocalDate interBankSettlementDate) {
        this.interBankSettlementDate = interBankSettlementDate;
    }
}
