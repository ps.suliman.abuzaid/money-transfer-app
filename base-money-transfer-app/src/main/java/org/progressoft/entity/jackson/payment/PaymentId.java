package org.progressoft.entity.jackson.payment;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class PaymentId {

    @JacksonXmlProperty(localName = "InstrId")
    private String instructionId;

    @JacksonXmlProperty(localName = "EndToEndId")
    private String endToEndId;

    @JacksonXmlProperty(localName = "TxId")
    private String transactionId;


    public String getInstructionId() {
        return instructionId;
    }

    public void setInstructionId(String instructionId) {
        this.instructionId = instructionId;
    }

    public String getEndToEndId() {
        return endToEndId;
    }

    public void setEndToEndId(String endToEndId) {
        this.endToEndId = endToEndId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
