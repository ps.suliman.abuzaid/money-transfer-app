package org.progressoft.entity.jackson.payment;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Account {

    @JacksonXmlProperty(localName = "Id")
    private AccountId id;

    public Account() {
    }

    public Account(String iban) {
        this.id = new AccountId();
        id.Iban = iban;
    }

    public static class AccountId {
        @JacksonXmlProperty(localName = "IBAN")
        private String Iban;

        public String getIban() {
            return Iban;
        }

        public void setIban(String iban) {
            Iban = iban;
        }
    }

    public AccountId getId() {
        return id;
    }

    public void setId(AccountId id) {
        this.id = id;
    }
}
