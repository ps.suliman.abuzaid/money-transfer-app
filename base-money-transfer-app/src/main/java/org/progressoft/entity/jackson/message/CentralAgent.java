package org.progressoft.entity.jackson.message;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class CentralAgent {
    @JacksonXmlProperty(localName = "FinInstnId")
    private CentralFinancialInstitutionId financialInstitutionId;

    public CentralAgent() {
        this.financialInstitutionId = new CentralFinancialInstitutionId();
    }

    public CentralAgent(String bic) {
        this.financialInstitutionId = new CentralFinancialInstitutionId();
        financialInstitutionId.setSystemMemberId(new CentralFinancialInstitutionId.ClearingSystemMemberId(bic));
    }

    public CentralFinancialInstitutionId getFinancialInstitutionId() {
        return financialInstitutionId;
    }

    public void setFinancialInstitutionId(CentralFinancialInstitutionId financialInstitutionId) {
        this.financialInstitutionId = financialInstitutionId;
    }
}
