package org.progressoft.entity.jackson.payment;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.time.LocalDateTime;

public class GroupHeader {

    @JacksonXmlProperty(localName = "MsgId")
    private String messageId;

    @JacksonXmlProperty(localName = "CreDtTm")
    private LocalDateTime creationDateTime;

    @JacksonXmlProperty(localName = "NbOfTxs")
    private int numberOfTransactions;

    @JacksonXmlProperty(localName = "SttlmInf")
    private SettlementInfo settlementInfo;

    public static class SettlementInfo {

        @JacksonXmlProperty(localName = "SttlmMtd")
        private String settlementMethodCode;

        public String getSettlementMethodCode() {
            return settlementMethodCode;
        }

        public void setSettlementMethodCode(String settlementMethodCode) {
            this.settlementMethodCode = settlementMethodCode;
        }
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public LocalDateTime getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(LocalDateTime creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    public int getNumberOfTransactions() {
        return numberOfTransactions;
    }

    public void setNumberOfTransactions(int numberOfTransactions) {
        this.numberOfTransactions = numberOfTransactions;
    }

    public SettlementInfo getSettlementInfo() {
        return settlementInfo;
    }

    public void setSettlementInfo(SettlementInfo settlementInfo) {
        this.settlementInfo = settlementInfo;
    }
}
