package org.progressoft.entity.jackson.payment;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.time.LocalDate;

public class CreditTransferTransactionInfo {

    @JacksonXmlProperty(localName = "PmtId")
    private PaymentId paymentId;

    @JacksonXmlProperty(localName = "PmtTpInf")
    private PaymentTypeInfo paymentTypeInfo;

    @JacksonXmlProperty(localName = "IntrBkSttlmAmt")
    private InterBankSettlementAmount settlementAmount;

    @JacksonXmlProperty(localName = "IntrBkSttlmDt")
    private LocalDate interBankSettlementDate;

    @JacksonXmlProperty(localName = "ChrgBr")
    private String chargeBearer;

    @JacksonXmlProperty(localName = "InstgAgt")
    private Agent instructingAgent;

    @JacksonXmlProperty(localName = "InstdAgt")
    private Agent instructedAgent;

    @JacksonXmlProperty(localName = "Dbtr")
    private Client debtor;

    @JacksonXmlProperty(localName = "DbtrAcct")
    private Account debtorAccount;

    @JacksonXmlProperty(localName = "DbtrAgt")
    private Agent debtorAgent;

    @JacksonXmlProperty(localName = "Cdtr")
    private Client creditor;

    @JacksonXmlProperty(localName = "CdtrAcct")
    private Account creditorAccount;

    @JacksonXmlProperty(localName = "CdtrAgt")
    private Agent creditorAgent;


    public PaymentId getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(PaymentId paymentId) {
        this.paymentId = paymentId;
    }

    public PaymentTypeInfo getPaymentTypeInfo() {
        return paymentTypeInfo;
    }

    public void setPaymentTypeInfo(PaymentTypeInfo paymentTypeInfo) {
        this.paymentTypeInfo = paymentTypeInfo;
    }

    public InterBankSettlementAmount getSettlementAmount() {
        return settlementAmount;
    }

    public void setSettlementAmount(InterBankSettlementAmount settlementAmount) {
        this.settlementAmount = settlementAmount;
    }

    public LocalDate getInterBankSettlementDate() {
        return interBankSettlementDate;
    }

    public void setInterBankSettlementDate(LocalDate interBankSettlementDate) {
        this.interBankSettlementDate = interBankSettlementDate;
    }

    public String getChargeBearer() {
        return chargeBearer;
    }

    public void setChargeBearer(String chargeBearer) {
        this.chargeBearer = chargeBearer;
    }

    public Agent getInstructingAgent() {
        return instructingAgent;
    }

    public void setInstructingAgent(Agent instructingAgent) {
        this.instructingAgent = instructingAgent;
    }

    public Agent getInstructedAgent() {
        return instructedAgent;
    }

    public void setInstructedAgent(Agent instructedAgent) {
        this.instructedAgent = instructedAgent;
    }

    public Client getDebtor() {
        return debtor;
    }

    public void setDebtor(Client debtor) {
        this.debtor = debtor;
    }

    public Account getDebtorAccount() {
        return debtorAccount;
    }

    public void setDebtorAccount(Account debtorAccount) {
        this.debtorAccount = debtorAccount;
    }

    public Agent getDebtorAgent() {
        return debtorAgent;
    }

    public void setDebtorAgent(Agent debtorAgent) {
        this.debtorAgent = debtorAgent;
    }

    public Client getCreditor() {
        return creditor;
    }

    public void setCreditor(Client creditor) {
        this.creditor = creditor;
    }

    public Account getCreditorAccount() {
        return creditorAccount;
    }

    public void setCreditorAccount(Account creditorAccount) {
        this.creditorAccount = creditorAccount;
    }

    public Agent getCreditorAgent() {
        return creditorAgent;
    }

    public void setCreditorAgent(Agent creditorAgent) {
        this.creditorAgent = creditorAgent;
    }
}
