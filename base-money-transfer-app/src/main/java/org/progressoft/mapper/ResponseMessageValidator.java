package org.progressoft.mapper;

import org.progressoft.contract.EnvironmentPropertyService;
import org.progressoft.entity.jackson.message.CentralAgent;
import org.progressoft.entity.jackson.message.ResponseMessage;
import org.progressoft.entity.jackson.payment.Agent;
import org.progressoft.exception.ResponseNotValidException;
import org.progressoft.service.DefaultEnvironmentPropertyService;

class ResponseMessageValidator {

    private final EnvironmentPropertyService environmentPropertyService;

    public ResponseMessageValidator() {
        this.environmentPropertyService = new DefaultEnvironmentPropertyService();
    }

    public void validateMessage(ResponseMessage message) {
        String currentBic = environmentPropertyService.getProperty("bank.default.bic");
        String centralBankBic = environmentPropertyService.getProperty("bank.central.bic");
        CentralAgent centralAgent = message.getResponseGroupHeader().getCentralAgent();
        Agent instructedAgent = message.getResponseGroupHeader().getInstructedAgent();
        String messageCentralBankBic = centralAgent.getFinancialInstitutionId().getSystemMemberId().getMemberId();
        String messageInstructedBic = instructedAgent.getInstitutionId().getBicFi();
        if (!centralBankBic.equals(messageCentralBankBic) || !currentBic.equals(messageInstructedBic))
            throw new ResponseNotValidException("values received not correct");
    }

}
