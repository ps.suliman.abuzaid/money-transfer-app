package org.progressoft.mapper;

import org.progressoft.entity.Payment;
import org.progressoft.entity.jackson.Proprietary;
import org.progressoft.entity.jackson.payment.*;
import org.progressoft.exception.NullJacksonPaymentException;

import java.util.Objects;

class JacksonPaymentMapper {
    public Payment convert(JacksonXmlPayment paymentObject) {
        if (Objects.isNull(paymentObject)) {
            throw new NullJacksonPaymentException("paymentObject is null");
        }
        Payment payment = new Payment();
        fillGroupHeaderData(paymentObject, payment);
        fillCreditTransferTransactionInfo(paymentObject, payment);
        return payment;
    }

    private void fillCreditTransferTransactionInfo(JacksonXmlPayment paymentObject, Payment payment) {
        CreditTransferTransactionInfo transactionInfo = paymentObject.getTransferTransactionInfo();
        if (Objects.isNull(transactionInfo))
            return;
        fillPaymentId(payment, transactionInfo);
        fillPaymentTypeInfo(payment, transactionInfo);
        fillSettlementAmount(payment, transactionInfo);
        payment.setInterBankSettlementDate(transactionInfo.getInterBankSettlementDate());
        payment.setChargeBearer(transactionInfo.getChargeBearer());
        fillInstructingAgentBicFi(payment, transactionInfo);
        fillInstructedAgentBicFi(payment, transactionInfo);
        fillDebtor(payment, transactionInfo);
        fillCreditor(payment, transactionInfo);
        fillDebtorAccount(payment, transactionInfo);
        fillCreditorAccount(payment, transactionInfo);
        fillDebtorAgentBicFi(payment, transactionInfo);
        fillCreditorAgentBicFi(payment, transactionInfo);
    }

    private void fillCreditorAccount(Payment payment, CreditTransferTransactionInfo transactionInfo) {
        Account creditorAccount = transactionInfo.getCreditorAccount();
        if (Objects.isNull(creditorAccount) || isAccountIbanNull(creditorAccount))
            return;
        payment.setCreditorAccountIban(creditorAccount.getId().getIban());
    }

    private void fillDebtorAccount(Payment payment, CreditTransferTransactionInfo transactionInfo) {
        Account debtorAccount = transactionInfo.getDebtorAccount();
        if (Objects.isNull(debtorAccount) || isAccountIbanNull(debtorAccount))
            return;
        payment.setDebtorAccountIban(debtorAccount.getId().getIban());
    }

    private void fillDebtor(Payment payment, CreditTransferTransactionInfo transactionInfo) {
        Client debtor = transactionInfo.getDebtor();
        if (Objects.isNull(debtor))
            return;
        payment.setDebtorName(debtor.getName());
        if (Objects.isNull(debtor.getPostalAddress()))
            return;
        payment.setDebtorAddressLines(debtor.getPostalAddress().getAddressLines());
    }

    private void fillCreditor(Payment payment, CreditTransferTransactionInfo transactionInfo) {
        Client creditor = transactionInfo.getCreditor();
        if (Objects.isNull(creditor))
            return;
        payment.setCreditorName(creditor.getName());
        if (Objects.isNull(creditor.getPostalAddress()))
            return;
        payment.setCreditorAddressLines(creditor.getPostalAddress().getAddressLines());
    }

    private void fillInstructingAgentBicFi(Payment payment, CreditTransferTransactionInfo transactionInfo) {
        Agent instructingAgent = transactionInfo.getInstructingAgent();
        if (isAgentBciNull(instructingAgent))
            return;
        payment.setInstructingAgentBicFi(instructingAgent.getInstitutionId().getBicFi());
    }

    private void fillInstructedAgentBicFi(Payment payment, CreditTransferTransactionInfo transactionInfo) {
        Agent instructedAgent = transactionInfo.getInstructedAgent();
        if (isAgentBciNull(instructedAgent))
            return;
        payment.setInstructedAgentBicFi(instructedAgent.getInstitutionId().getBicFi());
    }

    private void fillDebtorAgentBicFi(Payment payment, CreditTransferTransactionInfo transactionInfo) {
        Agent debtorAgent = transactionInfo.getDebtorAgent();
        if (isAgentBciNull(debtorAgent))
            return;
        payment.setDebtorAgentBicFi(debtorAgent.getInstitutionId().getBicFi());
    }

    private void fillCreditorAgentBicFi(Payment payment, CreditTransferTransactionInfo transactionInfo) {
        Agent creditorAgent = transactionInfo.getCreditorAgent();
        if (isAgentBciNull(creditorAgent))
            return;
        payment.setCreditorAgentBicFi(creditorAgent.getInstitutionId().getBicFi());
    }

    private boolean isAgentBciNull(Agent agent) {
        return Objects.isNull(agent) || Objects.isNull(agent.getInstitutionId());
    }

    private boolean isAccountIbanNull(Account account) {
        return Objects.isNull(account) || Objects.isNull(account.getId());
    }

    private void fillSettlementAmount(Payment payment, CreditTransferTransactionInfo transactionInfo) {
        InterBankSettlementAmount settlementAmount = transactionInfo.getSettlementAmount();
        if (Objects.isNull(settlementAmount))
            return;
        payment.setInterBankSettlementCurrency(settlementAmount.getCurrency());
        payment.setInterBankSettlementAmount(settlementAmount.getValue());
    }

    private void fillPaymentTypeInfo(Payment payment, CreditTransferTransactionInfo transactionInfo) {
        PaymentTypeInfo paymentTypeInfo = transactionInfo.getPaymentTypeInfo();
        if (Objects.isNull(paymentTypeInfo))
            return;
        payment.setClearingChannel(paymentTypeInfo.getClearingChannel());
        Proprietary serviceLevel = paymentTypeInfo.getServiceLevel();
        if (!Objects.isNull(serviceLevel)) {
            payment.setServiceLevel(serviceLevel.getProprietary());
        }
        Proprietary localInstrument = paymentTypeInfo.getLocalInstrument();
        if (!Objects.isNull(localInstrument)) {
            payment.setLocalInstrument(localInstrument.getProprietary());
        }
        Proprietary categoryPurpose = paymentTypeInfo.getCategoryPurpose();
        if (!Objects.isNull(categoryPurpose)) {
            payment.setCategoryPurpose(categoryPurpose.getProprietary());
        }
    }

    private void fillPaymentId(Payment payment, CreditTransferTransactionInfo transactionInfo) {
        PaymentId paymentId = transactionInfo.getPaymentId();
        if (Objects.isNull(paymentId))
            return;
        payment.setInstructionId(paymentId.getInstructionId());
        payment.setEndToEndId(paymentId.getEndToEndId());
        payment.setTransactionId(paymentId.getTransactionId());
    }

    private void fillGroupHeaderData(JacksonXmlPayment paymentObject, Payment payment) {
        GroupHeader groupHeader = paymentObject.getGroupHeader();
        if (Objects.isNull(groupHeader))
            return;
        payment.setMessageId(groupHeader.getMessageId());
        payment.setCreationDateTime(groupHeader.getCreationDateTime());
        payment.setNumberOfTransactions(groupHeader.getNumberOfTransactions());
        GroupHeader.SettlementInfo settlementInfo = groupHeader.getSettlementInfo();
        if (Objects.isNull(settlementInfo))
            return;
        payment.setSettlementMethod(settlementInfo.getSettlementMethodCode());
    }
}
