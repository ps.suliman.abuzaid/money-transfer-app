package org.progressoft.mapper;

import org.progressoft.contract.ResponseParser;
import org.progressoft.contract.XmlBodyValidator;
import org.progressoft.entity.ResponseData;
import org.progressoft.entity.jackson.message.ResponseMessage;
import org.progressoft.enums.Reason;

public class DefaultResponseParser implements ResponseParser {
    private final XmlBodyValidator xmlResponseValidator;

    private final ResponseMessageMapper responseMessageMapper;

    private final ResponseMessageValidator responseMessageValidator;

    public DefaultResponseParser(XmlBodyValidator xmlResponseValidator) {
        this.xmlResponseValidator = xmlResponseValidator;
        this.responseMessageMapper = new ResponseMessageMapper();
        this.responseMessageValidator = new ResponseMessageValidator();
    }

    @Override
    public ResponseData parseResponse(String xmlResponse) {
        xmlResponseValidator.validateXmlBody(xmlResponse);
        ResponseMessage responseMessage = responseMessageMapper.parseXmlToMessage(xmlResponse);
        responseMessageValidator.validateMessage(responseMessage);
        return getResponseData(responseMessage);
    }

    private ResponseData getResponseData(ResponseMessage responseMessage) {
        ResponseData responseData = new ResponseData();
        responseData.setMessageId(responseMessage.getResponseGroupHeader().getMessageId());
        responseData.setPaymentMessageId(responseMessage.getGroupInfoAndStatus().getOriginalMessageId());
        responseData.setInstructionId(responseMessage.getTransactionInfoAndStatus().getOriginalInstructionId());
        responseData.setEndToEndId(responseMessage.getTransactionInfoAndStatus().getOriginalEndToEndId());
        responseData.setTransactionId(responseMessage.getTransactionInfoAndStatus().getOriginalTransactionId());
        responseData.setSettlementDate(responseMessage.getTransactionInfoAndStatus()
                .getOriginalTransactionRef().getInterBankSettlementDate());
        String messageReason = responseMessage.getGroupInfoAndStatus().getStatusReasonInfo().getReason().getProprietary();
        responseData.setReason(Reason.valueOf(messageReason));
        return responseData;
    }
}
