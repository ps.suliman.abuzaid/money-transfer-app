package org.progressoft.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.progressoft.entity.jackson.message.ResponseMessage;
import org.progressoft.factory.XmlMapperFactory;

class ResponseMessageMapper {

    private final XmlMapperFactory xmlMapperFactory;

    public ResponseMessageMapper() {
        this.xmlMapperFactory = new XmlMapperFactory();
    }

    public ResponseMessage parseXmlToMessage(String xmlResponse) {
        XmlMapper xmlMapper = xmlMapperFactory.getConfiguredMapper();
        ResponseMessage message;
        try {
            message = xmlMapper.readValue(xmlResponse, ResponseMessage.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return message;
    }
}
