package org.progressoft.mapper;

import org.progressoft.contract.PaymentParser;
import org.progressoft.contract.XmlBodyValidator;
import org.progressoft.entity.Payment;
import org.progressoft.entity.jackson.payment.JacksonXmlPayment;

public class JacksonPaymentParser implements PaymentParser {

    private final XmlToJacksonPaymentMapper xmlToJacksonPaymentMapper;
    private final JacksonPaymentMapper jacksonPaymentMapper;
    private final XmlBodyValidator xmlBodyValidator;

    public JacksonPaymentParser(XmlBodyValidator xmlBodyValidator) {
        this.xmlToJacksonPaymentMapper = new XmlToJacksonPaymentMapper();
        this.jacksonPaymentMapper = new JacksonPaymentMapper();
        this.xmlBodyValidator = xmlBodyValidator;
    }

    @Override
    public Payment parse(String xmlPayment) {
        xmlBodyValidator.validateXmlBody(xmlPayment);
        JacksonXmlPayment jacksonXmlPayment = xmlToJacksonPaymentMapper.convertXmlToObject(xmlPayment);
        return jacksonPaymentMapper.convert(jacksonXmlPayment);
    }
}
