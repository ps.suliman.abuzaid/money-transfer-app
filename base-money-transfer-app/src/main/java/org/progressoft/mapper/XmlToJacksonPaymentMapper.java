package org.progressoft.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.progressoft.entity.jackson.payment.JacksonXmlPayment;
import org.progressoft.exception.InvalidSchemaException;
import org.progressoft.factory.XmlMapperFactory;

class XmlToJacksonPaymentMapper {
    private final XmlMapperFactory xmlMapperFactory;

    public XmlToJacksonPaymentMapper() {
        this.xmlMapperFactory = new XmlMapperFactory();
    }

    public JacksonXmlPayment convertXmlToObject(String xmlPayment) {
        JacksonXmlPayment payment;
        try {
            payment = xmlMapperFactory.getConfiguredMapper().readValue(xmlPayment, JacksonXmlPayment.class);
        } catch (JsonProcessingException e) {
            throw new InvalidSchemaException("Xml string schema does not match expected schema", e);
        }
        return payment;
    }
}
