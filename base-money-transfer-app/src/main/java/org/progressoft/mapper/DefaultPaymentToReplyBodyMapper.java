package org.progressoft.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.progressoft.contract.MessageIdGenerator;
import org.progressoft.contract.PaymentToReplyBodyMapper;
import org.progressoft.entity.Payment;
import org.progressoft.entity.RejectionMotive;
import org.progressoft.entity.Reply;
import org.progressoft.entity.ReplyBody;
import org.progressoft.entity.jackson.Proprietary;
import org.progressoft.entity.jackson.message.*;
import org.progressoft.entity.jackson.payment.Agent;
import org.progressoft.entity.jackson.payment.InterBankSettlementAmount;
import org.progressoft.enums.Reason;
import org.progressoft.enums.Status;
import org.progressoft.exception.NullPaymentException;
import org.progressoft.exception.NullRejectionMotiveSetException;
import org.progressoft.factory.XmlMapperFactory;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class DefaultPaymentToReplyBodyMapper implements PaymentToReplyBodyMapper {

    private final MessageIdGenerator generator;

    private final XmlMapperFactory xmlMapperFactory;

    public DefaultPaymentToReplyBodyMapper(MessageIdGenerator generator) {
        this.generator = generator;
        this.xmlMapperFactory = new XmlMapperFactory();
    }

    @Override
    public ReplyBody convertPaymentToReplyBody(Payment payment, Set<RejectionMotive> rejectionMotives) {
        throwIfNull(payment, new NullPaymentException("payment is null"));
        throwIfNull(rejectionMotives, new NullRejectionMotiveSetException("rejectionMotives set is null"));
        String status = getMessageStatus(rejectionMotives);
        Message message = new Message();
        setMessageData(payment, rejectionMotives, message, status);
        String xmlReply = parseMessageToXmlString(message);
        Reply reply = getReply(payment, rejectionMotives);
        reply.setMessageId(message.getMessageGroupHeader().getMessageId());
        return new ReplyBody(xmlReply, reply);
    }

    private String parseMessageToXmlString(Message message) {
        String xmlReply;
        try {
            XmlMapper xmlMapper = xmlMapperFactory.getConfiguredMapper();
            xmlReply = xmlMapper.writeValueAsString(new MessageDocumentWrapper(message));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return xmlReply;
    }

    private static void throwIfNull(Object object, RuntimeException exception) {
        if (Objects.isNull(object)) {
            throw exception;
        }
    }

    private void setMessageData(Payment payment, Set<RejectionMotive> rejectionMotives, Message message, String status) {
        setGroupHeader(payment, message);
        setOriginalGroupInfoStatus(payment, message, status);
        setTransactionInfoAndStatus(payment, rejectionMotives, message);
    }

    private void setTransactionInfoAndStatus(Payment payment, Set<RejectionMotive> rejectionMotives, Message message) {
        TransactionInfoAndStatus transactionInfoAndStatus = new TransactionInfoAndStatus();
        transactionInfoAndStatus.setInstructingAgent(new Agent(payment.getInstructingAgentBicFi()));
        transactionInfoAndStatus.setInstructedAgent(new Agent(payment.getInstructedAgentBicFi()));
        transactionInfoAndStatus.setOriginalInstructionId(payment.getInstructionId());
        transactionInfoAndStatus.setOriginalEndToEndId(payment.getEndToEndId());
        transactionInfoAndStatus.setOriginalTransactionId(payment.getTransactionId());
        setOriginalTransactionRef(payment, transactionInfoAndStatus);
        setStatusReasons(rejectionMotives, transactionInfoAndStatus);
        message.setTransactionInfoAndStatus(transactionInfoAndStatus);
    }

    private void setStatusReasons(Set<RejectionMotive> rejectionMotives, TransactionInfoAndStatus transactionInfoAndStatus) {
        Set<StatusReasonInfo> statusReasons = rejectionMotives.stream().map(
                        rejectionMotive -> new StatusReasonInfo(new Proprietary(rejectionMotive.getMotiveCode()),
                                rejectionMotive.getMotiveDescription()))
                .collect(Collectors.toSet());
        transactionInfoAndStatus.setStatusReasons(statusReasons);
    }

    private void setOriginalTransactionRef(Payment payment, TransactionInfoAndStatus transactionInfoAndStatus) {
        OriginalTransactionRef originalTransactionRef = new OriginalTransactionRef();
        InterBankSettlementAmount settlementAmount = new InterBankSettlementAmount(
                payment.getInterBankSettlementCurrency(),
                payment.getInterBankSettlementAmount());
        originalTransactionRef.setSettlementAmount(settlementAmount);
        originalTransactionRef.setInterBankSettlementDate(payment.getInterBankSettlementDate());
        transactionInfoAndStatus.setOriginalTransactionRef(originalTransactionRef);
    }

    private String getMessageStatus(Set<RejectionMotive> rejectionMotives) {
        String reason = Reason.NAUT.toString();
        if (Objects.isNull(rejectionMotives) || rejectionMotives.isEmpty()) {
            reason = Reason.AUTH.toString();
        }
        return reason;
    }

    private void setOriginalGroupInfoStatus(Payment payment, Message message, String reason) {
        OriginalGroupInfoAndStatus groupInfoAndStatus = new OriginalGroupInfoAndStatus();
        groupInfoAndStatus.setOriginalMessageId(payment.getMessageId());
        groupInfoAndStatus.setOriginalMessageNumberId("pacs.008.001.08");
        groupInfoAndStatus.setStatusReasonInfo(new StatusReasonInfo(new Proprietary(reason)));
        message.setGroupInfoAndStatus(groupInfoAndStatus);
    }

    private void setGroupHeader(Payment payment, Message message) {
        MessageGroupHeader groupHeader = new MessageGroupHeader();
        groupHeader.setMessageId(generator.generateUniqueId());
        groupHeader.setCreationDateTime(LocalDateTime.now());
        groupHeader.setInstructingAgent(new Agent(payment.getCreditorAgentBicFi()));
        groupHeader.setCentralAgent(new CentralAgent());
        message.setMessageGroupHeader(groupHeader);
    }

    private Reply getReply(Payment payment, Set<RejectionMotive> rejectionMotives) {
        Reply reply = new Reply();
        reply.setCreationDateTime(LocalDateTime.now());
        reply.setStatus(Status.PENDING_RESPONSE);
        reply.setPaymentMessageId(payment.getMessageId());
        reply.setReason(Reason.AUTH);
        if (!rejectionMotives.isEmpty())
            reply.setReason(Reason.NAUT);
        return reply;
    }
}
