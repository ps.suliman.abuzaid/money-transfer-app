package org.progressoft.factory;

import org.h2.jdbcx.JdbcDataSource;
import org.progressoft.contract.*;
import org.progressoft.mapper.DefaultPaymentToReplyBodyMapper;
import org.progressoft.mapper.DefaultResponseParser;
import org.progressoft.mapper.JacksonPaymentParser;
import org.progressoft.repository.*;
import org.progressoft.service.*;
import org.progressoft.validator.DefaultPaymentValidator;
import org.progressoft.validator.DefaultXmlBodyValidator;

import javax.sql.DataSource;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.UUID;

public class DefaultBeanFactory implements BeanFactory {
    @Override
    public XmlBodyValidator getXmlPaymentValidator() {
        return new DefaultXmlBodyValidator(Paths.get("base-money-transfer-app", "src", "main", "resources", "schema", "payment_schema.xsd"));
    }

    @Override
    public PaymentParser getPaymentParser() {
        return new JacksonPaymentParser(getXmlPaymentValidator());
    }

    @Override
    public PaymentValidator getPaymentValidator() {
        return new DefaultPaymentValidator(getBankConfiguration(), getAccountValidator());
    }

    @Override
    public EnvironmentPropertyService getEnvironmentPropertyService() {
        return new DefaultEnvironmentPropertyService();
    }

    @Override
    public ParticipantRepository getParticipantRepository() {
        return new ParticipantRepositoryImpl(getConfiguredDataSource());
    }

    @Override
    public PurposeCodeRepository getPurposeCodeRepository() {
        return new PurposeCodeRepositoryImpl(getConfiguredDataSource());
    }

    @Override
    public RejectionMotiveRepository getRejectionMotiveRepository() {
        return new RejectionMotiveRepositoryImpl(getConfiguredDataSource());
    }

    @Override
    public CurrencyRepository getCurrencyRepository() {
        return new CurrencyRepositoryImpl(getConfiguredDataSource());
    }

    @Override
    public AccountValidator getAccountValidator() {
        return accountRequest -> new HashSet<>();
    }

    @Override
    public BankConfiguration getBankConfiguration() {
        return new DefaultBankConfiguration(getEnvironmentPropertyService(),
                getParticipantRepository(),
                getPurposeCodeRepository(),
                getRejectionMotiveRepository(),
                getCurrencyRepository());
    }

    @Override
    public MessageIdGenerator getMessageIdGenerator() {
        return () -> UUID.randomUUID().toString();
    }

    @Override
    public PaymentToReplyBodyMapper getPaymentToReplyBodyMapper() {
        return new DefaultPaymentToReplyBodyMapper(getMessageIdGenerator());
    }

    @Override
    public PaymentRepository getPaymentRepository() {
        return new PaymentRepositoryImpl(getConfiguredDataSource());
    }

    @Override
    public MessageDispatcher getMessageDispatcher() {
        return new DefaultMessageDispatcher(getEnvironmentPropertyService());
    }

    @Override
    public ReplyRepository getReplyRepository() {
        return new ReplyRepositoryImpl(getConfiguredDataSource());
    }

    @Override
    public ReplyService getReplyService() {
        return new DefaultReplyService(getPaymentToReplyBodyMapper(), getMessageDispatcher(), getReplyRepository());
    }

    @Override
    public PaymentRequestService getPaymentRequestService() {
        return new DefaultPaymentRequestService(getPaymentParser(), getPaymentValidator(),
                getReplyService(), getPaymentRepository());
    }

    @Override
    public AccountCreditorService getAccountCreditorService() {
        return payment -> System.out.println("Amount of "
                + payment.getInterBankSettlementAmount()
                + " has been credited to "
                + payment.getCreditorName());
    }

    @Override
    public NotificationService getNotificationService() {
        return payment -> System.out.println("Your account has been credited with an amount of "
                + payment.getInterBankSettlementAmount());
    }

    @Override
    public ResponseService getResponseService() {
        return new DefaultResponseService(getPaymentRepository(),
                getReplyRepository(),
                getAccountCreditorService(),
                getNotificationService(),
                getResponseErrorDispatcher(),
                getResponseParser());
    }

    @Override
    public XmlBodyValidator getXmlResponseValidator() {
        Path path = Paths.get("src", "main", "resources", "schema", "response_schema.xsd");
        return new DefaultXmlBodyValidator(path);
    }

    @Override
    public ErrorHandler getPaymentErrorDispatcher() {
        Path path = Paths.get(getEnvironmentPropertyService().getProperty("error.pacs.008.dir"));
        return new FileErrorHandler(path, "pacs.008");
    }

    @Override
    public ErrorHandler getResponseErrorDispatcher() {
        Path path = Paths.get(getEnvironmentPropertyService().getProperty("error.pacs.002.dir"));
        return new FileErrorHandler(path, "pacs.002");
    }

    @Override
    public ResponseParser getResponseParser() {
        return new DefaultResponseParser(getXmlResponseValidator());
    }


    private DataSource getConfiguredDataSource() {
        EnvironmentPropertyService propertyService = getEnvironmentPropertyService();
        JdbcDataSource dataSource = new JdbcDataSource();
        dataSource.setUrl(propertyService.getProperty("url"));
        dataSource.setUser(propertyService.getProperty("username"));
        dataSource.setPassword(propertyService.getProperty("password"));
        return dataSource;
    }

}
