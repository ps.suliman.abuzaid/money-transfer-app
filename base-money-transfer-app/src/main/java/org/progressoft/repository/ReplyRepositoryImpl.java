package org.progressoft.repository;

import org.progressoft.contract.ReplyRepository;
import org.progressoft.entity.Reply;
import org.progressoft.enums.Reason;
import org.progressoft.enums.Status;
import org.progressoft.exception.*;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;

import static java.util.Objects.isNull;


public class ReplyRepositoryImpl implements ReplyRepository {
    private final DataSource dataSource;

    public ReplyRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    @Override
    public void saveReply(Reply reply) {
        if (isNull(reply)) {
            throw new NullReplyException("reply is null");
        }
        try (Connection connection = dataSource.getConnection()) {
            String insertSql = "INSERT INTO reply (message_id, reason, status, payment_message_id, creation_date_time) " +
                    "VALUES (?, ?, ?, ?, ?)";
            try (PreparedStatement preparedStatement = connection.prepareStatement(insertSql)) {
                setStatementParams(reply, preparedStatement);
                boolean isUnSuccessful = preparedStatement.execute();
                if (isUnSuccessful) {
                    System.out.println("reply insertion failed.");
                    return;
                }
                System.out.println("reply inserted successfully.");
            }
        } catch (SQLException e) {
            throw new ReplyNotSavedException("Could not save reply", e);
        }

    }


    @Override
    public void updateReplyStatusAndReasonById(String messageId, Status status, Reason responseReason) {
        validateArguments(messageId, status, responseReason);
        String updateStatement = "UPDATE reply SET status = ?, response_reason = ? WHERE message_id = ?";
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(updateStatement)) {
                preparedStatement.setString(1, status.toString());
                preparedStatement.setString(2, responseReason.toString());
                preparedStatement.setString(3, messageId);
                int rows = preparedStatement.executeUpdate();
                if (rows <= 0) {
                    System.out.println("No reply updated");
                    return;
                }
                System.out.println("reply updated");
            }
        } catch (SQLException e) {
            throw new ReplyNotUpdatedException("Could not update reply", e);
        }
    }

    private void validateArguments(String messageId, Status status, Reason responseReason) {
        if (isNull(messageId))
            throw new NullMessageIdException("messageId is null");
        if (isNull(status))
            throw new NullStatusException("status is null");
        if (isNull(responseReason))
            throw new NullReasonException("reason is null");
    }

    private void setStatementParams(Reply reply, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, reply.getMessageId());
        Reason reason = reply.getReason();
        Status status = reply.getStatus();
        LocalDateTime dateTime = reply.getCreationDateTime();
        if (isNotNull(reason))
            preparedStatement.setString(2, reply.getReason().toString());
        if (isNotNull(status))
            preparedStatement.setString(3, reply.getStatus().toString());
        preparedStatement.setString(4, reply.getPaymentMessageId());
        if (isNotNull(dateTime))
            preparedStatement.setTimestamp(5, Timestamp.valueOf(reply.getCreationDateTime()));
    }

    private boolean isNotNull(Object object) {
        return !isNull(object);
    }
}
