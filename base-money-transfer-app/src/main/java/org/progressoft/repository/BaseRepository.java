package org.progressoft.repository;

import org.progressoft.exception.NullDataSourceException;

import javax.sql.DataSource;
import java.util.Objects;

public abstract class BaseRepository {

    public BaseRepository(DataSource dataSource) {
        if (Objects.isNull(dataSource))
            throw new NullDataSourceException("dataSource must not be null");
        init(dataSource);
    }

    protected abstract void init(DataSource dataSource);
}
