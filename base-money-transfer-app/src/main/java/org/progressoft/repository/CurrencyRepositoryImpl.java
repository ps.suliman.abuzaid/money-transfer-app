package org.progressoft.repository;

import org.progressoft.contract.CurrencyRepository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CurrencyRepositoryImpl extends BaseRepository implements CurrencyRepository {

    private List<String> currencies;

    public CurrencyRepositoryImpl(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected void init(DataSource dataSource) {
        this.currencies = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement currencyQuery = connection.prepareStatement("SELECT currency from currency")) {
                try (ResultSet currencyResult = currencyQuery.executeQuery()) {
                    while (currencyResult.next()) {
                        String currency = currencyResult.getString("currency");
                        currencies.add(currency);
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<String> getSupportedIsoCurrencies() {
        return currencies;
    }
}
