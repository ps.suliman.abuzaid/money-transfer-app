package org.progressoft.repository;

import org.progressoft.contract.PurposeCodeRepository;
import org.progressoft.entity.PurposeCode;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PurposeCodeRepositoryImpl extends BaseRepository implements PurposeCodeRepository {
    private List<PurposeCode> purposeCodes;

    public PurposeCodeRepositoryImpl(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected void init(DataSource dataSource) {
        this.purposeCodes = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement purposeCodeQuery = connection.prepareStatement("SELECT code,usage from purpose_code")) {
                try (ResultSet purposeCodesResult = purposeCodeQuery.executeQuery()) {
                    while (purposeCodesResult.next()) {
                        String code = purposeCodesResult.getString("code");
                        String usage = purposeCodesResult.getString("usage");
                        purposeCodes.add(new PurposeCode(code, usage));
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<PurposeCode> getPurposeCodes() {
        return purposeCodes;
    }
}
