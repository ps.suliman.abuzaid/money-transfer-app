package org.progressoft.repository;

import org.progressoft.contract.ParticipantRepository;
import org.progressoft.entity.Participant;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ParticipantRepositoryImpl extends BaseRepository implements ParticipantRepository {
    private List<Participant> participants;

    public ParticipantRepositoryImpl(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public List<Participant> getParticipants() {
        return participants;
    }

    @Override
    protected void init(DataSource dataSource) {
        this.participants = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement participantQuery = connection.prepareStatement("SELECT name,bic from participant")) {
                try (ResultSet participantResult = participantQuery.executeQuery()) {
                    while (participantResult.next()) {
                        String name = participantResult.getString("name");
                        String bic = participantResult.getString("bic");
                        participants.add(new Participant(name, bic));
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
