package org.progressoft.repository;

import org.progressoft.contract.RejectionMotiveRepository;
import org.progressoft.entity.RejectionMotive;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RejectionMotiveRepositoryImpl extends BaseRepository implements RejectionMotiveRepository {

    private List<RejectionMotive> rejectionMotives;

    public RejectionMotiveRepositoryImpl(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected void init(DataSource dataSource) {
        this.rejectionMotives = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement rejectionMotiveQuery = connection.prepareStatement("SELECT motive_code,motive_description from rejection_motive")) {
                try (ResultSet rejectionMotiveResult = rejectionMotiveQuery.executeQuery()) {
                    while (rejectionMotiveResult.next()) {
                        String motiveCode = rejectionMotiveResult.getString("motive_code");
                        String motiveDescription = rejectionMotiveResult.getString("motive_description");
                        rejectionMotives.add(new RejectionMotive(motiveCode, motiveDescription));
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public List<RejectionMotive> getRejectionMotives() {
        return rejectionMotives;
    }
}
