package org.progressoft.repository;

import org.progressoft.contract.PaymentRepository;
import org.progressoft.entity.Payment;
import org.progressoft.entity.PaymentCompositeKey;
import org.progressoft.entity.PaymentFilterRequest;
import org.progressoft.enums.Status;
import org.progressoft.exception.*;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Objects.isNull;

public class PaymentRepositoryImpl implements PaymentRepository {

    private final DataSource dataSource;
    private final String UPDATE_PAYMENT_WHERE_CONDITION = " WHERE message_id = ? AND instruction_id = ? " +
            "AND end_to_end_id = ? " +
            "AND transaction_id = ? " +
            "AND interbank_settlement_date = ?;";

    private final String PAYMENT_FILTER_REQUEST_CONDITION = " WHERE " +
            "(? IS NULL OR message_id like CONCAT('%',?,'%')) AND " +
            "(? IS NULL OR interbank_settlement_amount >= ?) AND " +
            "(? IS NULL OR interbank_settlement_amount <= ?) AND " +
            "(? IS NULL OR interbank_settlement_date >= ?) AND " +
            "(? IS NULL OR interbank_settlement_date <= ?) AND " +
            "(? IS NULL OR interbank_settlement_currency LIKE CONCAT('%',?,'%')) AND " +
            "(? IS NULL OR debtor_account_iban like CONCAT('%',?,'%')) AND " +
            "(? IS NULL OR debtor_agent_bicfi like CONCAT('%',?,'%')) AND " +
            "(? IS NULL OR creditor_account_iban like CONCAT('%',?,'%')) AND " +
            "(? IS NULL OR auth_status like CONCAT('%',?,'%')) ";

    public PaymentRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void savePayment(Payment payment) {
        if (isNull(payment))
            throw new NullPaymentException("Can not save a null payment");
        try (Connection connection = dataSource.getConnection()) {
            executeInsert(payment, connection);
        } catch (SQLException e) {
            throw new PaymentNotSavedException("Could not save payment", e);
        }
    }

    private void executeInsert(Payment payment, Connection connection) throws SQLException {
        String insertSql = "INSERT INTO payment " +
                "(message_id, creation_date_time, number_of_transactions, settlement_method, " +
                "instruction_id, end_to_end_id, transaction_id, clearing_channel, service_level, " +
                "local_instrument, category_purpose, interbank_settlement_currency, " +
                "interbank_settlement_amount, interbank_settlement_date, charge_bearer, " +
                "instructing_agent_bicfi, instructed_agent_bicfi, debtor_name, debtor_address_lines, " +
                "debtor_account_iban, debtor_agent_bicfi, creditor_name, creditor_address_lines, " +
                "creditor_account_iban, creditor_agent_bicfi, status, auth_status, rejection_codes, xml_payment)" +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try (PreparedStatement preparedStatement = connection.prepareStatement(insertSql)) {
            setStatementParams(payment, preparedStatement);
            boolean isUnSuccessful = preparedStatement.execute();
            if (isUnSuccessful) {
                System.out.println("payment insertion failed");
                return;
            }
            System.out.println("payment inserted successfully");
        }
    }

    @Override
    public void updatePaymentStatusByCompositeKey(Status status, PaymentCompositeKey key) {
        if (isNull(status))
            throw new NullStatusException("status is null");
        validatePaymentCompositeKey(key);
        String updateStatement = "UPDATE payment SET status = ?" + UPDATE_PAYMENT_WHERE_CONDITION;
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(updateStatement)) {
                preparedStatement.setString(1, status.toString());
                addCompositeIdToStatementParameters(key, preparedStatement, 2);
                int rows = preparedStatement.executeUpdate();
                if (rows > 0)
                    System.out.println("Payment Updated");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void validatePaymentCompositeKey(PaymentCompositeKey paymentCompositeKey) {
        if (isNull(paymentCompositeKey))
            throw new NullPaymentCompositeKeyException("paymentCompositeKey is null");
        throwInvalidKeyExceptionIfNull(paymentCompositeKey.getMessageId(), "messageId is null");
        throwInvalidKeyExceptionIfNull(paymentCompositeKey.getInstructionId(), "instructionId is null");
        throwInvalidKeyExceptionIfNull(paymentCompositeKey.getEndToEndId(), "endToEndId is null");
        throwInvalidKeyExceptionIfNull(paymentCompositeKey.getTransactionId(), "transactionId is null");
        throwInvalidKeyExceptionIfNull(paymentCompositeKey.getInterBankSettlementDate(),
                "settlementDate is null");
    }

    private void throwInvalidKeyExceptionIfNull(Object object, String message) {
        if (isNull(object))
            throw new InvalidPaymentCompositeKeyException(message);
    }

    @Override
    public Payment getPaymentByCompositeId(PaymentCompositeKey paymentCompositeKey) {
        Payment payment = new Payment();
        try (Connection connection = dataSource.getConnection()) {
            String selectSql = "SELECT * FROM payment WHERE message_id = ? " +
                    "AND instruction_id = ? " +
                    "AND end_to_end_id = ? " +
                    "AND transaction_id = ? " +
                    "AND interbank_settlement_date = ? " +
                    "LIMIT 1";
            try (PreparedStatement preparedStatement = connection.prepareStatement(selectSql)) {
                addCompositeIdToStatementParameters(paymentCompositeKey, preparedStatement, 1);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (!resultSet.next()) {
                        System.out.println("No payment records found.");
                        return null;
                    }
                    fillPaymentData(payment, resultSet);
                    payment.setXmlPayment(resultSet.getString("xml_payment"));
                    payment.setXmlReply(resultSet.getString("xml_reply"));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not fetch payment", e);
        }
        return payment;
    }

    @Override
    public List<Payment> getPaymentsByFilterRequest(PaymentFilterRequest request) {
        List<Payment> payments = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            String selectSql = "SELECT * FROM payment" +
                    PAYMENT_FILTER_REQUEST_CONDITION +
                    "LIMIT ? OFFSET ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(selectSql)) {
                setPaymentFilterRequestParams(request, preparedStatement);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        Payment payment = new Payment();
                        fillPaymentData(payment, resultSet);
                        payments.add(payment);
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not fetch payment", e);
        }
        return payments;
    }

    @Override
    public int getTotalPaymentsNumberByFilter(PaymentFilterRequest request) {
        int totalNumber = 0;
        try (Connection connection = dataSource.getConnection()) {
            String selectSql = "SELECT COUNT(*) FROM payment" + PAYMENT_FILTER_REQUEST_CONDITION;
            try (PreparedStatement preparedStatement = connection.prepareStatement(selectSql)) {
                setPaymentFilterRequestMainParams(request, preparedStatement);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        totalNumber = resultSet.getInt(1);
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not count payments", e);
        }
        return totalNumber;
    }

    @Override
    public void updatePaymentXmlReplyByCompositeKey(String xmlReply, PaymentCompositeKey key) {
        if (isNull(xmlReply))
            throw new NullXmlReplyException("xmlReply is null");
        validatePaymentCompositeKey(key);
        String updateStatement = "UPDATE payment SET xml_reply = ?" + UPDATE_PAYMENT_WHERE_CONDITION;
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(updateStatement)) {
                preparedStatement.setString(1, xmlReply);
                addCompositeIdToStatementParameters(key, preparedStatement, 2);
                int rows = preparedStatement.executeUpdate();
                if (rows > 0)
                    System.out.println("Payment Updated");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public BigDecimal getTotalSumByPaymentFilter(PaymentFilterRequest filterRequest) {
        BigDecimal sum = BigDecimal.ZERO;
        try (Connection connection = dataSource.getConnection()) {
            String selectSql = "SELECT SUM(interbank_settlement_amount) FROM payment" +
                    PAYMENT_FILTER_REQUEST_CONDITION;
            try (PreparedStatement preparedStatement = connection.prepareStatement(selectSql)) {
                setPaymentFilterRequestMainParams(filterRequest, preparedStatement);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        sum = resultSet.getBigDecimal(1);
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not count payments", e);
        }
        return sum;
    }

    private void setPaymentFilterRequestParams(PaymentFilterRequest request, PreparedStatement preparedStatement) throws SQLException {
        int offset = (request.getPageNumber() - 1) * request.getPageCount();
        setPaymentFilterRequestMainParams(request, preparedStatement);
        preparedStatement.setInt(21, request.getPageCount());
        preparedStatement.setInt(22, offset);
    }

    private void setPaymentFilterRequestMainParams(PaymentFilterRequest request, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, request.getMessageId());
        preparedStatement.setString(2, request.getMessageId());
        preparedStatement.setBigDecimal(3, request.getSettlementAmountFrom());
        preparedStatement.setBigDecimal(4, request.getSettlementAmountFrom());
        preparedStatement.setBigDecimal(5, request.getSettlementAmountTo());
        preparedStatement.setBigDecimal(6, request.getSettlementAmountTo());
        Date fromSettlementDate = getSettlementDate(request.getSettlementDateFrom());
        Date toSettlementDate = getSettlementDate(request.getSettlementDateTo());
        preparedStatement.setDate(7, fromSettlementDate);
        preparedStatement.setDate(8, fromSettlementDate);
        preparedStatement.setDate(9, toSettlementDate);
        preparedStatement.setDate(10, toSettlementDate);
        preparedStatement.setString(11, request.getCurrency());
        preparedStatement.setString(12, request.getCurrency());
        preparedStatement.setString(13, request.getDebtorAccountIban());
        preparedStatement.setString(14, request.getDebtorAccountIban());
        preparedStatement.setString(15, request.getDebtorAgentBicFi());
        preparedStatement.setString(16, request.getDebtorAgentBicFi());
        preparedStatement.setString(17, request.getCreditorAccountIban());
        preparedStatement.setString(18, request.getCreditorAccountIban());
        preparedStatement.setString(19, request.getAuthStatus());
        preparedStatement.setString(20, request.getAuthStatus());
    }

    private Date getSettlementDate(LocalDate date) {
        return isNull(date) ? null : Date.valueOf(date);
    }

    private void addCompositeIdToStatementParameters(PaymentCompositeKey paymentCompositeKey,
                                                     PreparedStatement preparedStatement,
                                                     int parameterIndex) throws SQLException {
        preparedStatement.setString(parameterIndex++, paymentCompositeKey.getMessageId());
        preparedStatement.setString(parameterIndex++, paymentCompositeKey.getInstructionId());
        preparedStatement.setString(parameterIndex++, paymentCompositeKey.getEndToEndId());
        preparedStatement.setString(parameterIndex++, paymentCompositeKey.getTransactionId());
        preparedStatement.setDate(parameterIndex, Date.valueOf(paymentCompositeKey.getInterBankSettlementDate()));
    }

    private void fillPaymentData(Payment payment, ResultSet resultSet) throws SQLException {
        payment.setMessageId(resultSet.getString("message_id"));
        payment.setCreationDateTime(resultSet.getTimestamp("creation_date_time").toLocalDateTime());
        payment.setNumberOfTransactions(resultSet.getInt("number_of_transactions"));
        payment.setSettlementMethod(resultSet.getString("settlement_method"));
        payment.setInstructionId(resultSet.getString("instruction_id"));
        payment.setEndToEndId(resultSet.getString("end_to_end_id"));
        payment.setTransactionId(resultSet.getString("transaction_id"));
        payment.setClearingChannel(resultSet.getString("clearing_channel"));
        payment.setServiceLevel(resultSet.getString("service_level"));
        payment.setLocalInstrument(resultSet.getString("local_instrument"));
        payment.setCategoryPurpose(resultSet.getString("category_purpose"));
        payment.setInterBankSettlementCurrency(resultSet.getString("interbank_settlement_currency"));
        payment.setInterBankSettlementAmount(resultSet.getBigDecimal("interbank_settlement_amount"));
        payment.setInterBankSettlementDate(resultSet.getDate("interbank_settlement_date").toLocalDate());
        payment.setChargeBearer(resultSet.getString("charge_bearer"));
        payment.setInstructingAgentBicFi(resultSet.getString("instructing_agent_bicfi"));
        payment.setInstructedAgentBicFi(resultSet.getString("instructed_agent_bicfi"));
        payment.setDebtorName(resultSet.getString("debtor_name"));
        String[] debtorAddressLines = resultSet.getString("debtor_address_lines").split(",");
        payment.setDebtorAddressLines(Arrays.asList(debtorAddressLines));
        payment.setDebtorAccountIban(resultSet.getString("debtor_account_iban"));
        payment.setDebtorAgentBicFi(resultSet.getString("debtor_agent_bicfi"));
        payment.setCreditorName(resultSet.getString("creditor_name"));
        String[] creditorAddressLines = resultSet.getString("creditor_address_lines").split(",");
        payment.setCreditorAddressLines(Arrays.asList(creditorAddressLines));
        payment.setCreditorAccountIban(resultSet.getString("creditor_account_iban"));
        payment.setCreditorAgentBicFi(resultSet.getString("creditor_agent_bicfi"));
        payment.setStatus(resultSet.getString("status"));
        payment.setAuthStatus(resultSet.getString("auth_status"));
        String[] rejectionCodes = resultSet.getString("rejection_codes").split(",");
        payment.setRejectionCodes(Arrays.asList(rejectionCodes));
    }

    private void setStatementParams(Payment payment, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, payment.getMessageId());
        preparedStatement.setTimestamp(2, Timestamp.valueOf(payment.getCreationDateTime()));
        preparedStatement.setInt(3, payment.getNumberOfTransactions());
        preparedStatement.setString(4, payment.getSettlementMethod());
        preparedStatement.setString(5, payment.getInstructionId());
        preparedStatement.setString(6, payment.getEndToEndId());
        preparedStatement.setString(7, payment.getTransactionId());
        preparedStatement.setString(8, payment.getClearingChannel());
        preparedStatement.setString(9, payment.getServiceLevel());
        preparedStatement.setString(10, payment.getLocalInstrument());
        preparedStatement.setString(11, payment.getCategoryPurpose());
        preparedStatement.setString(12, payment.getInterBankSettlementCurrency());
        preparedStatement.setBigDecimal(13, payment.getInterBankSettlementAmount());
        preparedStatement.setDate(14, Date.valueOf(payment.getInterBankSettlementDate()));
        preparedStatement.setString(15, payment.getChargeBearer());
        preparedStatement.setString(16, payment.getInstructingAgentBicFi());
        preparedStatement.setString(17, payment.getInstructedAgentBicFi());
        preparedStatement.setString(18, payment.getDebtorName());
        preparedStatement.setString(19, String.join(",", payment.getDebtorAddressLines()));
        preparedStatement.setString(20, payment.getDebtorAccountIban());
        preparedStatement.setString(21, payment.getDebtorAgentBicFi());
        preparedStatement.setString(22, payment.getCreditorName());
        preparedStatement.setString(23, String.join(",", payment.getCreditorAddressLines()));
        preparedStatement.setString(24, payment.getCreditorAccountIban());
        preparedStatement.setString(25, payment.getCreditorAgentBicFi());
        preparedStatement.setString(26, payment.getStatus());
        preparedStatement.setString(27, payment.getAuthStatus());
        preparedStatement.setString(28, String.join(",", payment.getRejectionCodes()));
        preparedStatement.setString(29, payment.getXmlPayment());
    }
}
