package org.progressoft.service;

import org.progressoft.contract.*;
import org.progressoft.entity.Payment;
import org.progressoft.entity.PaymentCompositeKey;
import org.progressoft.entity.ResponseData;
import org.progressoft.enums.Reason;
import org.progressoft.enums.Status;
import org.progressoft.exception.ResponseNotValidException;
import org.progressoft.exception.XmlBodyValidationException;

import java.time.LocalDate;

public class DefaultResponseService implements ResponseService {

    private final PaymentRepository paymentRepository;
    private final ReplyRepository replyRepository;
    private final AccountCreditorService accountCreditorService;
    private final NotificationService notificationService;
    private final ResponseParser responseParser;
    private final ErrorHandler errorHandler;


    public DefaultResponseService(PaymentRepository paymentRepository,
                                  ReplyRepository replyRepository,
                                  AccountCreditorService accountCreditorService,
                                  NotificationService notificationService,
                                  ErrorHandler errorHandler,
                                  ResponseParser responseParser) {
        this.paymentRepository = paymentRepository;
        this.replyRepository = replyRepository;
        this.accountCreditorService = accountCreditorService;
        this.notificationService = notificationService;
        this.errorHandler = errorHandler;
        this.responseParser = responseParser;
    }

    @Override
    public void processResponse(String xmlResponse) {
        try {
            ResponseData responseData = responseParser.parseResponse(xmlResponse);
            PaymentCompositeKey paymentCompositeKey = getPaymentCompositeKey(responseData);
            updatePaymentAndReplyStatus(paymentCompositeKey, responseData);
            creditAndNotify(responseData);
        } catch (XmlBodyValidationException | ResponseNotValidException e) {
            System.out.println("Invalid pacs.002 file, check error_in_pacs.002 directory for more details");
            errorHandler.dispatchError(xmlResponse, e.getMessage());
        }
    }

    private void updatePaymentAndReplyStatus(PaymentCompositeKey paymentCompositeKey, ResponseData responseData) {
        Reason responseReason = responseData.getReason();
        paymentRepository.updatePaymentStatusByCompositeKey(getPaymentStatus(responseReason),
                paymentCompositeKey);
        replyRepository.updateReplyStatusAndReasonById(responseData.getMessageId(), Status.PROCESSED, responseReason);
    }

    private Status getPaymentStatus(Reason responseReason) {
        return responseReason.equals(Reason.RJCT) ?
                Status.REJECTED : Status.PROCESSED;
    }

    private void creditAndNotify(ResponseData responseData) {
        if (!responseData.getReason().equals(Reason.ACSP)) return;
        PaymentCompositeKey key = getPaymentCompositeKey(responseData);
        Payment payment = paymentRepository.getPaymentByCompositeId(key);
        accountCreditorService.creditAccount(payment);
        notificationService.notifyAccount(payment);
    }

    private PaymentCompositeKey getPaymentCompositeKey(ResponseData responseData) {
        String messageId = responseData.getPaymentMessageId();
        String instructionId = responseData.getInstructionId();
        String endToEndId = responseData.getEndToEndId();
        String transactionId = responseData.getTransactionId();
        LocalDate settlementDate = responseData.getSettlementDate();
        return new PaymentCompositeKey(messageId, instructionId, endToEndId,
                transactionId, settlementDate);
    }
}
