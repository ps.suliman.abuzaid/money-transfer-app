package org.progressoft.service;

import org.progressoft.contract.EnvironmentPropertyService;
import org.progressoft.exception.NullPropertyKeyException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

public class DefaultEnvironmentPropertyService implements EnvironmentPropertyService {
    private final Properties properties;

    public DefaultEnvironmentPropertyService() {
        this.properties = new Properties();
        try (InputStream input = getClass().getResourceAsStream("/application.properties")) {
            this.properties.load(input);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getProperty(String key) {
        if (Objects.isNull(key))
            throw new NullPropertyKeyException("property key is null");
        return properties.getProperty(key);
    }
}
