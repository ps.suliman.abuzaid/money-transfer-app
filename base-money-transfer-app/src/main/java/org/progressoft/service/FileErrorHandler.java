package org.progressoft.service;

import org.progressoft.contract.ErrorHandler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;

public class FileErrorHandler implements ErrorHandler {
    private final Path filePath;
    private final String filePrefix;

    public FileErrorHandler(Path filePath, String filePrefix) {
        this.filePath = filePath;
        this.filePrefix = filePrefix;
    }

    @Override
    public void dispatchError(String xmlBody, String errorMessage) {
        LocalDateTime localDateTime = LocalDateTime.now();
        try {
            Files.write(Paths.get(String.format("%s/%s_%s.xml", filePath, filePrefix, localDateTime)),
                    xmlBody.getBytes());
            Files.write(Paths.get(String.format("%s/%s_error_%s.txt", filePath, filePrefix, localDateTime)),
                    errorMessage.getBytes());
        } catch (IOException ex) {
            throw new RuntimeException("Could not write error to path", ex);
        }
    }
}
