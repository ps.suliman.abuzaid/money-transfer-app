package org.progressoft.service;

import org.progressoft.contract.EnvironmentPropertyService;
import org.progressoft.contract.MessageDispatcher;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;

public class DefaultMessageDispatcher implements MessageDispatcher {

    private final EnvironmentPropertyService environmentPropertyService;

    public DefaultMessageDispatcher(EnvironmentPropertyService environmentPropertyService) {
        this.environmentPropertyService = environmentPropertyService;
    }

    @Override
    public void dispatchMessage(String message) {
        Path outPacs2Path = Path.of(environmentPropertyService.getProperty("out.pacs.002.dir"));
        try {
            Path newFilePath = Files.createFile(Paths.get(outPacs2Path + "/pacs.002." + LocalDateTime.now() + ".xml"));
            Files.write(newFilePath, message.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
