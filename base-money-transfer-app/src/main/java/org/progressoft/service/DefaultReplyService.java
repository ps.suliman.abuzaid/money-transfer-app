package org.progressoft.service;

import org.progressoft.contract.MessageDispatcher;
import org.progressoft.contract.PaymentToReplyBodyMapper;
import org.progressoft.contract.ReplyRepository;
import org.progressoft.contract.ReplyService;
import org.progressoft.entity.Payment;
import org.progressoft.entity.RejectionMotive;
import org.progressoft.entity.Reply;
import org.progressoft.entity.ReplyBody;
import org.progressoft.exception.NullReplyBodyException;
import org.progressoft.exception.NullReplyException;
import org.progressoft.exception.NullXmlReplyException;

import java.util.Objects;
import java.util.Set;

public class DefaultReplyService implements ReplyService {

    private final PaymentToReplyBodyMapper paymentToReplyBodyMapper;
    private final MessageDispatcher dispatcher;
    private final ReplyRepository replyRepository;

    public DefaultReplyService(PaymentToReplyBodyMapper paymentToReplyBodyMapper,
                               MessageDispatcher dispatcher,
                               ReplyRepository replyRepository) {
        this.paymentToReplyBodyMapper = paymentToReplyBodyMapper;
        this.dispatcher = dispatcher;
        this.replyRepository = replyRepository;
    }

    @Override
    public String sendReply(Payment payment, Set<RejectionMotive> rejectionMotives) {
        ReplyBody replyBody = paymentToReplyBodyMapper.convertPaymentToReplyBody(payment, rejectionMotives);
        validateReplyBody(replyBody);
        String xmlReply = replyBody.getXmlReply();
        Reply reply = replyBody.getReply();
        dispatcher.dispatchMessage(xmlReply);
        replyRepository.saveReply(reply);
        return xmlReply;
    }

    private void validateReplyBody(ReplyBody replyBody) {
        if (Objects.isNull(replyBody))
            throw new NullReplyBodyException("replyBody is null");
        if (Objects.isNull(replyBody.getXmlReply()))
            throw new NullXmlReplyException("xmlReply is null");
        if (Objects.isNull(replyBody.getReply()))
            throw new NullReplyException("reply is null");
    }
}
