package org.progressoft.service;

import org.progressoft.contract.*;
import org.progressoft.entity.Participant;
import org.progressoft.entity.PurposeCode;
import org.progressoft.entity.RejectionMotive;

import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DefaultBankConfiguration implements BankConfiguration {

    private final EnvironmentPropertyService propertyService;
    private final ParticipantRepository participantRepository;
    private final PurposeCodeRepository purposeCodeRepository;
    private final RejectionMotiveRepository rejectionMotiveRepository;
    private final CurrencyRepository currencyRepository;
    private Map<String, Currency> currencyCache;
    private Map<String, Participant> participantCache;
    private Map<String, PurposeCode> purposeCodeCache;
    private Map<String, RejectionMotive> rejectionMotiveCache;


    private void initParticipantCache() {
        participantCache = new HashMap<>();
        getParticipants().forEach(participant ->
                participantCache.put(participant.getBic(), participant));
    }

    public DefaultBankConfiguration(EnvironmentPropertyService propertyService,
                                    ParticipantRepository participantRepository,
                                    PurposeCodeRepository purposeCodeRepository,
                                    RejectionMotiveRepository rejectionMotiveRepository,
                                    CurrencyRepository currencyRepository) {
        this.propertyService = propertyService;
        this.participantRepository = participantRepository;
        this.purposeCodeRepository = purposeCodeRepository;
        this.rejectionMotiveRepository = rejectionMotiveRepository;
        this.currencyRepository = currencyRepository;
        initParticipantCache();
        initPurposeCodeCache();
        intiRejectionMotiveCache();
        initCurrencyCache();
    }

    private void initCurrencyCache() {
        currencyCache = new HashMap<>();
        getSupportedCurrencies().forEach(
                isoCurrency -> currencyCache.put(isoCurrency, Currency.getInstance(isoCurrency))
        );
    }

    private void intiRejectionMotiveCache() {
        rejectionMotiveCache = new HashMap<>();
        getRejectionMotives().forEach(
                rejectionMotive -> rejectionMotiveCache
                        .put(rejectionMotive.getMotiveCode(), rejectionMotive));
    }

    private void initPurposeCodeCache() {
        purposeCodeCache = new HashMap<>();
        getPurposeCodes().forEach(purposeCode -> purposeCodeCache.put(purposeCode.getCode(), purposeCode));
    }

    @Override
    public List<Participant> getParticipants() {
        return participantRepository.getParticipants();
    }

    @Override
    public Participant getParticipantByBic(String bic) {
        return participantCache.get(bic);
    }

    @Override
    public List<PurposeCode> getPurposeCodes() {
        return purposeCodeRepository.getPurposeCodes();
    }

    @Override
    public PurposeCode getPurposeCodeByCode(String code) {
        return purposeCodeCache.get(code);
    }

    @Override
    public List<RejectionMotive> getRejectionMotives() {
        return rejectionMotiveRepository.getRejectionMotives();
    }

    @Override
    public RejectionMotive getRejectionMotiveByCode(String code) {
        return rejectionMotiveCache.get(code);
    }

    @Override
    public List<String> getSupportedCurrencies() {
        return currencyRepository.getSupportedIsoCurrencies();
    }

    @Override
    public Currency getCurrencyByIsoCurrency(String isoCurrency) {
        return currencyCache.get(isoCurrency);
    }

    @Override
    public String getDefaultRejectionCode() {
        return propertyService.getProperty("bank.default.rejection.code");
    }

    @Override
    public String getCurrentParticipantBicFi() {
        return propertyService.getProperty("bank.default.bic");
    }
}
