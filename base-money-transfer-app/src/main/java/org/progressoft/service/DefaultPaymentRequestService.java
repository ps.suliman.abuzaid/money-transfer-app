package org.progressoft.service;

import org.progressoft.contract.*;
import org.progressoft.entity.Payment;
import org.progressoft.entity.PaymentCompositeKey;
import org.progressoft.entity.RejectionMotive;
import org.progressoft.enums.Reason;
import org.progressoft.enums.Status;

import java.util.Set;
import java.util.stream.Collectors;

public class DefaultPaymentRequestService implements PaymentRequestService {

    private final PaymentParser paymentParser;
    private final PaymentValidator paymentValidator;
    private final ReplyService replyService;
    private final PaymentRepository paymentRepository;

    public DefaultPaymentRequestService(PaymentParser paymentParser,
                                        PaymentValidator paymentValidator,
                                        ReplyService replyService,
                                        PaymentRepository paymentRepository) {
        this.paymentParser = paymentParser;
        this.paymentValidator = paymentValidator;
        this.replyService = replyService;
        this.paymentRepository = paymentRepository;
    }

    @Override
    public String processRequest(String xmlPayment) {
        Payment payment = paymentParser.parse(xmlPayment);
        Set<RejectionMotive> rejectionMotives = paymentValidator.validatePayment(payment);
        payment.setStatus(Status.PENDING_RESPONSE.toString());
        payment.setXmlPayment(xmlPayment);
        setAuthStatus(rejectionMotives, payment);
        setRejectionCodes(payment, rejectionMotives);
        paymentRepository.savePayment(payment);
        String xmlReply = replyService.sendReply(payment, rejectionMotives);
        paymentRepository.updatePaymentXmlReplyByCompositeKey(xmlReply, getPaymentCompositeKey(payment));
        return xmlReply;
    }

    private static PaymentCompositeKey getPaymentCompositeKey(Payment payment) {
        return new PaymentCompositeKey(payment.getMessageId(), payment.getInstructionId(), payment.getEndToEndId(),
                payment.getTransactionId(), payment.getInterBankSettlementDate());
    }

    private static void setRejectionCodes(Payment payment, Set<RejectionMotive> rejectionMotives) {
        payment.setRejectionCodes(rejectionMotives.stream().map(RejectionMotive::getMotiveCode).collect(Collectors.toList()));
    }

    private static void setAuthStatus(Set<RejectionMotive> rejectionMotives, Payment payment) {
        String authStatus = rejectionMotives.isEmpty() ? Reason.AUTH.toString() : Reason.NAUT.toString();
        payment.setAuthStatus(authStatus);
    }


}
