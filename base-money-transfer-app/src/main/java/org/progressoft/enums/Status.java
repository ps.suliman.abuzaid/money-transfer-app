package org.progressoft.enums;

public enum Status {
    PENDING_RESPONSE,
    REJECTED,
    PROCESSED;
}
