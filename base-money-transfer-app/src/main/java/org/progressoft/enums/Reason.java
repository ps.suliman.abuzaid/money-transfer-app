package org.progressoft.enums;

public enum Reason {
    AUTH,
    NAUT,
    ACSP,
    RJCT;
}
