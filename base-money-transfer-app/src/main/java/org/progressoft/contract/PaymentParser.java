package org.progressoft.contract;

import org.progressoft.entity.Payment;

public interface PaymentParser {
    Payment parse(String xmlString);
}
