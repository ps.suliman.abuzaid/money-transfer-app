package org.progressoft.contract;

import org.progressoft.entity.Participant;
import org.progressoft.entity.PurposeCode;
import org.progressoft.entity.RejectionMotive;

import java.util.Currency;
import java.util.List;

public interface BankConfiguration {

    List<Participant> getParticipants();

    Participant getParticipantByBic(String bic);

    List<PurposeCode> getPurposeCodes();

    PurposeCode getPurposeCodeByCode(String code);

    List<RejectionMotive> getRejectionMotives();

    RejectionMotive getRejectionMotiveByCode(String code);

    List<String> getSupportedCurrencies();

    Currency getCurrencyByIsoCurrency(String isoCurrency);

    String getDefaultRejectionCode();

    String getCurrentParticipantBicFi();
}
