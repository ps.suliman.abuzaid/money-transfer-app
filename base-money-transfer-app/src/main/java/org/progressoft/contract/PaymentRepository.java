package org.progressoft.contract;

import org.progressoft.entity.Payment;
import org.progressoft.entity.PaymentCompositeKey;
import org.progressoft.entity.PaymentFilterRequest;
import org.progressoft.enums.Status;

import java.math.BigDecimal;
import java.util.List;

public interface PaymentRepository {
    void savePayment(Payment payment);

    void updatePaymentStatusByCompositeKey(Status status, PaymentCompositeKey key);

    Payment getPaymentByCompositeId(PaymentCompositeKey paymentCompositeKey);

    List<Payment> getPaymentsByFilterRequest(PaymentFilterRequest request);

    int getTotalPaymentsNumberByFilter(PaymentFilterRequest request);

    void updatePaymentXmlReplyByCompositeKey(String xmlReply, PaymentCompositeKey key);

    BigDecimal getTotalSumByPaymentFilter(PaymentFilterRequest filterRequest);
}
