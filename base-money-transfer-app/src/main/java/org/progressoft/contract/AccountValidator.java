package org.progressoft.contract;

import org.progressoft.entity.AccountRequest;
import org.progressoft.entity.RejectionMotive;

import java.util.Set;

public interface AccountValidator {

    Set<RejectionMotive> validateAccount(AccountRequest accountRequest);
}
