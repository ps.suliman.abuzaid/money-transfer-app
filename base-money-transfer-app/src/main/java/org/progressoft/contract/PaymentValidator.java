package org.progressoft.contract;

import org.progressoft.entity.Payment;
import org.progressoft.entity.RejectionMotive;

import java.util.Set;

public interface PaymentValidator {

    Set<RejectionMotive> validatePayment(Payment payment);
}
