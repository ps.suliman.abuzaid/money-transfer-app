package org.progressoft.contract;

import org.progressoft.entity.Payment;
import org.progressoft.entity.RejectionMotive;
import org.progressoft.entity.ReplyBody;

import java.util.Set;

public interface PaymentToReplyBodyMapper {

    ReplyBody convertPaymentToReplyBody(Payment payment, Set<RejectionMotive> rejectionMotives);
}
