package org.progressoft.contract;

public interface EnvironmentPropertyService {
    String getProperty(String key);
}
