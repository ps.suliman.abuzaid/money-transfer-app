package org.progressoft.contract;

import org.progressoft.entity.Payment;

public interface NotificationService {

    void notifyAccount(Payment payment);
}
