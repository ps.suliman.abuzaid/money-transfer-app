package org.progressoft.contract;

import org.progressoft.entity.RejectionMotive;

import java.util.List;

public interface RejectionMotiveRepository {

    List<RejectionMotive> getRejectionMotives();
}
