package org.progressoft.contract;

public interface ErrorHandler {

    void dispatchError(String xmlBody, String errorMessage);

}
