package org.progressoft.contract;

import org.progressoft.entity.ResponseData;

public interface ResponseParser {
    ResponseData parseResponse(String xmlResponse);
}
