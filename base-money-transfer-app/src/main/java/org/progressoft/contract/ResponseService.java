package org.progressoft.contract;

public interface ResponseService {
    void processResponse(String xmlResponse);
}
