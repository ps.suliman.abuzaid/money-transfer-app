package org.progressoft.contract;

import org.progressoft.entity.PurposeCode;

import java.util.List;

public interface PurposeCodeRepository {
    List<PurposeCode> getPurposeCodes();
}
