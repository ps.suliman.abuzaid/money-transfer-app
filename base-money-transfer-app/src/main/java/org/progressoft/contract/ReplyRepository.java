package org.progressoft.contract;

import org.progressoft.entity.Reply;
import org.progressoft.enums.Reason;
import org.progressoft.enums.Status;

public interface ReplyRepository {
    void saveReply(Reply reply);

    void updateReplyStatusAndReasonById(String messageId, Status status, Reason responseReason);
}
