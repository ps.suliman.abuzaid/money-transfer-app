package org.progressoft.contract;

public interface XmlBodyValidator {

    void validateXmlBody(String xmlBody);
}
