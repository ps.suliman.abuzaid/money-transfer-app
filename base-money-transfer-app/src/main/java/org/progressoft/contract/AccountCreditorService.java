package org.progressoft.contract;

import org.progressoft.entity.Payment;

public interface AccountCreditorService {
    void creditAccount(Payment payment);
}
