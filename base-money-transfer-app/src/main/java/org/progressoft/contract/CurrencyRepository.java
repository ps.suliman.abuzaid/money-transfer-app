package org.progressoft.contract;

import java.util.List;

public interface CurrencyRepository {
    List<String> getSupportedIsoCurrencies();
}
