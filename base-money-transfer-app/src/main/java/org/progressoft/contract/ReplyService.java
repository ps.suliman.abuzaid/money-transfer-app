package org.progressoft.contract;

import org.progressoft.entity.Payment;
import org.progressoft.entity.RejectionMotive;

import java.util.Set;

public interface ReplyService {
    String sendReply(Payment payment, Set<RejectionMotive> rejectionMotives);
}
