package org.progressoft.contract;

import org.progressoft.entity.Participant;

import java.util.List;

public interface ParticipantRepository {

    List<Participant> getParticipants();
}
