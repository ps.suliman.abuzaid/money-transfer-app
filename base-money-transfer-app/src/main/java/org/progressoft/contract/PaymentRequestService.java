package org.progressoft.contract;

public interface PaymentRequestService {
    String processRequest(String xmlPayment);
}
