package org.progressoft.contract;

public interface XmlResponseValidator {

    boolean validateXmlResponse(String xmlResponse);
}
