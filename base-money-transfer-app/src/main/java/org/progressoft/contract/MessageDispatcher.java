package org.progressoft.contract;

public interface MessageDispatcher {

    void dispatchMessage(String message);
}
