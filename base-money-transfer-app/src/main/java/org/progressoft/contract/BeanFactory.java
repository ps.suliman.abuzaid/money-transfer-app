package org.progressoft.contract;

public interface BeanFactory {

    XmlBodyValidator getXmlPaymentValidator();

    PaymentParser getPaymentParser();

    PaymentValidator getPaymentValidator();

    EnvironmentPropertyService getEnvironmentPropertyService();

    ParticipantRepository getParticipantRepository();

    PurposeCodeRepository getPurposeCodeRepository();

    RejectionMotiveRepository getRejectionMotiveRepository();

    CurrencyRepository getCurrencyRepository();

    AccountValidator getAccountValidator();

    BankConfiguration getBankConfiguration();

    MessageIdGenerator getMessageIdGenerator();

    PaymentToReplyBodyMapper getPaymentToReplyBodyMapper();

    PaymentRepository getPaymentRepository();

    MessageDispatcher getMessageDispatcher();

    ReplyRepository getReplyRepository();

    ReplyService getReplyService();

    PaymentRequestService getPaymentRequestService();

    AccountCreditorService getAccountCreditorService();

    NotificationService getNotificationService();

    ResponseService getResponseService();

    XmlBodyValidator getXmlResponseValidator();

    ErrorHandler getPaymentErrorDispatcher();

    ErrorHandler getResponseErrorDispatcher();

    ResponseParser getResponseParser();
}
