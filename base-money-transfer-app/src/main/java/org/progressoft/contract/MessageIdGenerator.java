package org.progressoft.contract;

public interface MessageIdGenerator {

    String generateUniqueId();

}
