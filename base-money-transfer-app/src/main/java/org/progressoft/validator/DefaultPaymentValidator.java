package org.progressoft.validator;

import org.iban4j.IbanFormatException;
import org.iban4j.IbanUtil;
import org.progressoft.contract.AccountValidator;
import org.progressoft.contract.BankConfiguration;
import org.progressoft.contract.PaymentValidator;
import org.progressoft.entity.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

public class DefaultPaymentValidator implements PaymentValidator {

    private final BankConfiguration bankConfiguration;
    private final AccountValidator accountValidator;

    public DefaultPaymentValidator(BankConfiguration bankConfiguration,
                                   AccountValidator accountValidator) {
        this.bankConfiguration = bankConfiguration;
        this.accountValidator = accountValidator;
    }

    @Override
    public Set<RejectionMotive> validatePayment(Payment payment) {
        Set<RejectionMotive> rejectionMotives = new HashSet<>();
        validatePaymentHeaderInfo(payment, rejectionMotives);
        validatePaymentId(payment, rejectionMotives);
        validatePaymentTypeInfo(payment, rejectionMotives);
        validateSettlementAmountAndDate(payment, rejectionMotives);
        validateChargeBearer(payment, rejectionMotives);
        validateParticipants(payment, rejectionMotives);
        validateDebtorInfo(payment, rejectionMotives);
        validateCreditorInfo(payment, rejectionMotives);
        validateAccount(payment, rejectionMotives);
        return rejectionMotives;
    }

    private void validateChargeBearer(Payment payment, Set<RejectionMotive> rejectionMotives) {
        addDefaultMotiveIfNullOrNotEqualValue(payment.getChargeBearer(),
                "SLEV", "Missing or Invalid Charge Bearer", rejectionMotives);
    }

    private void validateCreditorInfo(Payment payment, Set<RejectionMotive> rejectionMotives) {
        validateCreditorName(payment.getCreditorName(), rejectionMotives);
        validateCreditorAddressLines(payment.getCreditorAddressLines(), rejectionMotives);
        validateIban(payment.getCreditorAccountIban(), "AC03", rejectionMotives);
    }

    private void validateDebtorInfo(Payment payment, Set<RejectionMotive> rejectionMotives) {

        validateDebtorName(payment.getDebtorName(),
                rejectionMotives);
        validateDebtorAddressLines(payment.getDebtorAddressLines(), rejectionMotives);
        validateIban(payment.getDebtorAccountIban(), bankConfiguration.getDefaultRejectionCode(), rejectionMotives);
    }

    private void validateSettlementAmountAndDate(Payment payment, Set<RejectionMotive> rejectionMotives) {
        Currency currency = validateCurrency(payment, rejectionMotives);
        BigDecimal amount = payment.getInterBankSettlementAmount();
        validateSettlementAmount(amount,
                currency, rejectionMotives);
        addDefaultMotiveIfNullOrNotEqualValue(payment.getInterBankSettlementDate(),
                LocalDate.now(), "Missing or Invalid Settlement Date", rejectionMotives);
    }

    private void validatePaymentHeaderInfo(Payment payment, Set<RejectionMotive> rejectionMotives) {
        addDefaultMotiveIfNullOrEmpty(payment.getMessageId(), "Missing Message Id", rejectionMotives);
        if (Objects.isNull(payment.getCreationDateTime()))
            addDefaultRejectionMotive("Missing Creation Date And Time", rejectionMotives);
        addDefaultMotiveIfNullOrNotEqualValue(payment.getNumberOfTransactions(),
                1, "Missing or Invalid Number of Transactions", rejectionMotives);
        addDefaultMotiveIfNullOrNotEqualValue(payment.getSettlementMethod(),
                "CLRG", "Missing or Invalid Settlement Method", rejectionMotives);
    }

    private void validatePaymentId(Payment payment, Set<RejectionMotive> rejectionMotives) {
        addDefaultMotiveIfNullOrEmpty(payment.getInstructionId(), "Missing Instructing Id", rejectionMotives);
        addDefaultMotiveIfNullOrEmpty(payment.getEndToEndId(), "Missing EndToEnd Id", rejectionMotives);
        addDefaultMotiveIfNullOrEmpty(payment.getTransactionId(), "Missing Transaction Id", rejectionMotives);
    }

    private void validatePaymentTypeInfo(Payment payment, Set<RejectionMotive> rejectionMotives) {
        addMotiveIfNullOrNotEqualValue(payment.getClearingChannel(),
                "RTNS", rejectionMotives);
        addMotiveIfNullOrNotEqualValue(payment.getServiceLevel(),
                "0100", rejectionMotives);
        addMotiveIfNullOrNotEqualValue(payment.getLocalInstrument(),
                "CSDC", rejectionMotives);
        validatePurposeCode(payment, rejectionMotives);
    }

    private void validateAccount(Payment payment, Set<RejectionMotive> rejectionMotives) {
        AccountRequest accountRequest = new AccountRequest();
        accountRequest.setName(payment.getCreditorName());
        accountRequest.setAddressLines(payment.getCreditorAddressLines());
        accountRequest.setSettlementAmount(payment.getInterBankSettlementAmount());
        accountRequest.setSettlementIsoCurrency(payment.getInterBankSettlementCurrency());
        accountRequest.setIban(payment.getCreditorAccountIban());
        rejectionMotives.addAll(accountValidator.validateAccount(accountRequest));
    }

    private void validateIban(String debtorIban, String rejectionCode, Set<RejectionMotive> rejectionMotives) {
        if (Objects.isNull(debtorIban)) {
            addRejectionMotive(rejectionCode, rejectionMotives);
        }
        try {
            IbanUtil.validate(debtorIban);
        } catch (IbanFormatException e) {
            addRejectionMotive(rejectionCode, rejectionMotives);
        }
    }

    private void addRejectionMotive(String rejectionCode, Set<RejectionMotive> rejectionMotives) {
        rejectionMotives.add(bankConfiguration.getRejectionMotiveByCode(rejectionCode));
    }

    private void addDefaultRejectionMotive(String motiveDescription, Set<RejectionMotive> rejectionMotives) {
        String defaultRejectionCode = bankConfiguration.getDefaultRejectionCode();
        rejectionMotives.add(new RejectionMotive(defaultRejectionCode, motiveDescription));
    }

    private void validateCreditorAddressLines(List<String> addressLines,
                                              Set<RejectionMotive> rejectionMotives) {
        if (!Objects.isNull(addressLines) && getNumberOfAddressLinesSections(addressLines) >= 2) {
            return;
        }
        addRejectionMotive("ACDM", rejectionMotives);
    }

    private void validateDebtorAddressLines(List<String> addressLines,
                                            Set<RejectionMotive> rejectionMotives) {
        if (!Objects.isNull(addressLines) && getNumberOfAddressLinesSections(addressLines) >= 2) {
            return;
        }
        addDefaultRejectionMotive("Invalid or Missing Debtor AddressLines", rejectionMotives);
    }

    private static int getNumberOfAddressLinesSections(List<String> addressLines) {
        return addressLines
                .stream().mapToInt(addressLine -> addressLine.split(" ").length)
                .sum();
    }

    private void validateCreditorName(String name, Set<RejectionMotive> rejectionMotives) {
        if (isNameValid(name)) return;
        addRejectionMotive("ACDM", rejectionMotives);
    }

    private void validateDebtorName(String name, Set<RejectionMotive> rejectionMotives) {
        if (isNameValid(name)) return;
        addDefaultRejectionMotive("Missing Or Invalid Debtor Name", rejectionMotives);
    }

    private static boolean isNameValid(String name) {
        return !Objects.isNull(name) && !name.isEmpty() && name.split(" ").length >= 3;
    }

    private void validateParticipants(Payment payment, Set<RejectionMotive> rejectionMotives) {
        String currentParticipantBic = bankConfiguration.getCurrentParticipantBicFi();
        Participant instructingParticipant = validateParticipant(payment.getInstructingAgentBicFi(), rejectionMotives);
        Participant instructedParticipant = validateParticipant(payment.getInstructedAgentBicFi(), rejectionMotives);
        Participant debtorParticipant = validateParticipant(payment.getDebtorAgentBicFi(),
                "AC02", rejectionMotives);
        Participant creditorParticipant = validateParticipant(payment.getCreditorAgentBicFi(),
                "AC03", rejectionMotives);
        ValidateIfDebtorAgentEqualInstructingAgent(rejectionMotives, debtorParticipant, instructingParticipant);
        ValidateIfCreditorAgentEqualInstructedAgent(rejectionMotives, creditorParticipant, instructedParticipant);
        validateIfInstructedEqualCurrent(rejectionMotives, instructedParticipant, currentParticipantBic);
    }

    private void validateIfInstructedEqualCurrent(Set<RejectionMotive> rejectionMotives, Participant instructedParticipant, String currentParticipantBic) {
        if (Objects.isNull(instructedParticipant))
            return;
        addDefaultMotiveIfNullOrNotEqualValue(instructedParticipant.getBic(),
                currentParticipantBic,
                "Instructed Participant is Missing or not the same as Current Participant",
                rejectionMotives);
    }

    private void ValidateIfCreditorAgentEqualInstructedAgent(Set<RejectionMotive> rejectionMotives, Participant creditorParticipant, Participant instructedParticipant) {
        addDefaultMotiveIfNullOrNotEqualValue(creditorParticipant,
                instructedParticipant,
                "Creditor Agent is missing or not the same as Instructed Agent",
                rejectionMotives);
    }

    private void ValidateIfDebtorAgentEqualInstructingAgent(Set<RejectionMotive> rejectionMotives, Participant debtorParticipant, Participant instructingParticipant) {
        addDefaultMotiveIfNullOrNotEqualValue(debtorParticipant,
                instructingParticipant,
                "Debtor Agent is missing or not the same as Instructing Agent",
                rejectionMotives);
    }

    private Participant validateParticipant(String bic, Set<RejectionMotive> rejectionMotives) {
        Participant participant =
                bankConfiguration.getParticipantByBic(bic);
        if (Objects.isNull(participant))
            addDefaultRejectionMotive("Missing Or Invalid Agent", rejectionMotives);
        return participant;
    }

    private Participant validateParticipant(String bic, String rejectionCode, Set<RejectionMotive> rejectionMotives) {
        Participant participant =
                bankConfiguration.getParticipantByBic(bic);
        if (Objects.isNull(participant))
            addRejectionMotive(rejectionCode, rejectionMotives);
        return participant;
    }

    private void validateSettlementAmount(BigDecimal amount,
                                          Currency currency,
                                          Set<RejectionMotive> rejectionMotives) {
        if (isAmountValid(amount, currency))
            return;
        addDefaultRejectionMotive("Missing or Invalid Settlement Amount", rejectionMotives);
    }

    private static boolean isAmountValid(BigDecimal amount, Currency currency) {
        return !Objects.isNull(amount) &&
                amount.compareTo(BigDecimal.ZERO) > 0 &&
                amount.scale() == currency.getDefaultFractionDigits();
    }

    private Currency validateCurrency(Payment payment,
                                      Set<RejectionMotive> rejectionMotives) {
        Currency currency = bankConfiguration.getCurrencyByIsoCurrency(payment.getInterBankSettlementCurrency());
        if (Objects.isNull(currency))
            addDefaultRejectionMotive("Missing Or Invalid Currency", rejectionMotives);
        return currency;
    }

    private void validatePurposeCode(Payment payment, Set<RejectionMotive> rejectionMotives) {
        PurposeCode purposeCode = bankConfiguration.getPurposeCodeByCode(payment.getCategoryPurpose());
        if (!Objects.isNull(purposeCode)) return;
        addRejectionMotive("FF03", rejectionMotives);
    }

    private void addMotiveIfNullOrNotEqualValue(Object value,
                                                Object fixedValue,
                                                Set<RejectionMotive> rejectionMotives) {
        if (!Objects.isNull(value) && value.equals(fixedValue))
            return;
        addRejectionMotive("FF03", rejectionMotives);
    }

    private void addDefaultMotiveIfNullOrNotEqualValue(Object value,
                                                       Object fixedValue,
                                                       String motiveDescription,
                                                       Set<RejectionMotive> rejectionMotives) {
        if (!Objects.isNull(value) && value.equals(fixedValue))
            return;
        addDefaultRejectionMotive(motiveDescription, rejectionMotives);
    }

    private void addDefaultMotiveIfNullOrEmpty(String value,
                                               String motiveDescription,
                                               Set<RejectionMotive> rejectionMotives) {
        if (!Objects.isNull(value) && !value.isEmpty())
            return;
        addDefaultRejectionMotive(motiveDescription, rejectionMotives);
    }
}
