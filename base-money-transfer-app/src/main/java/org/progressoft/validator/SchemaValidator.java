package org.progressoft.validator;

import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Path;

class SchemaValidator {
    void validateXmlToSchema(String xml, Path xsdSchemaPath) throws IOException, SAXException {
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Source schemaFile = new StreamSource(xsdSchemaPath.toFile());
        Validator validator;
        Schema schema = factory.newSchema(schemaFile);
        validator = schema.newValidator();
        validator.validate(new StreamSource(new StringReader(xml)));
    }
}
