package org.progressoft.validator;

import org.progressoft.contract.XmlBodyValidator;
import org.progressoft.exception.InvalidXmlBodyException;
import org.progressoft.exception.NullXmlBodyException;
import org.progressoft.exception.XmlBodyValidationException;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Objects;

public class DefaultXmlBodyValidator implements XmlBodyValidator {
    private final Path xsdSchemaPath;
    private final SchemaValidator schemaValidator;

    public DefaultXmlBodyValidator(Path xsdSchemaPath) {
        this.xsdSchemaPath = xsdSchemaPath;
        this.schemaValidator = new SchemaValidator();
    }

    @Override
    public void validateXmlBody(String xmlBody) {
        if (Objects.isNull(xmlBody)) {
            throw new NullXmlBodyException("Xml string is null");
        }
        if (xmlBody.isEmpty()) {
            throw new InvalidXmlBodyException("Xml string is empty");
        }
        try {
            schemaValidator.validateXmlToSchema(xmlBody, xsdSchemaPath);
        } catch (SAXException | IOException e) {
            throw new XmlBodyValidationException(e, e.getMessage());
        }
    }

}
