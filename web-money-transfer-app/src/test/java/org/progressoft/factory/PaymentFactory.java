package org.progressoft.factory;

import org.progressoft.entity.Payment;
import org.progressoft.enums.Reason;
import org.progressoft.enums.Status;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class PaymentFactory {

    public static Payment getValidPayment() {
        Payment payment = new Payment();
        payment.setMessageId("MessageId12345");
        payment.setCreationDateTime(LocalDateTime.now());
        payment.setNumberOfTransactions(1);
        payment.setSettlementMethod("CLRG");
        payment.setInstructionId("JGBA2812094750927334298");
        payment.setEndToEndId("NOTPROVIDED");
        payment.setTransactionId("JGBA2812094750927334297");
        payment.setClearingChannel("RTNS");
        payment.setServiceLevel("0100");
        payment.setLocalInstrument("CSDC");
        payment.setCategoryPurpose("11110");
        payment.setInterBankSettlementCurrency("JOD");
        payment.setInterBankSettlementAmount(new BigDecimal("234.000"));
        payment.setInterBankSettlementDate(LocalDate.now());
        payment.setChargeBearer("SLEV");
        payment.setInstructingAgentBicFi("JGBAJOA0");
        payment.setInstructedAgentBicFi("HBHOJOA0");
        payment.setDebtorName("NISREEN MOHAMMAD YOUSEF");
        payment.setDebtorAddressLines(List.of("New Zarqa"));
        payment.setDebtorAccountIban("JO93JGBA6010000290450010010000");
        payment.setDebtorAgentBicFi("JGBAJOA0");
        payment.setCreditorName("MAEN SAMI HATTAR SALEM");
        payment.setCreditorAddressLines(List.of("Amman Jordan SLT JO SLT"));
        payment.setCreditorAccountIban("JO83HBHO0320000033330600101001");
        payment.setCreditorAgentBicFi("HBHOJOA0");
        payment.setStatus(Status.PENDING_RESPONSE.toString());
        payment.setAuthStatus(Reason.AUTH.toString());
        payment.setRejectionCodes(new ArrayList<>());
        return payment;
    }

    public static Payment getInvalidPayment() {
        Payment payment = getValidPayment();
        payment.setDebtorName("Ahmed Ali");
        payment.setInterBankSettlementAmount(new BigDecimal("1"));
        payment.setAuthStatus(Reason.NAUT.toString());
        payment.setRejectionCodes(List.of("AC02"));
        return payment;
    }
}
