package org.progressoft.servlet;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;
import org.progressoft.contract.ObjectJsonMapper;
import org.progressoft.contract.PaymentRepository;
import org.progressoft.contract.PaymentRequestService;
import org.progressoft.entity.Payment;
import org.progressoft.entity.PaymentFilterRequest;
import org.progressoft.factory.PaymentFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

class PaymentServletTest {

    private final PaymentRequestService requestService = mock(PaymentRequestService.class);
    private final PaymentRepository paymentRepository = mock(PaymentRepository.class);
    private final ObjectJsonMapper objectJsonMapper = mock(ObjectJsonMapper.class);

    private final HttpServletRequest request = mock(HttpServletRequest.class);
    private final HttpServletResponse response = mock(HttpServletResponse.class);
    private final PaymentServlet paymentServlet = new PaymentServlet(requestService, paymentRepository, objectJsonMapper);

    @Test
    void givenRequestParameters_whenDoGet_thenSuccess() throws IOException {
        setupPaginationOnlyFilter();
        PaymentFilterRequest filterRequest = getFilterRequest();
        List<Payment> payments = getPayments();
        setupMockData(filterRequest, payments);
        PrintWriter printWriter = mock(PrintWriter.class);
        when(response.getWriter()).thenReturn(printWriter);
        doNothing().when(printWriter).write("Mock Json String");
        paymentServlet.doGet(request, response);
        verify(request, times(14)).getParameter(anyString());
        verify(response).getWriter();
        verify(printWriter).write("Mock Json String");
        verify(paymentRepository, times(3))
                .getTotalPaymentsNumberByFilter(any(PaymentFilterRequest.class));
    }

    private void setupMockData(PaymentFilterRequest filterRequest, List<Payment> payments) {
        when(paymentRepository.getPaymentsByFilterRequest(filterRequest)).thenReturn(
                payments);
        when(paymentRepository.getTotalPaymentsNumberByFilter(filterRequest)).thenReturn(2);
        when(paymentRepository.getTotalPaymentsNumberByFilter(filterRequest)).thenReturn(1);
        when(paymentRepository.getTotalPaymentsNumberByFilter(filterRequest)).thenReturn(1);
        when(paymentRepository.getTotalSumByPaymentFilter(filterRequest)).thenReturn(new BigDecimal("40.232"));
        when(objectJsonMapper.convertToJson(any())).thenReturn("Mock Json String");
    }

    private static List<Payment> getPayments() {
        return Arrays.asList(PaymentFactory.getValidPayment(), PaymentFactory.getInvalidPayment());
    }

    private void setupPaginationOnlyFilter() {
        when(request.getParameter("messageId")).thenReturn(null);
        when(request.getParameter("amountFrom")).thenReturn(null);
        when(request.getParameter("amountTo")).thenReturn(null);
        when(request.getParameter("settlementDateFrom")).thenReturn(null);
        when(request.getParameter("settlementDateTo")).thenReturn(null);
        when(request.getParameter("debtorAccount")).thenReturn(null);
        when(request.getParameter("debtorBic")).thenReturn(null);
        when(request.getParameter("creditorAccount")).thenReturn(null);
        when(request.getParameter("status")).thenReturn(null);
        when(request.getParameter("currency")).thenReturn(null);
        when(request.getParameter("pageCount")).thenReturn("2");
        when(request.getParameter("pageNumber")).thenReturn("1");
    }

    private PaymentFilterRequest getFilterRequest() {
        PaymentFilterRequest filterRequest = new PaymentFilterRequest();
        filterRequest.setPageCount(2);
        filterRequest.setPageNumber(1);
        return filterRequest;
    }

}