<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Payment Dashboard</title>
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN"
      crossorigin="anonymous"
    />
    <script
      src="https://code.jquery.com/jquery-3.7.1.min.js"
      integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo="
      crossorigin="anonymous"
    ></script>

    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
      crossorigin="anonymous"
    ></script>

    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> <%@taglib
    prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
  </head>
  <body>
    <form method="get" id="paymentForm" class="container-fluid">
      <div class="row row-cols-1">
        <div class="col-6 mb-4 p-2">
          <label for="messageId" class="form-label">Message ID</label>
          <input
            id="messageId"
            name="messageId"
            class="form-control border-dark"
          />
        </div>
        <div class="col-3 mb-4 p-2">
          <label for="settlementDateFrom" class="form-label"
            >Settlement Date From</label
          >
          <input
            id="settlementDateFrom"
            name="settlementDateFrom"
            class="form-control border-dark"
            type="date"
          />
        </div>
        <div class="col-3 mb-4 p-2">
          <label for="settlementDateTo" class="form-label"
            >Settlement Date To</label
          >
          <input
            id="settlementDateTo"
            name="settlementDateTo"
            class="form-control border-dark"
            type="date"
          />
        </div>
        <div class="col-6 mb-4 p-2">
          <label for="debtorBic" class="form-label">Debtor BIC</label>
          <input
            id="debtorBic"
            name="debtorBic"
            class="form-control border-dark"
          />
        </div>
        <div class="col-3 mb-4 p-2">
          <label for="debtorAccount" class="form-label">Debtor Account</label>
          <input
            id="debtorAccount"
            name="debtorAccount"
            class="form-control border-dark"
          />
        </div>
        <div class="col-3 mb-4 p-2">
          <label for="creditorAccount" class="form-label"
            >Creditor Account</label
          >
          <input
            id="creditorAccount"
            name="creditorAccount"
            class="form-control border-dark"
          />
        </div>
        <div class="col-3 mb-4 p-2">
          <label for="currency" class="form-label">Currency</label>
          <select
            id="currency"
            name="currency"
            class="form-control border-dark"
          >
            <option value="">ALL</option>
            <c:forEach var="currency" items="${sessionScope.currencies}">
              <option value="${currency}">${currency}</option>
            </c:forEach>
          </select>
        </div>
        <div class="col-3 mb-4 p-2">
          <label for="status" class="form-label">Status</label>
          <select id="status" name="status" class="form-control border-dark">
            <option value="">ALL</option>
            <c:forEach var="status" items="${sessionScope.statuses}">
              <option value="${status}">${status}</option>
            </c:forEach>
          </select>
        </div>
        <div class="col-3 mb-4 p-2">
          <label for="amountFrom" class="form-label">Amount From</label>
          <input
            id="amountFrom"
            name="amountFrom"
            class="form-control border-dark"
            type="number"
            min="0"
            step="any"
          />
        </div>
        <div class="col-3 mb-4 p-2">
          <label for="amountTo" class="form-label">Amount To</label>
          <input
            id="amountTo"
            name="amountTo"
            class="form-control border-dark"
            type="number"
            min="0"
            step="any"
          />
        </div>
      </div>
      <div class="d-flex flex-row justify-content-end">
        <button type="submit" class="btn btn-primary mx-2">Search</button>
        <button
          type="button"
          onclick="clearForm()"
          class="btn btn-warning mx-2"
        >
          Clear
        </button>
      </div>
    </form>

    <div
    class="d-flex flex-row justify-content-between align-items-center m-3 fs-5"
    >
    <div class="mx-1"></div>
    <div class="mx-1" id="paginator"></div>
    <div class="mx-1">
      <select id="pageCount">
        <c:forEach var="count" items="${sessionScope.defaultPageCounts}">
          <option value="${count}">${count}</option>
        </c:forEach>
      </select>
    </div>
  </div>
  <div class="mx-1 row row-cols-1" >
    <label class="col-3">Total Count: <span id="totalCount"></span></label>
    <label class="col-3">Total Amount: <span id="totalAmount"></span></label>
    <label class="col-3">AUTH Count: <span id="authCount"></span></label>
    <label class="col-3">NAUT Count: <span id="nautCount"></span></label>
</div>
  
    <div class="table-responsive">
      <table class="table table-dark table-striped-columns text-center">
        <thead>
          <tr>
            <th scope="col">#</th>
            <c:forEach items="${sessionScope.columns}" var="column">
              <th scope="col">${column}</th>
            </c:forEach>
            <th scope="col" colspan="2">actions</th>
          </tr>
        </thead>
        <tbody id="tableData"></tbody>
      </table>
      <p id="paymentMessage" class="text-info text-center fs-3"></p>
    </div>
    <script
      type="text/javascript"
      src="${pageContext.request.contextPath}/js/paymentFilterService.js"
    ></script>
  </body>
</html>
