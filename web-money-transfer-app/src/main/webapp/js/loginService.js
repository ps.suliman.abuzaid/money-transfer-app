function login(){
    const username = document.getElementById('username').value;
    const password = document.getElementById('password').value;
        
    const apiUrl = '/Login';

    
    fetch(apiUrl, {
        method: 'POST', // Replace with the desired HTTP method
        body:{
            username,
            password
        }
    }).then(response=>console.log(response.status))
}