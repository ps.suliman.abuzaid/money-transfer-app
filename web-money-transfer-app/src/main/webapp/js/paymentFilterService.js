initPayments();

$("#paymentForm").on("submit", (event) => {
  event.preventDefault();
  getPayments();
});

function initPayments() {
  const filterData = {
    pageCount: $("#pageCount").val(),
    pageNumber: 1,
  };
  getPaymentsByFilterData(filterData);
}

function getPayments() {
  const filterData = getFilterData(1);
  getPaymentsByFilterData(filterData);
}
function getPaymentsByPageNumber(pageNumber) {
  const filterData = getFilterData(pageNumber);
  getPaymentsByFilterData(filterData);
}

function getFilterData(pageNumber) {
  return {
    messageId: $("#messageId").val(),
    settlementDateFrom: $("#settlementDateFrom").val(),
    settlementDateTo: $("#settlementDateTo").val(),
    debtorBic: $("#debtorBic").val(),
    debtorAccount: $("#debtorAccount").val(),
    creditorAccount: $("#creditorAccount").val(),
    amountFrom: $("#amountFrom").val(),
    amountTo: $("#amountTo").val(),
    status: $("#status").val(),
    currency: $("#currency").val(),
    pageCount: $("#pageCount").val(),
    pageNumber,
  };
}

function getPaymentsByFilterData(filterData) {
  let queryParams = new URLSearchParams(filterData);

  queryParams.forEach(([k, v]) => {
    if (!v) {
      queryParams.delete(k);
    }
  });

  let url = `/auth/payment?${queryParams.toString()}`;
  fetchPayments(url);
}

function fetchPayments(url) {
  fetch(url, {
    method: "GET",
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      $("#tableData").empty();
      $("#paymentMessage").empty();
      const paginator = $("#paginator").empty();
      generateSummryData(data);
      if (data.payments?.length === 0) {
        $("#paymentMessage").append(`No Payments Found`);
        return;
      }
      generatePaymentTableRecords(data);
      const paginationData = data.paginationData;

      for (
        let page = paginationData.startPage;
        page <= paginationData.endPage;
        page++
      ) {
        const anchor = $("<a>");
        anchor.attr("href", "#");
        anchor.attr("class", "mx-1");
        anchor.on("click", getPaymentsByPageNumber.bind(null, page));
        anchor.text(page);
        paginator.append(anchor);
      }
    });
}

function generatePdf(
  messageId,
  instructionId,
  endToEndId,
  transactionId,
  settlementDate,
  isPaymentMessage
) {
  const request = {
    messageId,
    instructionId,
    endToEndId,
    transactionId,
    settlementDate,
    isPaymentMessage,
  };
  let queryParams = new URLSearchParams(request);
  const filename =
    messageId +
    (isPaymentMessage === "true" ? "-pacs.008.xml" : "-pacs.002.xml");
  const url = "/auth/download?" + queryParams.toString();
  fetch(url, {
    method: "GET",
  })
    .then((response) => response.blob())
    .then((blob) => {
      const url = window.URL.createObjectURL(blob);
      const a = document.createElement("a");
      a.href = url;
      a.download = filename;
      document.body.appendChild(a);
      a.click();
      window.URL.revokeObjectURL(url);
    });
}

function generatePaymentTableRecords(data) {
  $.each(data.payments, (index, payment) => {
    const settlementDateObject = payment.interBankSettlementDate;
    const settlementDate = `${settlementDateObject.year}-${settlementDateObject.month}-${settlementDateObject.day}`;
    $("#tableData").append(`<tr>
              <th class="px-4" scope="row">
                ${
                  index +
                  1 +
                  (data.paginationData.pageNumber - 1) *
                    data.paginationData.pageCount
                }
              </th>
              <td class="px-4">${payment.messageId}</td>
              <td class="px-4">${payment.instructionId}</td>
              <td class="px-4">${payment.endToEndId}</td>
              <td class="px-4">${payment.transactionId}</td>
              <td class="px-4">${settlementDate}</td>
              <td class="px-4">${payment.debtorAgentBicFi}</td>
              <td class="px-4">${payment.debtorAccountIban}</td>
              <td class="px-4">${payment.debtorName}</td>
              <td class="px-4">${payment.debtorAddressLines}</td>
              <td class="px-4">${payment.creditorAccountIban}</td>
              <td class="px-4">${payment.creditorName}</td>
              <td class="px-4">${payment.creditorAddressLines}</td>
              <td class="px-4">${payment.interBankSettlementAmount}</td>
              <td class="px-4">${payment.interBankSettlementCurrency}</td>
              <td class="px-4">${payment.authStatus}</td>
              <td class="px-4">${payment.rejectionCodes}</td>
              <td class="px-4">
              <button class="btn btn-light" onclick="generatePdf('${
                payment.messageId
              }', '${payment.instructionId}','${payment.endToEndId}', '${
      payment.transactionId
    }',
               '${settlementDate}', 'true')">
              Download Xml Payment
              </button>
              </td>
              <td class="px-4">
              <button class="btn btn-light" onclick="generatePdf('${
                payment.messageId
              }', '${payment.instructionId}','${payment.endToEndId}', '${
      payment.transactionId
    }',
               '${settlementDate}', 'false')">
              Download Xml Reply
              </button>
              </td>
            </tr>`);
  });
}

function generateSummryData(data) {
  const summaryData = data.summaryData;
  const totalCountElm = $("#totalCount");
  const totalAmountElm = $("#totalAmount");
  const authCountElm = $("#authCount");
  const nautCountElm = $("#nautCount");
  totalCountElm.empty();
  totalAmountElm.empty();
  authCountElm.empty();
  nautCountElm.empty();
  totalCountElm.append(data.paginationData.totalItemsNumber);
  totalAmountElm.append(summaryData.totalSum);
  authCountElm.append(summaryData.authCount);
  nautCountElm.append(summaryData.nautCount);
}

function clearForm() {
  $("#messageId").val("");
  $("#settlementDateFrom").val("");
  $("#settlementDateTo").val("");
  $("#debtorBic").val("");
  $("#debtorAccount").val("");
  $("#creditorAccount").val("");
  $("#amountFrom").val("");
  $("#amountTo").val("");
  $("#status").val("");
  $("#currency").val("");
}
