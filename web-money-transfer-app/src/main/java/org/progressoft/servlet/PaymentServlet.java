package org.progressoft.servlet;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.progressoft.contract.ObjectJsonMapper;
import org.progressoft.contract.PaymentRepository;
import org.progressoft.contract.PaymentRequestService;
import org.progressoft.entity.Payment;
import org.progressoft.entity.PaymentFilterRequest;
import org.progressoft.enums.Reason;
import org.progressoft.exception.XmlBodyValidationException;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public class PaymentServlet extends HttpServlet {
    private final PaymentRequestService requestService;
    private final PaymentRepository paymentRepository;
    private final ObjectJsonMapper objectJsonMapper;

    public PaymentServlet(PaymentRequestService requestService, PaymentRepository paymentRepository, ObjectJsonMapper objectJsonMapper) {
        this.requestService = requestService;
        this.paymentRepository = paymentRepository;
        this.objectJsonMapper = objectJsonMapper;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PaymentFilterRequest filterRequest = getPaymentFilterRequest(req);
        List<Payment> payments = getPayments(req, filterRequest);
        Map<String, Number> summaryData = getSummaryData(filterRequest);
        Map<String, Integer> paginationData = getPaginationData(filterRequest);
        String paymentsJson = objectJsonMapper.convertToJson(Map.of("payments", payments,
                "paginationData", paginationData, "summaryData", summaryData));
        resp.getWriter().write(paymentsJson);
    }

    private Map<String, Number> getSummaryData(PaymentFilterRequest filterRequest) {
        BigDecimal totalSum = paymentRepository.getTotalSumByPaymentFilter(filterRequest);
        int totalAuthCount = getAuthStatusTotalCount(filterRequest, Reason.AUTH);
        int totalNAutCount = getAuthStatusTotalCount(filterRequest, Reason.NAUT);
        Map<String, Number> summaryData = new HashMap<>();
        summaryData.put("authCount", totalAuthCount);
        summaryData.put("nautCount", totalNAutCount);
        summaryData.put("totalSum", totalSum);
        return summaryData;
    }

    private int getAuthStatusTotalCount(PaymentFilterRequest filterRequest, Reason reason) {
        int result = 0;
        String authReason = reason.toString();
        String filterAuthStatus = filterRequest.getAuthStatus();
        if (isEmpty(filterAuthStatus) ||
                filterAuthStatus.equals(authReason)) {
            filterRequest.setAuthStatus(authReason);
            result = paymentRepository.getTotalPaymentsNumberByFilter(filterRequest);
            filterRequest.setAuthStatus(filterAuthStatus);
        }
        return result;
    }

    private List<Payment> getPayments(HttpServletRequest req, PaymentFilterRequest filterRequest) {
        List<Payment> payments = paymentRepository.getPaymentsByFilterRequest(filterRequest);
//        TODO remove as its not needed when responding with json containing the payments
        req.setAttribute("payments", payments);
        return payments;
    }

    private Map<String, Integer> getPaginationData(PaymentFilterRequest filterRequest) {
        Map<String, Integer> paginationData = new HashMap<>();
        //TODO remove
        paginationData.put("pageCount", filterRequest.getPageCount());
        //TODO remove
        paginationData.put("pageNumber", filterRequest.getPageNumber());
        //TODO remove
        paginationData.put("startPage", filterRequest.getStartPage());
        //TODO remove
        paginationData.put("endPage", filterRequest.getEndPage());

        paginationData.put("totalPages", filterRequest.getTotalPages());
        paginationData.put("totalItemsNumber", filterRequest.getTotalItems());
        return paginationData;
    }

    private PaymentFilterRequest getPaymentFilterRequest(HttpServletRequest req) {
        PaymentFilterRequest filterRequest = new PaymentFilterRequest();
        filterRequest.setMessageId(req.getParameter("messageId"));
        setAmountFrom(req, filterRequest);
        setAmountTo(req, filterRequest);
        setDateFrom(req, filterRequest);
        setDateTo(req, filterRequest);
        setDebtorInfo(req, filterRequest);
        setCreditorAndCurrencyAndAuthStatus(req, filterRequest);
        setRequestPageMetaData(req, filterRequest);
        preparePaginationData(filterRequest);
        return filterRequest;
    }

    private void setCreditorAndCurrencyAndAuthStatus(HttpServletRequest req, PaymentFilterRequest filterRequest) {
        filterRequest.setCreditorAccountIban(req.getParameter("creditorAccount"));
        filterRequest.setCurrency(req.getParameter("currency"));
        filterRequest.setAuthStatus(req.getParameter("status"));
    }

    private void setRequestPageMetaData(HttpServletRequest req, PaymentFilterRequest filterRequest) {
        filterRequest.setPageCount(Integer.parseInt(req.getParameter("pageCount")));
        filterRequest.setPageNumber(Integer.parseInt(req.getParameter("pageNumber")));
    }

    private void setDebtorInfo(HttpServletRequest req, PaymentFilterRequest filterRequest) {
        filterRequest.setDebtorAgentBicFi(req.getParameter("debtorBic"));
        filterRequest.setDebtorAccountIban(req.getParameter("debtorAccount"));
    }

    private void setDateTo(HttpServletRequest req, PaymentFilterRequest filterRequest) {
        String dateTo = req.getParameter("settlementDateTo");
        if (isNotEmpty(dateTo)) {
            filterRequest.setSettlementDateTo(LocalDate.parse(dateTo));
        }
    }

    private void setDateFrom(HttpServletRequest req, PaymentFilterRequest filterRequest) {
        String dateFrom = req.getParameter("settlementDateFrom");
        if (isNotEmpty(dateFrom)) {
            filterRequest.setSettlementDateFrom(LocalDate.parse(dateFrom));
        }
    }

    private void setAmountTo(HttpServletRequest req, PaymentFilterRequest filterRequest) {
        String amountTo = req.getParameter("amountTo");
        if (isNotEmpty(amountTo)) {
            filterRequest.setSettlementAmountTo(new BigDecimal(amountTo));
        }
    }

    private void setAmountFrom(HttpServletRequest req, PaymentFilterRequest filterRequest) {
        String amountFrom = req.getParameter("amountFrom");
        if (isNotEmpty(amountFrom)) {
            filterRequest.setSettlementAmountFrom(new BigDecimal(amountFrom));
        }
    }

    private void preparePaginationData(PaymentFilterRequest filterRequest) {
        int totalNumber = paymentRepository.getTotalPaymentsNumberByFilter(filterRequest);
        int totalPages = (int) Math.ceil((double) totalNumber / filterRequest.getPageCount());
        setPaymentFilterPaginationData(filterRequest,totalPages, totalNumber);
    }

    private void setPaymentFilterPaginationData(PaymentFilterRequest filterRequest, int totalPages, int totalCount) {
//        TODO simplify and remove un-needed variables
        if (filterRequest.getPageNumber() > totalPages)
            filterRequest.setPageNumber(1);
        filterRequest.setPageCount(filterRequest.getPageCount());
        filterRequest.setPageNumber(filterRequest.getPageNumber());
        filterRequest.setTotalPages(totalPages);
        filterRequest.setStartPage(1);
        filterRequest.setEndPage(totalPages);
        filterRequest.setTotalItems(totalCount);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String contentType = "application/xml";
        resp.setContentType(contentType);
        try {
            processPaymentAndReturnReply(req, resp);
        } catch (XmlBodyValidationException e) {
            writeError(resp, HttpServletResponse.SC_BAD_REQUEST, "Bad Request: ", e);
        } catch (Exception e) {
            writeError(resp, HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
                    "Internal Server Error: ", e);
        }
    }

    private void writeError(HttpServletResponse resp, int errorStatus,
                            String prefix, Exception e) throws IOException {
        resp.setStatus(errorStatus);
        PrintWriter printWriter = resp.getWriter();
        printWriter.println(prefix + e.getMessage());
        printWriter.flush();
    }

    private void processPaymentAndReturnReply(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String xmlPayment = IOUtils.toString(req.getReader());
        String xmlReply = requestService.processRequest(xmlPayment);
        try (PrintWriter writer = resp.getWriter()) {
            writer.println(xmlReply);
        }
    }
}
