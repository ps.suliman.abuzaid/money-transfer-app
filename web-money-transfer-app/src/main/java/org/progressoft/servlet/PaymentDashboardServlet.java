package org.progressoft.servlet;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.progressoft.contract.CurrencyRepository;
import org.progressoft.enums.Reason;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public class PaymentDashboardServlet extends HttpServlet {

    private final CurrencyRepository currencyRepository;

    public PaymentDashboardServlet(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletContext context = req.getServletContext();
        initFormData(req, context);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/views/PaymentDashboard.jsp");
        requestDispatcher.forward(req, resp);
    }

    private void initFormData(HttpServletRequest req, ServletContext context) {
        HttpSession session = req.getSession();
        preparePaginationData(req, context);
        prepareCurrencies(session);
        prepareStatuses(session);
        prepareTableColumns(session);
    }

    private void prepareTableColumns(HttpSession session) {
        List<String> columns = Arrays.asList("Message ID", "Instruction ID", "End To End ID", "Transaction ID",
                "Settlement Date", "Debtor BIC", "Debtor Account", "Debtor Name", "Debtor Address",
                "Creditor Account", "Creditor Name", "Creditor Address", "Amount", "Currency",
                "Authorization status", "Rejection motive");
        session.setAttribute("columns", columns);
    }

    private void prepareStatuses(HttpSession session) {
        session.setAttribute("statuses", Arrays.asList(Reason.AUTH.toString(), Reason.NAUT.toString()));
    }

    private void prepareCurrencies(HttpSession session) {
        List<String> supportedCurrencies = currencyRepository.getSupportedIsoCurrencies();
        session.setAttribute("currencies", supportedCurrencies);
    }

    private void preparePaginationData(HttpServletRequest req, ServletContext context) {
        List<String> defaultPageCounts = Arrays.asList(context.getInitParameter("defaultPageCounts").split(","));
        int defaultPageCount = Integer.parseInt(defaultPageCounts.get(0));
        int pageCount = getPageValue(req, "pageCount", defaultPageCount);
        int pageNumber = getPageValue(req, "page", 1);
        setPaginationSessionData(req, defaultPageCounts, pageCount, pageNumber);
    }

    private void setPaginationSessionData(HttpServletRequest req, List<String> defaultPageCounts,
                                          int pageCount, int pageNumber) {
        HttpSession session = req.getSession();
        session.setAttribute("pageCount", pageCount);
        session.setAttribute("pageNumber", pageNumber);
        session.setAttribute("defaultPageCounts", defaultPageCounts);
    }

    private int getPageValue(HttpServletRequest req, String parameter, int defaultValue) {
        return isNotEmpty(req.getParameter(parameter)) ?
                Integer.parseInt(req.getParameter(parameter)) : defaultValue;
    }

}
