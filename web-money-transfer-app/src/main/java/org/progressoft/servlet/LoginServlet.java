package org.progressoft.servlet;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.progressoft.contract.UserService;
import org.progressoft.model.UserDTO;

import java.io.IOException;

public class LoginServlet extends HttpServlet {
    private final UserService userService;

    public LoginServlet(UserService userService) {
        this.userService = userService;
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/views/Login.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        UserDTO userDTO = getUserDTO(req);
        HttpSession session = req.getSession();
        boolean isAuthenticated = userService.authenticateUser(userDTO);
        session.setAttribute("authenticated", isAuthenticated);
        if (!isAuthenticated) {
            resp.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthenticated Access");
            return;
        }
        resp.sendRedirect("/auth/dashboard");
    }

    private static UserDTO getUserDTO(HttpServletRequest req) {
//        TODO accept basic authorization
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        return new UserDTO(username, password);
    }
}
