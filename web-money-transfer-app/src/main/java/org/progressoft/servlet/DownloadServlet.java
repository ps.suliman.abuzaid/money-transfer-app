package org.progressoft.servlet;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.progressoft.contract.PaymentRepository;
import org.progressoft.entity.Payment;
import org.progressoft.entity.PaymentCompositeKey;

import java.io.IOException;
import java.time.LocalDate;

public class DownloadServlet extends HttpServlet {

    private final PaymentRepository paymentRepository;

    public DownloadServlet(PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PaymentCompositeKey compositeKey = getPaymentCompositeKey(req);
        boolean isPaymentMessage = Boolean.parseBoolean(req.getParameter("isPaymentMessage"));
        Payment payment = paymentRepository.getPaymentByCompositeId(compositeKey);
        String data = getData(isPaymentMessage, payment);
        resp.setContentType("application/xml");
        resp.getWriter().write(data);
    }

    private String getData(boolean isPaymentMessage, Payment payment) {
        return isPaymentMessage ? payment.getXmlPayment() : payment.getXmlReply();
    }

    private PaymentCompositeKey getPaymentCompositeKey(HttpServletRequest req) {
        String messageId = req.getParameter("messageId");
        String instructionId = req.getParameter("instructionId");
        String endToEndId = req.getParameter("endToEndId");
        String transactionId = req.getParameter("transactionId");
        LocalDate settlementDate = LocalDate.parse(req.getParameter("settlementDate"));
        return new PaymentCompositeKey(messageId, instructionId, endToEndId,
                transactionId, settlementDate);
    }
}
