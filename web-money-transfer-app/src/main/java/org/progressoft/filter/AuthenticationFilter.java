package org.progressoft.filter;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

import static java.util.Objects.isNull;

public class AuthenticationFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession session = httpRequest.getSession();
        boolean isAuthenticated = getAuthenticationStatus(session);
        if (!isAuthenticated) {
            httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthenticated Access");
            return;
        }
        chain.doFilter(request, response);
    }

    private static boolean getAuthenticationStatus(HttpSession session) {
        Object authenticatedObject = session.getAttribute("authenticated");
        boolean isAuthenticated;
        if (isNull(authenticatedObject))
            isAuthenticated = false;
        else {
            isAuthenticated = (boolean) authenticatedObject;
        }
        return isAuthenticated;
    }
}
