package org.progressoft.filter;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

public class DomainFilter implements Filter {
    private String domainName;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        domainName = filterConfig.getServletContext().getInitParameter("domain.name");
        Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String host = request.getServerName();
        if (!host.equals(domainName)) {
            HttpServletResponse response = (HttpServletResponse) servletResponse;
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid domain");
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
