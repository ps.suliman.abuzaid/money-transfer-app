package org.progressoft.filter;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

import static org.apache.commons.lang3.StringUtils.equalsAny;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

//TODO Packages are clear
//TODO Splitted multiple modules for sake of re-usability
//TODO re-used written query for totals,sums and payments fetching
//TODO abstracted the serializing and deserializing with interfaces so we can change at anytime
//TODO Most of the requirements are done at the time given
//TODO mocked the external classes like servlets in order to do unit tests
//TODO refactored original code in order to re-use (had little changes which means that the code was clear and not coupled)
//TODO Didn't do the same mistakes from previous tasks
//TODO Changes are minimal and clear
//TODO Grade : 8.0
public class PaymentServletFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String contentType = request.getContentType();
        String httpMethod = request.getMethod();

        if (isContentTypeOrHttpMethodInvalid(contentType, httpMethod)) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().println("Bad Request: Invalid Content-Type Or Http Method");
            return;
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    private boolean isContentTypeOrHttpMethodInvalid(String contentType, String httpMethod) {
        return (isNotEmpty(contentType) && httpMethod.equals("GET")) ||
                (isNotEmpty(contentType) &&
                        !contentType.equals("application/xml")
                        && !httpMethod.equals("POST")) ||
                !equalsAny(httpMethod, "GET", "POST");
    }

}
