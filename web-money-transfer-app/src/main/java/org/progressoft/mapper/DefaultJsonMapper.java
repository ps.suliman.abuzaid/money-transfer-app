package org.progressoft.mapper;

import com.google.gson.Gson;
import org.progressoft.contract.ObjectJsonMapper;

public class DefaultJsonMapper implements ObjectJsonMapper {

    private final Gson gson;

    public DefaultJsonMapper() {
        this.gson = new Gson();
    }

    @Override
    public String convertToJson(Object object) {
        return gson.toJson(object);
    }
}
