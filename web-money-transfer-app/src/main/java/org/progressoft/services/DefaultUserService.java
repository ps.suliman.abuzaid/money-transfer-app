package org.progressoft.services;

import jakarta.servlet.ServletContext;
import org.progressoft.contract.UserService;
import org.progressoft.model.UserDTO;

public class DefaultUserService implements UserService {

    private final ServletContext context;

    public DefaultUserService(ServletContext context) {
        this.context = context;
    }

    @Override
    public boolean authenticateUser(UserDTO user) {
        String authUsername = context.getInitParameter("authUsername");
        String authPassword = context.getInitParameter("authPassword");
        return user.getUsername().equals(authUsername) && user.getPassword().equals(authPassword);
    }
}
