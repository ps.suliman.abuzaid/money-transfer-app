package org.progressoft.contract;

import org.progressoft.model.UserDTO;

public interface UserService {
    boolean authenticateUser(UserDTO user);
}
