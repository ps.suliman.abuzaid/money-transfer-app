package org.progressoft.contract;

public interface ObjectJsonMapper {

    String convertToJson(Object object);
}
