package org.progressoft.initializer;

import jakarta.servlet.*;
import org.progressoft.contract.*;
import org.progressoft.factory.DefaultBeanFactory;
import org.progressoft.filter.AuthenticationFilter;
import org.progressoft.filter.DomainFilter;
import org.progressoft.filter.PaymentServletFilter;
import org.progressoft.mapper.DefaultJsonMapper;
import org.progressoft.services.DefaultUserService;
import org.progressoft.servlet.DownloadServlet;
import org.progressoft.servlet.LoginServlet;
import org.progressoft.servlet.PaymentDashboardServlet;
import org.progressoft.servlet.PaymentServlet;

import java.util.EnumSet;
import java.util.Set;

public class ServletInitializer implements ServletContainerInitializer {
    private final BeanFactory beanFactory;

    public ServletInitializer() {
        this.beanFactory = new DefaultBeanFactory();
    }

    @Override
    public void onStartup(Set<Class<?>> set, ServletContext servletContext) {
        registerLoginServlet(servletContext);
        registerPaymentServlet(servletContext);
        registerPaymentDashboardServlet(servletContext);
        registerPDFGeneratorServlet(servletContext);

        registerPaymentServletFilter(servletContext);
        registerAuthenticationFilter(servletContext);
        registerDomainFilter(servletContext);
    }

    private void registerPDFGeneratorServlet(ServletContext servletContext) {
        PaymentRepository paymentRepository = beanFactory.getPaymentRepository();
        DownloadServlet generatorServlet = new DownloadServlet(paymentRepository);
        ServletRegistration.Dynamic servletRegistration = servletContext.addServlet("generatorServlet", generatorServlet);
        servletRegistration.addMapping("/auth/download");
    }

    private void registerLoginServlet(ServletContext servletContext) {
        UserService userService = new DefaultUserService(servletContext);
        LoginServlet loginServlet = new LoginServlet(userService);
        ServletRegistration.Dynamic servletRegistration = servletContext.addServlet("loginServlet", loginServlet);
        servletRegistration.addMapping("/login");
    }

    private void registerAuthenticationFilter(ServletContext servletContext) {
        FilterRegistration.Dynamic authenticationFilter = servletContext.addFilter("authenticationFilter", new AuthenticationFilter());
        authenticationFilter.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST),
                false, "/auth/*");
    }

    private void registerPaymentDashboardServlet(ServletContext servletContext) {
        CurrencyRepository currencyRepository = beanFactory.getCurrencyRepository();
        PaymentDashboardServlet dashboardServlet = new PaymentDashboardServlet(currencyRepository);
        ServletRegistration.Dynamic servletRegistration = servletContext.addServlet("dashboardServlet", dashboardServlet);
        servletRegistration.addMapping("/auth/dashboard");
    }

    private void registerPaymentServletFilter(ServletContext servletContext) {
        FilterRegistration.Dynamic paymentServletFilter = servletContext
                .addFilter("paymentServletFilter", new PaymentServletFilter());
        paymentServletFilter.addMappingForServletNames(EnumSet.of(DispatcherType.REQUEST),
                false, "paymentServlet");
    }

    private void registerDomainFilter(ServletContext servletContext) {
        FilterRegistration.Dynamic domainFilter = servletContext
                .addFilter("domainFilter", new DomainFilter());
        domainFilter.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST),
                false, "/*");
    }

    private void registerPaymentServlet(ServletContext servletContext) {
        PaymentRequestService requestService = beanFactory.getPaymentRequestService();
        PaymentServlet paymentServlet = new PaymentServlet(requestService,
                beanFactory.getPaymentRepository(),
                new DefaultJsonMapper());
        ServletRegistration.Dynamic servletRegistration = servletContext
                .addServlet("paymentServlet", paymentServlet);
        servletRegistration.addMapping("/auth/payment");
    }
}
